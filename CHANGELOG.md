# Change Log
All the changes made to this project are written here. The change log starts as
of development version [17w10a].

## Table of Contents
[pre1.1](#pre1.1-august-20-2017)  
[pre1](#pre1-august-19-2017)  
[dev4a](#dev4a-june-13-2017)  
[17w12b](#17w12b-march-22-2017)  
[17w12a](#17w12a-march-22-2017)  
[17w11a](#17w11a-march-16-2017)  
[17w10a](#17w10a-march-11-2017)  

## [pre1.1] - August 20, 2017
Changed the game's canvas size to 1000x610 to best fit most screens. Also 
fixed the level selection screen's UI to fit the screen's resolution.

### Changes
 - Changed canvas resolution from 1280x720 to 1000x610.
 - Changed Level Selection UI elements to fit the new canvas resolution.

## [pre1] - August 19, 2017
Muted the game's sounds and made it impossible to unmute. Also removed "fill
screen" feature to make the game playable in a website. This is the pre-release
version of the game, which is to be released on GameJolt. The only thing left 
to do is to add more levels to the game.

### Changes
 - Removed sounds and music in the game.
 - Muted sounds and music.
 - Removed settings menu.

## [dev4a] - June 13, 2017
Added multiple sound effects and fixed the sound engine to be more efficient 
and less aggressive. This update adds new levels as well, but is mostly about
making development easier. 

The version name scheme has also changed to "dev#" instead of having the week
number of the version's release: "dev" is development versions, which are like
the previous versions of the game. They are fully playable, but are far from 
being a final release. "beta" releases will be versions of the game that are 
much closer to being released, meaning that the features are fully implemented, 
but require polish. The final releases will simply have the version number of
the game.

### Additions
 - Added a method in level that allows you to spawn entity walls at corners of
   the play field. This method is very useful to create the _Waves_ level.
 - The player can now be invulnerable indefinitely (become God). This has to be 
   manually set in debugging.
 - Added debugging info on the raw score and the amount of points required to
   gain lives, bombs, change weapons and multiplier.
 - Added new levels: 
    - _Diamond Rush_. It spawns growing clusters of diamonds at random places 
      on the level. The player is stuck with the starting weapon with no life 
      and only one bomb.
    - _Super Lines_. It spawns lines of different enemies at a fast rate at 
      random locations.
    - _Traveling Madness_. Survive waves of travelers for 3 minutes.
 - Added a new weapon: _Sniper_. Shoots a single, fast bullet that is 
   invulnerable.
 - Added a method in Level: _spawnMixedTravelers_. This makes it easier to 
   spawn multiple golden travelers at the same edge as multiple travelers.
 - Added sound IDs so that it's easier to add sounds to be played.
 - Added all the sound effects required for the game.

### Changes
 - When spawning an entity wall, the entities will now face the orientation 
   given and will be on a line perpendicular to it.
 - Fixed the DeadEdge collisions with the edge of the level.
 - Removed TextEntity class as it is no longer used
 - TestLevel03 looks much more like a _Waves_ level. It is much more 
   challenging now.
 - Gaining lives and bombs are now based on the raw score, not the multiplied 
   score.
 - Changed the amount of points required to gain a life and a bomb.
 - Moved the _TestLevel03_ class to it's own file and renamed it to 
   _WavesLevel_, which is the first complete level.
 - Fixed the _Diamond_ AI. It was not getting faster. Now it is.
 - The _Super_ weapon doesn't ghost anymore.
 - Major code cleanup.
 - Fixed the way default weapons are set in levels. The player used to have the 
   narrow weapon instead of the level's default.
 - Removed the word "Start" on each button in the level selection menu.
 - Sounds are not duplicated as often as they were. Every time the game want's 
   to play a sound, it adds the ID instead of creating a sound instance 
   (playing the sound). Now the sounds are filtered so that they are not 
   duplicated.
 - When sounds are muted and get toggled back on after a few matches, the game
   won't play all the sounds that were queued up from the previous levels.
 - Fixed a bug where score gems spawned outside the level and stayed there.

### Fixed Issues
 - [54 - Change Entity Wall Orientation](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/54)
 - [56 - Wave Level](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/56)
 - [57 - Display raw score](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/57)
 - [59 - Make new sound effects](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/59)
 - [58 - Super Line Level](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/58)
 - [60 - No sound duplication](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/60)
 - [61 - No sound queuing when muted](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/61)
 - [62 - Score gems spawning out of bounds](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/62)

## [17w12b] - March 22, 2017
### Changes
 - Fixed the bonus controller's _tick_ function.
 - Fixed player collision with bonus peg. Now the game is showing the peg that 
   killed the player.

### Fixed Issues
 - [44 - Collision with BonusPegs](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/44)
 - [53 - Bonus Controller still using old tick](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/53)

## [17w12a] - March 22, 2017
Sounds are finally here! There are a lot of new features in this version: new 
enemies, new way of spawning them, but most importantly, sound! A sound engine 
has been created, but needs a bit of work to function properly and it needs 
more sounds, a lot more sounds.  

### Additions
 - Added the **Bouncer**: a simple enemy that moves very fast and bounces 
   around the play field.
 - A very basic sound engine has been added. With it, we can play sounds and 
   music. Both the sounds and the music can be muted individually.
 - Added the **Traveler**: an enemy that travels in a straight line like the 
   bouncer, but goes through the walls of the play field. It spawns outside of 
   the play field and fades away when it gets to the other side of it.
 - Added the **Golden Traveler**: a traveler that is twice as fast, but is 
   worth a lot more points and gems.
 - Added the **Slider**: an enemy very similar to the block, but smaller and 
   vulnerable.
 - Added the **Triangle**: a small enemy that follows you around.
 - Now able to spawn enemies in a cluster.
 - Added **Spawner** with two different kinds of spawners: **Point Spawner** 
   and **Random Spawner**. They both spawn a specific amount of entities at a 
   specifies time interval. The point spawner spawns the entities at a 
   specific point of the map. The random spawner spawns entities at a random 
   point on the play field.
 - Added enemy health. By default, every enemy has one health, meaning that it 
   only takes one bullet to kill, but this value can be changed for different 
   types of enemies.
 - Added the **Giant**: an enemy that is much larger than any other enemy. It 
   moves around very slowly and bounces around the play field. It takes more 
   that a single hit to kill, but once killed, it spawns more enemies of it's 
   type. The types can be:
    - _Spiral_
    - _Diamond_
    - _Slider_
    - _Bouncer_
 - Added a version label in the main menu. The game's version can be accessed 
   from everywhere in the game.
 - Added a settings menu. In there, you can set the canvas to fill the window 
   and if you want the music and/or sounds to be muted.
 - Added a method called _toggleColor_ in the Button class to make it change 
   between blue or red.

### Changes
 - Bonus Pegs spawn at the right place when width and/or height is even.
 - Ghosts and dead edges now require a width and height in the constructor.
 - Entities (enemies, score pops, ghosts and bullets) are not displayed 
   outside of the canvas, making the game run faster.
 - Added a debugging label in levels that display how many times the 
   _drawLine_ function gets called in a render cycle (not including GUI render 
   cycle).
 - When starting a level, player spawns in instead of just being there.
 - Moved enemy class files in a separate folder.
 - Level's _spawnEntitiesAtRandom_ and _spawnEntititesAtPosition_ methods 
   spawn everything in different directions unless specified. It used to just 
   spawn everything in the same direction, which was random.
 - Level's _spawnEntityWall_ and _spawnEntityRing_ can now spawn everything in 
   random directions instead of a specific, but can still be set the old way.
 - Enemies used to spawn at the top-left corner when spawning in random 
   locations. This issue has been fixed.
 - Render bounds were off when the render scale was changed. This has been 
   fixed.
 - _spawnEntityWall_ used to change the orientation of the wall when we 
   changed the amount of entities to be spawned. This has been fixed.
 - Changed the way entities are being updated. They now have full access to 
   the level they are in instead of a few things.
 - Changed the way entities are being deleted. Entities are now being updated, 
   then being deleted if needed.
 - The score that is checked when changing weapons is the raw score instead of 
   the multiplied score.
 - Changed the color of the _Slider_ model and its size has been shrunk by 25%.
 - Made the _Player_'s invulnerable flash time end around the end of the last 
   flash instead of at it.
 - Made the bouncing movement of the bouncer accessible to every other enemy 
   in the _move_ method of enemy.
 - Player doesn't go in a circle when it wants to turn around. The model still 
   has the turning animation, but the player goes straight where it wants to 
   go.
 - Window resize event only call the _resizeCanvas_ method from the main 
   component. This method handles the change in dimensions now.
 - Sounds and music get paused or start playing when their mute value toggles.

### Closed Issues
 - [14 - Settings Menu](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/14)
 - [31 - Not Rendering Outside of Canvas](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/31)
 - [39 - Slider](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/39)
 - [41 - Sound Engine](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/41)
 - [42 - Bigger, Spawner Enemies](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/42)
 - [43 - Bonus Pegs Don't Spawn when even width/height](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/43)
 - [45 - Bouncer](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/45)
 - [46 - Triangle](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/46)
 - [47 - Traveler](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/47)
 - [48 - Golden Traveler](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/48)
 - [49 - Spawning enemies in clusters](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/49)
 - [50 - Enemy Spawner](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/50)
 - [51 - Spawn Entity Wall is Broken](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/51)
 - [52 - Change Player Movement](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/52)

## [17w11a] - March 16, 2017
### Additions
 - Canvas can now be set to a specific resolution instead of taking the entire 
   window.
 - New enemy entity: Block
    - Moves either vertically or horizontally randomly in steps;
    - They are invulnerable;
    - Can spawn with either a specific size or a random one.
 - Time based levels. These levels are completed when the timer runs out. This 
   timer is set by default to 2 minutes, but can be set to any number
   (measured in seconds). This replaces levels that don't check lives by 
   default.
 - Added a static method to Game that splits numbered strings with a specific 
   separator. The default one is a comma.
 - Created a _Bonus_ category in BonusController. These bonuses include: 
   _Super Shot_, _Reverse Shot_ and _Invulnerable_. The weapons are now on a 
   timer, reverting back to the normal weapon you had before getting the bonus 
   weapon.
 - Added player invulnerability. Players are invulnerable when they spawn and 
   when they get an invulnerable bonus from the BonusController. Player casts 
   ghosts constantly when they are invulnerable, then cast three ghosts in a 
   much longer interval before the invulnerability wears off.

### Changes
 - When multiplier is based on score, the multiplier doesn't affect the points 
   needed to increase the multiplier.
 - Buttons can be set to be blue (default) or red.
 - Pause menus are deleted when they are done and the user is just resuming 
   the game.
 - TestLevel01 spawn Blocks.
 - Bullets now extends from enemy, allowing them to cast ghosts.
 - _Super_ weapon now spawn ghosts.
 - Score and multiplier labels in levels are now comma split.
 - Bombs and ghosts in levels are now updated even when the player is dying.
 - Debug info is not rendered by default, but it can still be toggled.
 - Removed the ring arrow spawning in TestLevel03.
 - Made the ghosts fade by default.

### Closed Issues
 - [23 - Block](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/23)
 - [25 - Time Based Levels](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/25)
 - [29 - Split Score Values](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/29)
 - [33 - Multiplier by Point Value](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/33)
 - [34 - Bullet Ghosting Effect](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/34)
 - [35 - Special Weapon Timer](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/35)
 - [36 - Update Ghosting Effect when Player is Dead](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/36)
 - [37 - Make Bombs Tick Even if Player is Dead](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/37)
 - [38 - Make Player Invulnerable](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/issues/38)

## [17w10a] - March 11, 2017
This is the initial development version released.


[17w10a]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/17w10a
[17w11a]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/17w11a
[17w12a]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/17w12a
[17w12b]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/17w12b
[dev4a]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/dev4a
[pre1]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/pre1
[pre1.1]: https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags/pre1.1
