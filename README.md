# Vector Attack 2
This project started in July 26, 2016 and is a follow up to my first game
[Vector Attack](http://gamejolt.com/games/vector-attack/113082 "Vector Attack on GameJolt"), which was the final project of a computer science course at John Abbott College. This game is intended to be a clone to the game _Geometry Wars: Retro Evolved_ by Bizarre Creations, released in 2003.

To run the game, simply download a released package of the game, which can be found [here](https://gitlab.com/SimonPierreBrazeau/VectorAttack2/tags), as a zip or tar file and extract the contents somewhere on your computer. Open the extracted folder and open the _index.html_ file. This should open a page on your web browser with the game running. The game in it's current state runs best on Google's Chrome web browser.
