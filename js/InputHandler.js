/** @class */
class InputHandler {
  /**
   * @constructor
   * @memberof InputHandler
   */
  constructor () {
    addEventListener("keydown", this.keyPress, false);
    addEventListener("keyup", this.keyRelease, false);
  }
  /**
   * Takes an event object and saves the key that is pressed in an array.
   * @method keyPress
   * @memberof InputHandler
   * @param {object} e Event object.
   */
  keyPress (e) {
    var keyCode = e.keyCode;
    keys[keyCode] = true;
  }
  /**
   * Takes an event object and removes the key that is released from an array.
   * @method keyRelease
   * @memberof InputHandler
   * @param {object} e Event object.
   */
  keyRelease (e) {
    var keyCode = e.keyCode;
    delete keys[keyCode];
  }
  /**
   * Returns whether a specified key is being pressed or not.
   * @method getKey
   * @memberof InputHandler
   * @param {number} key Key code of the key reqested.
   * @returns {boolean}
   */
  getKey (key) { return keys[key]; }
}
