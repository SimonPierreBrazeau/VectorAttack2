/** @class */
class Icon extends Generic {
  /**
   * @constructor
   * @memberof Icon
   * @param {string} identifier          Unique name of the icon.
   * @param {number} x                   X position.
   * @param {number} y                   Y position.
   * @param {object} [model = new Model] Model instance of the icon.
   */
  constructor (identifier, x, y, model = new Model()) {
    super(x, y, model);
    this.entityType = EntityID.ICON;
    this.identifier = identifier;
  }
  /**
   * Repositions the icon to the given coordinates.
   * @method reposition
   * @memberof Icon
   * @param {number} x New x position of the icon.
   * @param {number} y New y position of the icon.
   */
  reposition (x, y) {
    this.x = x;
    this.y = y;
  }
  /**
   * Goes through an array of entities and returns the entity that is requested.
   * If entity is not found, it returns null.
   * @method find
   * @memberof Icon
   * @static
   * @param {string} identifier   Identifier of entity to look for.
   * @param {Array<object>} group Array of entities to go through.
   * @returns {object}
   */
  static find (identifier, group) {
    for (var i = 0; i < group.length; i++)
      if (group[i].entityType == EntityID.ICON)
        if (group[i].identifier == identifier)
          return group[i];
  }
}
