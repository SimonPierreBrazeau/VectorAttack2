/** 
 * @class 
 * @extends Generic
 * */
class Label extends Generic {
  /**
   * @constructor
   * @memberof Label
   * @param {string} identifier          Unique name of the label.
   * @param {number} x                   X position.
   * @param {number} y                   Y position.
   * @param {string} text                Text of the label.
   * @param {number} fontSize            Size of the text.
   * @param {number} [align = 0]         Controls how the text is aligned. The
   *                                     alignments are:
   *                                      - 0: centered;
   *                                      - 1: left;
   *                                      - 2: right.
   * @param {string} [color = "#4bdfff"] Color of the text.
   */
  constructor (identifier, x, y, text, fontSize, align = 0,
    color = "#4bdfff") {
    super(x, y);
    this.entityType = EntityID.LABEL;
    this.identifier = identifier;

    this.model.addText(text, "text", color, fontSize, align, x, y);
  }
  /**
   * Repositions the label to the given coordinates.
   * @method reposition
   * @memberof Label
   * @param {number} x New x position of the label.
   * @param {number} y New y position of the label.
   */
  reposition (x, y) {
    this.x = x;
    this.y = y;
  }
  /**
   * Goes through an array of entities and returns the entity that is requested.
   * If entity is not found, it returns null.
   * @method find
   * @memberof Label
   * @static
   * @param {string} identifier   Identifier of entity to look for.
   * @param {Array<object>} group Array of entities to go through.
   * @returns {object}
   */
  static find (identifier, group) {
    for (var i = 0; i < group.length; i++) {
      if (group[i].entityType == EntityID.LABEL)
        if (group[i].identifier == identifier)
          return group[i];
    }
  }
}
