/** @class */
class Button {
  /**
   * @constructor
   * @memberof Button
   * @param {string} identifier     Unique name of the button.
   * @param {number} x              X position.
   * @param {number} y              Y position.
   * @param {number} width          Width of the button.
   * @param {number} height         Height of the button.
   * @param {string} text           Text inside the button.
   * @param {number} fontSize       Size of the text's font.
   * @param {boolean} [blue = true] Controls whether the button is blue or red.
   */
  constructor (identifier, x, y, width, height, text, fontSize, blue = true) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.entityType = EntityID.BUTTON;
    this.identifier = identifier;
    this.text = text;
    this.fontSize = fontSize;

    if (blue)
      this.model = Model.createButtonModel(width, height);
    else
      this.model = Model.createButtonModel(width, height, "#E8235A", "#bd4c6c");
    this.model.addText(text, "title", "#4bdfff", fontSize, 0, x, y);

    this.isPressed = false;
  }
  /**
   * Updates variables of the button.
   * @method tick
   * @memberof Button
   * @param {boolean} shoot          Controls whether the "shoot" button is 
   *                                 pressed or not.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of the rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (shoot, mousePos, renderScale, soundEngine) {
    if (mousePos.x >= this.x - this.width * renderScale / 2 &&
        mousePos.x <= this.x + this.width * renderScale / 2 &&
        mousePos.y >= this.y - this.height * renderScale / 2 &&
        mousePos.y <= this.y + this.height * renderScale / 2 &&
        shoot) {
      this.isPressed = true;
      soundEngine.addSound(SoundID.MENU_SELECT);
    } else
      this.isPressed = false;
  }
  /**
   * Repositions the button to the given coordinates.
   * @method reposition
   * @memberof Button
   * @param {number} x New x position of the button.
   * @param {number} y New y position of the button.
   */
  reposition (x, y) {
    this.x = x;
    this.y = y;
  }
  /**
   * Changes the color of the button to either blue or red.
   * @method toggleColor
   * @memberof Button
   * @param {boolean} blue Controls whether the button is blue or red.
   */
  toggleColor (blue) {
    if (blue)
      this.model = Model.createButtonModel(this.width, this.height);
    else
      this.model = Model.createButtonModel(this.width, this.height,
        "#E8235A", "#bd4c6c");

    this.model.addText(this.text, "title", "#4bdfff", this.fontSize, 0,
      this.x, this.y);
  }
  /**
   * Goes through an array of entities and returns the entity that is requested.
   * If entity is not found, it returns null.
   * @method find
   * @memberof Button
   * @static
   * @param {string} identifier   Identifier of entity to look for.
   * @param {Array<object>} group Array of entities to go through.
   * @returns {object}
   */
  static find (identifier, group) {
    for (var i = 0; i < group.length; i++)
      if (group[i].entityType == EntityID.BUTTON)
        if (group[i].identifier == identifier)
          return group[i];
    
    return null;
  }
}
