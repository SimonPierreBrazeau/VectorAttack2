/** @class */
class GameScreen {
  /**
   * @constructor
   * @memberof GameScreen
   */
  constructor () {
    this.xoffset = 0;
    this.yoffset = 0;
    this.canvasWidth = 0;
    this.canvasHeight = 0;
    this.renderScale = 0;
    this.font = "Arial";

    // Mouse cursor for in game play
    this.mouseComabtCursor = Model.createCombatCursor();

    this.curCursor = this.mouseComabtCursor;

    this.fpsText = new Label("fpsText", 0, 0, "0 fps", 10, 0, "#0F0");
    this.renderCallText = new Label("renderCalls", 0, 0, "0", 10, 1, "#0F0");
    
    // Score debug info
    this.scores = [
      "rawScore: ",
      "multiplierScoreChange: ",
      "scoreWeaponChange: ",
      "gainLivesScore: ",
      "gainBombScore: "
    ];

    this.rscoreText = new Label("rscore", 0, 0, this.scores[0], 10, 2, "#0F0");
    this.mScoreText = new Label("mscore", 0, 0, this.scores[1], 10, 2, "#0F0");
    this.wScoreText = new Label("wscore", 0, 0, this.scores[2], 10, 2, "#0F0");
    this.lScoreText = new Label("lscore", 0, 0, this.scores[3], 10, 2, "#0F0");
    this.bScoreText = new Label("bscore", 0, 0, this.scores[4], 10, 2, "#0F0");

    this.levelType = "";

    this.renderDebugInfo = false;

    this.renderCount = 0;
  }
  /**
   * Renders the level from a given Game object on a given canvas context, 
   * including entities.
   * @method render
   * @memberof GameScreen
   * @param {object} game Game instance.
   * @param {object} ctx  Canvas context object.
   */
  render (game, ctx) {
    this.renderCount = 0;
    // Update the canvas variables
    this.renderScale = game.renderScale;
    this.canvasWidth = ctx.canvas.width;
    this.canvasHeight = ctx.canvas.height;

    this.levelType = game.currentLevel.levelType;

    this.renderDebugInfo = game.renderDebugInfo;

    // Clear the canvas
    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, this.canvasWidth, this.canvasHeight);

    // -------------------------------- //
    // ----< Rendering the player >---- //
    // -------------------------------- //
    if (this.levelType != LevelID.MENU_LEVEL) {
      // Update on-screen position of the player
      var pl = game.currentLevel.player;
      this.xoffset = pl.x - (ctx.canvas.width / 2) / this.renderScale;
      this.yoffset = pl.y - (ctx.canvas.height / 2) / this.renderScale;
      var osx = this.canvasWidth / 2;
      var osy = this.canvasHeight / 2;
      pl.changeOnScreenPosition(osx, osy);

      // ------------------------------------ //
      // ----< Rendering the play field >---- //
      // ------------------------------------ //
      this.updateVertices(game.currentLevel.model, 0, 0);
      this.renderModel(game.currentLevel.model, ctx);

      // Render Bombs
      for (var i = 0; i < game.currentLevel.bombs.length; i++) {
        this.renderBomb(game.currentLevel.bombs[i], ctx);
      }

      // Is the player spawning?
      if (pl.isSpawning) {
        this.renderSpawningEntity(pl.model, pl.x, pl.y, pl.spawnTime,
                                  pl.spawnSpan, ctx);
      } else {
        // Update player vertices
        this.updateVertices(pl.model, pl.x, pl.y, false, pl.dirFacing);

        // Render the player
        this.renderModel(pl.model, ctx);
      }
      // ----------------------------- //
      // ----< Rendering bullets >---- //
      // ----------------------------- //

      for (var j = 0; j < game.currentLevel.bullets.length; j++) {
        if (this.checkRenderable(game.currentLevel.bullets[j])) {
          this.updateVertices(game.currentLevel.bullets[j].model,
            game.currentLevel.bullets[j].x,
            game.currentLevel.bullets[j].y, false,
            game.currentLevel.bullets[j].dirFacing);
          this.renderModel(game.currentLevel.bullets[j].model, ctx);
        }
      }

      // Render ghosts
      for (var i = 0; i < game.currentLevel.ghosts.length; i++) {
        let g = game.currentLevel.ghosts[i];
        if (this.checkRenderable(g)) {
          if (g.isFading) {
            this.renderFadingEntity(g.model, g.x, g.y, g.fadeTime, g.fadeSpan,
              ctx, g.dirFacing, false);
          } else {
            this.updateVertices(g.model, g.x, g.y, false, g.dirFacing);
            this.renderModel(g.model, ctx, false);
          }
        }
      }
    } else {
      // Render in menu
      this.xoffset = 0;
      this.yoffset = 0;

      // ------------------------------------ //
      // ----< Rendering the play field >---- //
      // ------------------------------------ //
      this.updateVertices(game.currentLevel.model, 0, 0);
      this.renderModel(game.currentLevel.model, ctx);
    }


    // ------------------------------ //
    // ----< Rendering entities >---- //
    // ------------------------------ //
    for (var k = 0; k < game.currentLevel.entities.length; k++) {
      var en = game.currentLevel.entities[k];
      if (this.checkRenderable(en)) {
        if (en.isSpawning)
          this.renderSpawningEntity(en.model, en.x, en.y, en.spawnTime,
            en.spawnSpan, ctx, en.dirFacing);
        else if (en.isFading)
          this.renderFadingEntity(en.model, en.x, en.y, en.fadeTime,
            en.fadeSpan, ctx, en.dirFacing);
        else {
          this.updateVertices(en.model, en.x, en.y, false, en.dirFacing);
          this.renderModel(en.model, ctx);
        }
      }
    }
  }
  /**
   * Renders the level's GUI elements from a given Game object on a given 
   * canvas context.
   * @method renderGUI
   * @memberof GameScreen
   * @param {object} game Game instance.
   * @param {object} ctx  Canvas context object.
   */
  renderGUI (game, ctx) {
    // Render level's GUI entities
    var lGUIe = game.currentLevel.GUIentities;
    for  (var i = 0; i < lGUIe.length; i++) {
      this.updateVertices(lGUIe[i].model, lGUIe[i].x, lGUIe[i].y, true);
      this.renderModel(lGUIe[i].model, ctx);
    }

    if (this.levelType != LevelID.MENU_LEVEL) {
      // Update game text
      this.updateDebugText(game);
    }

    // -------------------------------------- //
    // ----< Rendering the mouse cursor >---- //
    // -------------------------------------- //
    this.renderCursor(mousePos.x, mousePos.y, ctx);

    // Render debugging stuff
    if (this.renderDebugInfo) {
      this.updateVertices(this.fpsText.model, this.fpsText.x,
        this.fpsText.y, true);
      this.renderModel(this.fpsText.model, ctx);
      this.updateVertices(this.renderCallText.model, this.renderCallText.x,
        this.renderCallText.y, true);
      this.renderModel(this.renderCallText.model, ctx);

      // -- Score debugging --
      this.updateVertices(this.rscoreText.model, this.rscoreText.x,
        this.rscoreText.y, true);
      this.renderModel(this.rscoreText.model, ctx);

      this.updateVertices(this.mScoreText.model, this.mScoreText.x,
        this.mScoreText.y, true);
      this.renderModel(this.mScoreText.model, ctx);

      this.updateVertices(this.wScoreText.model, this.wScoreText.x,
        this.wScoreText.y, true);
      this.renderModel(this.wScoreText.model, ctx);

      this.updateVertices(this.lScoreText.model, this.lScoreText.x,
        this.lScoreText.y, true);
      this.renderModel(this.lScoreText.model, ctx);

      this.updateVertices(this.bScoreText.model, this.bScoreText.x,
        this.bScoreText.y, true);
      this.renderModel(this.bScoreText.model, ctx);
    }
  }
  /**
   * Renders a given Model object on a given canvas context.
   * @method renderModel
   * @memberof GameScreen
   * @param {object} model                    Model to render.
   * @param {object} ctx                      Canvas context object.
   * @param {boolean} [renderPolygons = true] Rendering model's polygons.
   */
  renderModel (model, ctx, renderPolygons = true) {
    // Is the model visible?
    if (model.visible) {
      // Render the model's polygons
      if (renderPolygons)
        this.renderModelPolygons(model, ctx);

      // Render vertices
      this.renderModelVertices(model, ctx);

      // Render text
      this.renderModelText(model, ctx);
    }
  }
  /**
   * Renders a given Model object's text elements on a given canvas context.
   * @method renderModelText
   * @memberof GameScreen
   * @param {object} model Model to render.
   * @param {object} ctx   Canvas context object.
   */
  renderModelText (model, ctx) {
    if (model.visible) {
      for (var i = 0; i < model.text.length; i++) {
        ctx.font = model.text[i].size * this.renderScale + "px " + this.font;
        ctx.fillStyle = model.text[i].color;

        var textWidth = ctx.measureText(model.text[i].string).width;

        var tempx = model.center[0];
        var tempy = model.center[1] + (model.text[i].size / 3);

        if (model.text[i].align == 0)
          tempx -= (textWidth / 2);
        else if (model.text[i].align == 2)
          tempx -= textWidth;

        ctx.fillText(model.text[i].string, tempx, tempy);
      }
    }
  }
  /**
   * Updates debugging information that can be rendered on a canvas.
   * @method updateDebugText
   * @memberof GameScreen
   * @param {object} game Game instance.
   */
  updateDebugText (game) {
    let l = game.currentLevel;
    
    // FPS counter
    this.fpsText.model.text[0].string = game.fpsCount + " fps";
    this.fpsText.x = (this.canvasWidth) - (50 * this.renderScale);
    this.fpsText.y = 10 * this.renderScale;

    // Render call
    this.renderCallText.model.text[0].string = "drawLine Calls: " +
      this.renderCount;
    this.renderCallText.reposition(0,
      this.canvasHeight - 10 * this.renderScale);
    
    // -- Score text --
    let eCount = 0;
    let w = MainComponent.getWidth();
    let h = MainComponent.getHeight();
    let gap = 12;

    // Gaining bombs
    if (l.levelType == LevelID.GAME_LEVEL) {
      if (l.gainBombs) {
        if (!this.bScoreText.model.visible)
          this.bScoreText.model.visible = true;

        this.bScoreText.model.text[0].string = this.scores[4] + 
          Game.scoreString(l.gainBombScore.toString());
        eCount++;
        this.bScoreText.reposition(w, h - (eCount * gap));
      } else
        this.bScoreText.model.visible = false;

      // Gaining lives
      if (l.gainLives) {
        if (!this.lScoreText.model.visible)
          this.lScoreText.model.visible = true;
        
        this.lScoreText.model.text[0].string = this.scores[3] + 
          Game.scoreString(l.gainLivesScore.toString());
        eCount++;
        this.lScoreText.reposition(w, h - (eCount * gap));
      } else
        this.lScoreText.model.visible = false;

      // Changing weapons based on score
      if (l.changeWeaponByScore) {
        if (!this.wScoreText.model.visible)
          this.wScoreText.model.visible = true;
        
        this.wScoreText.model.text[0].string = this.scores[2] + 
          Game.scoreString(l.scoreWeaponChange.toString());
        eCount++;
        this.wScoreText.reposition(w, h - (eCount * gap));
      } else
        this.wScoreText.model.visible = false;

      // Multiplier based on score
      if (l.multiplierBasedOnScore) {
        if (!this.mScoreText.model.visible)
          this.mScoreText.model.visible = true;

        this.mScoreText.model.text[0].string = this.scores[1] + 
          Game.scoreString(l.multiplierScoreChange.toString());
        eCount++;
        this.mScoreText.reposition(w, h - (eCount * gap));
      }

      this.rscoreText.model.text[0].string = this.scores[0] + 
        Game.scoreString(l.rawScore.toString());
      eCount++;
      this.rscoreText.reposition(w, h - (eCount * gap));
    }
  }
  /**
   * Renders a line between two given points on a given canvas context.
   * @method drawLine
   * @memberof GameScreen
   * @param {number} x1    X position of the first point.
   * @param {number} y1    Y position of the first point.
   * @param {number} x2    X position of the second point.
   * @param {number} y2    Y position of the second point.
   * @param {number} width Width of the line.
   * @param {string} color Color of the line.
   * @param {object} ctx   Canvas context object.
   */
  drawLine (x1, y1, x2, y2, width, color, ctx) {
    ctx.strokeStyle = color;
    ctx.lineWidth = width;

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();

    this.renderCount++;       // DEBUG - remove
  }
  /**
   * Updates a given Model object's coordinates for each vertex at a given 
   * position.
   * @method updateVertices
   * @memberof GameScreen
   * @param {object} model                 Model object to update.
   * @param {number} x                     X position of the model.
   * @param {number} y                     Y position of the model.
   * @param {boolean} [gui = false]        Controls whether the model is 
   *                                       rendered on the GUI or not.
   * @param {number} [rotation = 0]        Rotation of the model.
   * @param {number} [scale = model.scale] Scale of the model.
   */
  updateVertices (model, x, y, gui = false, rotation = 0,
                  scale = model.scale) {
    model.center[0] = x;
    model.center[1] = y;
    if (!gui) {
      model.center[0] -= this.xoffset;
      model.center[1] -= this.yoffset;

      model.center[0] *= this.renderScale;
      model.center[1] *= this.renderScale;
    }

    for (var i = 0; i < model.vertices.length; i++) {
      // Grab the vertices
      var vx = model.vertices[i][0];
      var vy = model.vertices[i][1];

      // scale the vertices
      vx *= scale;
      vy *= scale;

      if (gui) {
        vx *= this.renderScale;
        vy *= this.renderScale;
      }

      // Rotate the vertices
      var rx = (Math.cos(rotation * Math.PI) * vx -
        Math.sin(rotation * Math.PI) * vy);
      var ry = (Math.sin(rotation * Math.PI) * vx +
        Math.cos(rotation * Math.PI) * vy);

      // Reposition object's position
      rx += x;
      ry += y;

      if (!gui) {
        // Offset with Global position
        rx -= this.xoffset;
        ry -= this.yoffset;

        // Scale based on game's render scale
        rx *= this.renderScale;
        ry *= this.renderScale;
      }

      // Apply to object's vertices
      model.cvertices[i][0] = rx;
      model.cvertices[i][1] = ry;
    }
  }
  /**
   * Updates a given Model object's coordinates of each vertex at a given 
   * position on a given canvas context with a "spawning" animation.
   * @method renderSpawningEntity
   * @memberof GameScreen
   * @param {object} model                    Model to render.
   * @param {number} x                        X position of the model.
   * @param {number} y                        Y position of the model.
   * @param {number} time                     Current frame of the animation.
   * @param {number} span                     Total amount of frames the 
   *                                          animation lasts.
   * @param {object} ctx                      Canvas context object.
   * @param {number} [rotation = 0]           Rotation of the model.
   * @param {boolean} [renderPolygons = true] Controls whether the renderer
   *                                          should render the model's 
   *                                          polygons.
   * @param {number} [scale = model.scale]    Scale of the model.
   */
  renderSpawningEntity (model, x, y, time, span, ctx, rotation = 0,
                        renderPolygons = true, scale = model.scale) {
    // Set the scaling of the model based on spawn timer
    var vscale = model.scale + (model.scale * (time / span));
    var pscale = model.scale - (model.scale * (time / span));
    var alpha = (span - time) / span;

    // Change the global alpha
    ctx.globalAlpha = alpha;

    // Update vertives for polygons
    this.updateVertices(model, x, y, false, rotation, pscale);

    // Render polygons
    if (renderPolygons)
      this.renderModelPolygons(model, ctx)

    // Update the vertices
    this.updateVertices(model, x, y, false, rotation, vscale);

    // Render the vertices
    this.renderModelVertices(model, ctx);

    // Reset the global alpha
    ctx.globalAlpha = 1;
  }
  /**
   * Renders a given Model object's coordinates of each vertex at a given 
   * position on a given canvas context with a "fading" animation.
   * @method renderFadingEntity
   * @memberof GameScreen
   * @param {object} model                    Model to render.
   * @param {number} x                        X position of the model.
   * @param {number} y                        Y position of the model.
   * @param {number} time                     Current frame of the animation.
   * @param {number} span                     Total amount of frames the 
   *                                          animation lasts.
   * @param {object} ctx                      Canvas context object.
   * @param {number} [rotation = 0]           Rotation of the model.
   * @param {boolean} [renderPolygons = true] Controls whether the renderer
   *                                          should render the model's 
   *                                          polygons.
   * @param {number} [scale = model.scale]    Scale of the model.
   */
  renderFadingEntity (model, x, y, time, span, ctx, rotation = 0,
    renderPolygons = true, scale = model.scale) {
    // Set the scaling of the model based on spawn timer
    var vscale = model.scale + (model.scale * ((span - time) / span));
    var pscale = model.scale * (time / span);
    var alpha = time / span;

    // Change the global alpha
    ctx.globalAlpha = alpha;

    // Update vertives for polygons
    this.updateVertices(model, x, y, false, rotation, pscale);

    // Render polygons
    if (renderPolygons)
      this.renderModelPolygons(model, ctx)

    // Update the vertices
    this.updateVertices(model, x, y, false, rotation, vscale);

    // Render the vertices
    this.renderModelVertices(model, ctx);

    // Reset the global alpha
    ctx.globalAlpha = 1;
  }
  /**
   * Renders a given Model object's vertices on a given canvas context.
   * @method renderModelVertices
   * @memberof GameScreen
   * @param {object} model Model to render.
   * @param {object} ctx   Canvas context object.
   */
  renderModelVertices (model, ctx) {
    // Loop throught every edge of the model
    for (var i = 0; i < model.edges.length; i++) {
      // Grab the color and thickness of the edge
      var lineColor = model.ecolors[i];
      var lineWidth = model.thickness[i] * this.renderScale;

      // Grab the edge's coordinates
      var x1 = model.cvertices[model.edges[i][0]][0];
      var y1 = model.cvertices[model.edges[i][0]][1];
      var x2 = model.cvertices[model.edges[i][1]][0];
      var y2 = model.cvertices[model.edges[i][1]][1];


      // Draw the edge
      this.drawLine(x1, y1, x2, y2, lineWidth, lineColor, ctx);
    }
  }
  /**
   * Renders a given Model object's polygons on a given canvas context.
   * @method renderModelPolygons
   * @memberof GameScreen
   * @param {object} model Model to render.
   * @param {object} ctx   Canvas context object.
   */
  renderModelPolygons (model, ctx) {
    for (var j = 0; j < model.polygons.length; j++) {
      // Grab the polygon's color
      var polyColor = model.pcolors[j];
      ctx.lineWidth = 1;

      ctx.fillStyle = polyColor;
      ctx.beginPath();
      ctx.moveTo(model.cvertices[model.polygons[j][0]][0],
        model.cvertices[model.polygons[j][0]][1]);
      // Go through every edge of the polygon
      for (var k = model.polygons[j].length - 1; k > 0; k--) {
        ctx.lineTo(model.cvertices[model.polygons[j][k]][0],
          model.cvertices[model.polygons[j][k]][1]);
      }
      ctx.closePath();
      ctx.fill();
    }
  }
  /**
   * Renders a mouse cursor at a given position on a given canvas context.
   * @method renderCursor
   * @memberof GameScreen
   * @param {number} x X position of cursor.
   * @param {number} y Y position of cursor.
   * @param {object} ctx Canvas context object.
   */
  renderCursor (x, y, ctx) {
    this.updateVertices(this.curCursor, x, y, true);
    this.renderModel(this.curCursor, ctx);
  }
  /**
   * Renders a given Bomb object on a given canvas context.
   * @method renderBomb
   * @memberof GameScreen
   * @param {object} bomb Bomb object to render.
   * @param {object} ctx  Canvas context object.
   */
  renderBomb (bomb, ctx) {
    let x = bomb.x - this.xoffset;
    let y = bomb.y - this.yoffset;
    let size = 0;
    let width = 0;
    ctx.strokeStyle = bomb.color;

    if (bomb.lifeTime >= bomb.lifeSpan / 2) {
      width = bomb.currentSize;
      size = bomb.currentSize / 2;
    } else {
      width = (bomb.currentSize / (bomb.lifeSpan / 2)) * bomb.lifeTime;
      size = bomb.currentSize - width / 2;
    }

    ctx.lineWidth = width;

    ctx.beginPath();
    ctx.arc(x, y, size, 0, 2 * Math.PI);
    ctx.stroke();
  }
  /**
   * Verifies if a given entity is inside the canvas bounds so that it can be
   * rendered.
   * @method checkRenderable
   * @memberof GameScreen
   * @param {object} entity Entity to verify.
   * @returns {boolean}
   */
  checkRenderable (entity) {
    // Get the top-left and bottom-right corners of the model
    let ex1 = entity.x - (entity.width / 2);
    let ey1 = entity.y - (entity.height / 2);
    let ex2 = entity.x + (entity.width / 2);
    let ey2 = entity.y + (entity.height / 2);

    // Store the top-left and bottom-right corners of the canvas
    let cx1 = this.xoffset;
    let cy1 = this.yoffset;
    let cx2 = cx1 + this.canvasWidth / this.renderScale;
    let cy2 = cy1 + this.canvasHeight / this.renderScale;

    // Check if it's in the canvas
    if (ex2 >= cx1 && ex1 <= cx2 && ey2 >= cy1 && ey1 <= cy2)
      return true;
    else
      return false;
  }
}
