/** @class */
class SoundEngine {
  /**
   * @constructor
   * @memberof SoundEngine
   */
  constructor () {
    this.sounds = [];
    this.soundIDcount = [];

    this.playingSounds = [];
    this.playingMusic = new Audio();

    this.soundsMuted = false;
    this.musicMuted = false;

    this.setupDone = false;
  }
  /**
   * Updates the variables of the sound engine. This include playing and 
   * stopping sounds.
   * @method tick
   * @memberof SoundEngine
   * @param {boolean} soundsMuted Controls whether the sounds are muted or not.
   * @param {boolean} musicMuted  Controls whether the music is muted or not.
   */
  tick (soundsMuted, musicMuted) {    
    if (this.soundsMuted != soundsMuted) {
      this.soundsMuted = soundsMuted;
      if (soundsMuted)
        this.pauseSounds();
      else
        this.resumeSounds();
    }

    if (this.musicMuted != musicMuted) {
      this.musicMuted = musicMuted;
      if (musicMuted)
        this.pauseMusic();
      else
        this.resumeMusic();
    }

    // Add sounds from queue to actual object
    for (var i = 0; i < this.sounds.length; i++) {
      if (this.soundIDcount[this.sounds[i]] == undefined)
        this.soundIDcount[this.sounds[i]] = 1;
      else
        this.soundIDcount[this.sounds[i]] += 1;
    }

    this.sounds = [];

    for (var i = 0; i < this.soundIDcount.length; i++) {
      while (this.soundIDcount[i] > 0) {
        this.soundIDcount[i] -= 10;

        let j = this.playingSounds.length;
        this.playingSounds.push(new Audio());
        this.playingSounds[j].src = Sounds.getSource(i);
      }

      if (this.soundIDcount[i] < 0) this.soundIDcount[i] = 0;
    }

    // Check sounds
    if (!soundsMuted) {
      for (var i = 0; i < this.playingSounds.length; i++) {
        let s = this.playingSounds[i];

        // Is the sound loaded and not playing?
        if (s.currentTime == 0 && s.readyState == 4)
          s.play();
        else if (s.ended) {   // Sound is done playing
          this.playingSounds.splice(i, 1);
          i--
        }
      }
    }

    // Check music
    if (!musicMuted) {
      if (this.playingMusic != null)
        if (this.playingMusic.currentTime == 0 && 
            this.playingMusic.readyState == 4)
          this.playingMusic.play();
    }
  }
  /**
   * Creates an Audio object containing a sound from the given source and adds 
   * it to the sounds array.
   * @method addSound
   * @memberof SoundEngine
   * @param {unmber} sound Sound ID of the sound.
   */
  addSound (sound) {
    if (!this.soundsMuted)
      this.sounds.push(sound);
  }
  /**
   * Creates an Audio object containing the music from the given source and sets
   * it to the music variable.
   * @method addMusic
   * @memberof SoundEngine
   * @param {string} music Source of the music.
   */
  addMusic (music) {
    this.playingMusic.pause();
    this.playingMusic.setAttribute("src", music);
    this.playingMusic.load();
    this.playingMusic.loop = true;
  }
  /**
   * Pauses every playing sounds in the sounds array.
   * @method pauseSounds
   * @memberof SoundEngine
   */
  pauseSounds () {
    if (this.soundsMuted) {
      for (var i = 0; i < this.sounds.length; i++)
        this.sounds[i].pause();
    }
  }
  /**
   * Pauses the playing music.
   * @method pauseMusic
   * @memberof SoundEngine
   */
  pauseMusic () {
    if (this.musicMuted)
      this.playingMusic.pause();
  }
  /**
   * Resumes every sounds in the sounds array if the sounds are not muted.
   * @method resumeSounds
   * @memberof SoundEngine
   */
  resumeSounds () {
    if (!this.soundsMuted) {
      for (var i = 0; i < this.sounds.length; i++)
        this.sounds[i].play();
    }
  }
  /**
   * Resumes the music if it isn't muted.
   * @method resumeMusic
   * @memberof SoundEngine
   */
  resumeMusic () {
    if (!this.musicMuted)
      this.playingMusic.play();
  }
  /**
   * Returns an object containing the source of every playable music.
   * @method getMusic
   * @memberof SoundEngine
   * @static
   * @returns {object}
   */
  static getMusic () {
    return {
      INTRO: "",
      MAIN_MENU: "",
      GAME: ""
    };
  }
  /**
   * Returns an object containing the source of every playable sounds.
   * @method getSounds
   * @memberof SoundEngine
   * @static
   * @returns {object}
   */
  static getSounds () {
    return {
      SHOOT: "",
      BREAK: "",
      SPAWN: "",
      ENTITY_DEATH: ""
    };
  }
}
