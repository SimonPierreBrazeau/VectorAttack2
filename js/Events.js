/**
 * This file is written in what I call "clasic js" so that we can access and
 * create global variables.
 */

keys = {};

// Mouse buttons and keyboard keys --------------------------------------- {{{
KeyEvents = {
  LEFTCLICK: 0,
  MIDDLECLICK: 1,
  RIGHTCLICK: 2,
  BACKSPACE: 8,
  TAB: 9,
  RETURN: 13,
  SHIFT: 16,
  CONTROL: 17,
  ALT: 18,
  ESCAPE: 27,
  SPACE: 32,
  UP: 38,
  LEFT: 37,
  RIGHT: 39,
  DOWN: 40,
  K0: 48,
  K1: 49,
  K2: 50,
  K3: 51,
  K4: 52,
  K5: 53,
  K6: 54,
  K7: 55,
  K8: 56,
  K9: 57,
  A: 65,
  B: 66,
  C: 67,
  D: 68,
  E: 69,
  F: 70,
  G: 71,
  H: 72,
  I: 73,
  J: 74,
  K: 75,
  L: 76,
  M: 77,
  N: 78,
  O: 79,
  P: 80,
  Q: 81,
  R: 82,
  S: 83,
  T: 84,
  U: 85,
  V: 86,
  W: 87,
  X: 88,
  Y: 89,
  Z: 90,
  F1: 112,
  F2: 113,
  F3: 114,
  F4: 115,
  F5: 116,
  F6: 117,
  F7: 118,
  F8: 119,
  F9: 120,
  F10: 121,
  F11: 122,
  F12: 123,
  /*
                  -- Inconsisten Keys --
      These keys aren't consistent through all browser
  */
  MINUS: 189,
  EQUAL: 197
};
// }}}

mousePos = {};
leftDown = false;
rightDown = false;

// Keyboard down event --------------------------------------------------- {{{
addEventListener("keydown", function (e) {
  var keyCode = e.keyCode;
  keys[keyCode] = true;
}, false);
// }}}
// Keyboard up event ----------------------------------------------------- {{{
addEventListener("keyup", function (e) {
  var keyCode = e.keyCode;
  delete keys[keyCode];
}, false);
// }}}
// Mouse movement event -------------------------------------------------- {{{
addEventListener("mousemove", function (e) {
  mousePos = getMousePos(mainComp.canvas, e);
}, false);
// }}}
// Disable context menu for right clicks --------------------------------- {{{
addEventListener("contextmenu", function (e) {
  if (e.button === 2) {
    e.preventDefault();
  }
}, false);
// }}}
// Mouse click events ---------------------------------------------------- {{{
addEventListener("mousedown", function (e) {
  if (e.button === KeyEvents.LEFTCLICK)         // Left click
    leftDown = true;
  else if (e.button === KeyEvents.RIGHTCLICK)   // Right click
    rightDown = true;
}, false);
// }}}
// Mouse release events -------------------------------------------------- {{{
addEventListener("mouseup", function (e) {
  if (e.button === KeyEvents.LEFTCLICK)         // Left click
    leftDown = false;
  else if (e.button === KeyEvents.RIGHTCLICK)   // Right click
    rightDown = false;
}, false);
// }}}
// Gets mouse position relative to the top-left corner of the canvas ----- {{{
function getMousePos(c, e) {
  var rect = c.getBoundingClientRect();
  return {
    x: e.clientX - rect.left,
    y: e.clientY - rect.top
  };
}
// }}}
// Window focus event ---------------------------------------------------- {{{
addEventListener("focus", function (e) {
  mainComp.start();
}, false);
// }}}
// Window blur event ----------------------------------------------------- {{{
addEventListener("blur", function (e) {
  mainComp.stop();
}, false);
// }}}
// Screen Resizing ------------------------------------------------------- {{{
addEventListener("resize", function (e) {
  mainComp.resizeCanvas();
}, false);
// }}}
