/** @class */
class Controls {
  /**
   * @constructor
   * @memberof Controls
   */
  constructor () {
    // Game keys
    this.up = false;                        // Up key
    this.down = false;                      // Down key
    this.left = false;                      // Left key
    this.right = false;                     // Right key
    this.shoot = false;                     // Shoot key
    this.special = false;                   // Special key
    this.debugKey = false;
    this.menu = false;

    // Toggle left click
    this.leftClick = false;
    this.leftSwitched = false;

    // Toggle right click
    this.rightClick = false;
    this.rightSwitched = false;
  }
  /**
   * Updates the variables of the controls.
   * @method tick
   * @memberof Controls
   * @param {object} inputHandler InputHandler instance.
   */
  tick (inputHandler) {
    // Handle input keys
    this.up = keys[KeyEvents.W] || keys[KeyEvents.UP];
    this.down = keys[KeyEvents.S] || keys[KeyEvents.DOWN];
    this.left = keys[KeyEvents.A] || keys[KeyEvents.LEFT];
    this.right = keys[KeyEvents.D] || keys[KeyEvents.RIGHT];
    this.shoot = leftDown;
    this.special = this.rightClick;

    // Toggle left click
    if (leftDown) {
      if (!this.leftSwitched) {
        this.leftSwitched = true;
        this.leftClick = true;
      } else
        this.leftClick = false;
    } else
      this.leftSwitched = false;

    // Toggle right click
    if (rightDown) {
      if (!this.rightSwitched) {
        this.rightSwitched = true;
        this.rightClick = true;
      } else
        this.rightClick = false;
    } else
      this.rightSwitched = false;

    this.debugKey = keys[KeyEvents.P];
    keys[KeyEvents.P] = false;

    this.menu = keys[KeyEvents.ESCAPE];
    keys[KeyEvents.ESCAPE] = false;
  }
}
