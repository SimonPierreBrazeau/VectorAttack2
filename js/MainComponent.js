/** @class */
class MainComponent {
  /**
   * @constructor
   * @memberof MainComponent
   */
  constructor () {
    this.resizable = false;

    // Default canvas size
    this.DEFAULT_WIDTH = 1000;
    this.DEFAULT_HEIGHT = 610;

    // Dimensions of the canvas
    if (this.resizable) {
      this.WIDTH = window.innerWidth;
      this.HEIGHT = window.innerHeight;
    } else {
      this.WIDTH = this.DEFAULT_WIDTH;
      this.HEIGHT = this.DEFAULT_HEIGHT;
    }

    // Toggle for running the engine (pause switch)
    this.running = true;

    // FPS Meter
    this.fpsCounter = new FPScounter();
    this.logFPS = false;
    this.fpsCounter.startCounter();
    this.fpsCount = 0;

    // --------------------------- //
    // ---< MAIN CANVAS STUFF >--- //
    // --------------------------- //

    // Create the canvas and put it on the page
    this.canvas = document.createElement("Canvas");
    document.body.appendChild(this.canvas);
    // Get the 2D context from the canvas and identifie it
    this.ctx = this.canvas.getContext("2d");
    this.ctx.canvas.id = "VA2Canvas";
    // Set the size of the canvas
    this.resizeCanvas();
    // Disable image smoothing on Firefox and Chrome
    this.ctx.mozImageSmoothingEnabled = false;
    this.ctx.imageSmoothingEnabled = false;
    // Hide the mouse cursor when over the canvas
    this.canvas.style.cursor = "none";

    // ------------------------- //
    // ---< END MAIN CANVAS >--- //
    // ------------------------- //

    // Create a game instance
    this.game = new Game();

    // Create game screen instance
    this.gameScreen = new GameScreen();

    // Create sound engine instance
    this.soundEngine = new SoundEngine();
    this.soundsMuted = true;
    this.musicMuted = true;

    // Start the engine
    this.start();
    this.run();
  }
  /**
   * Starts or resumes the engine.
   * @method start
   * @memberof MainComponent
   */
  start () {
    if (this.running)
      return;
    this.running = true;
    if (this.logFPS)
      this.fpsCounter.reset();
    console.log("Resuming game");
  }
  /**
   * Pauses the engine.
   * @method stop
   * @memberof MainComponent
   */
  stop () {
    if (!this.running)
      return;
    this.running = false;
    console.log("Game paused!");
  }
  /**
   * Main loop of the engine.
   * @method run
   * @memberof MainComponent
   */
  run () {
    if (this.running) {
      // Check if sounds are still paused
      if (this.soundEngine.paused) this.soundEngine.resumeSounds();

      // Update the fps counter
      var fps = this.fpsCounter.stopAndPost(this.logFPS);
      if (fps != -1) this.fpsCount = fps;

      // update the main components of the game
      this.tick();
      this.render();
    } else
      this.soundEngine.pauseSounds();
  }
  /**
   * Updates the game's variables and child objects' variables.
   * @method tick
   * @memberof MainComponent
   */
  tick () {
    this.game.tick(this.inputHandler, this.fpsCount, this.soundEngine);
    this.soundEngine.tick(this.soundsMuted, this.musicMuted);
  }
  /**
   * Goes in the input handler and toggles a key down. This is done that way
   * because the event listener's "this" is the whole window instead of being
   * the input handler itself.
   * @method keyPress
   * @memberof MainComponent
   * @param {number} key Key code of the key being pressed.
   */
  keyPress (key) {
    this.inputHandler.keys[key] = true;
  }
  /**
   * Goes in the input handler and toggles a key up. This is done that way
   * because the event listener's "this" is the whole window instead of being
   * the input handler itself.
   * @method keyRelease
   * @memberof MainComponent
   * @param {number} key Key code of the key being released.
   */
  keyRelease (key) {
    delete this.inputHandler.keys[key];
  }
  /**
   * Changes the canvas dimensions to either the window's size or the default
   * size, depending on if the game is set to be resizable.
   * @method resizeCanvas
   * @memberof MainComponent
   */
  resizeCanvas () {
    if (this.resizable) {
      this.WIDTH = window.innerWidth;
      this.HEIGHT = window.innerHeight;
    } else {
      this.WIDTH = this.DEFAULT_WIDTH;
      this.HEIGHT = this.DEFAULT_HEIGHT;
    }

    if (this.canvas.width != this.WIDTH)
      this.canvas.width = this.WIDTH;

    if (this.canvas.height != this.HEIGHT)
      this.canvas.height = this.HEIGHT;
  }
  /**
   * Renders the game elements to the canvas context.
   * @method render
   * @memberof MainComponent
   */
  render () {
    this.gameScreen.render(this.game, this.ctx, this.WIDTH, this.HEIGHT);
    this.gameScreen.renderGUI(this.game, this.ctx, this.WIDTH, this.HEIGHT);
  }
  /**
   * Returns the canvas' width.
   * @method getWidth
   * @memberof MainComponent
   * @static
   * @returns {number}
   */
  static getWidth () {
    return document.getElementById("VA2Canvas").width;
  }
  /**
   * Returns the canvas' height.
   * @method getHeight
   * @memberof MainComponent
   * @static
   * @returns {number}
   */
  static getHeight () {
    return document.getElementById("VA2Canvas").height;
  }
  /**
   * Returns the version number of the product.
   * @method getVersion
   * @memberof MainComponent
   * @static
   * @returns {string}
   */
  static getVersion () {
    return "VA2-pre1.1";
  }
}
