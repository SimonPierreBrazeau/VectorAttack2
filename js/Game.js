/** @class */
class Game {
  /**
   * @constructor
   * @memberof Game
   */
  constructor () {
    this.startingLevel = new MainMenu();    // Level object
    this.menu = undefined;                  // Menu object
    this.levelIsMenu = false;
    this.currentLevel = undefined;

    // Game Controls (input)
    this.controls = new Controls();

    this.time = 0;

    this.renderScale = 1;                   // Scale of render

    this.renderDebugInfo = false;           // Render debugging info?

    this.fpsCount = 0;                      // Frame render rate
  }
  /**
   * Updates the game's variables. This includes the current level and the 
   * controls.
   * @method tick
   * @memberof Game
   * @param {object} inputHandler InputHandler instance.
   * @param {number} fpsCount     Last calculated frame rate.
   * @param {object} soundEngine  SoundEngine instance.
   */
  tick (inputHandler, fpsCount, soundEngine) {
    this.time++;
    this.fpsCount = fpsCount;

    this.currentLevel = this.setCurrentLevel(this.startingLevel);
    this.removeDoneLevels();

    // Remove the next level if it is done. This deletes the pause menu if it's
    // done.
    if (this.currentLevel.next != null)
      if (this.currentLevel.next.done)
        this.currentLevel.next = null;

    this.controls.tick(inputHandler);

    if (this.controls.debugKey) {
      if (this.renderDebugInfo) this.renderDebugInfo = false;
      else this.renderDebugInfo = true;

      this.controls.debugKey = false;
    }

    // Reset the keys (used in menus for toggled keys)
    // keys[KeyEvents.W] = keys[KeyEvents.UP] = false;   // Toggle up

    // Update the menu if defined
    this.currentLevel.tick(this.controls, mousePos, this.renderScale,
      soundEngine);
  }
  /**
   * Recursive function that sets the current level to the bottom-most level in 
   * the level tree.
   * @method setCurrentLevel
   * @memberof Game
   * @param {object} level Level instance to be verified.
   * @returns {object}
   */
  setCurrentLevel (level) {
    if (level.next != null) {
      var r = this.setCurrentLevel(level.next);
      if (r != null)
        return r;
    }

    if (level.done)
      return null;
    else
      return level;
  }
  /**
   * Removes level objects that are set as done in the whole level tree.
   * @method removeDoneLevels
   * @memberof Game
   */
  removeDoneLevels () {
    var currentLevel = this.startingLevel;

    while (currentLevel.next != null) {
      currentLevel = currentLevel.next;
      if (!currentLevel.done) {
        this.startingLevel.next = currentLevel;
        break;
      }
    }
  }
  /**
   * Returns the canvas width scaled by the render scale.
   * @method getScaledWidth
   * @memberof Game
   * @returns {number}
   */
  getScaledWidth () {
    return MainComponent.getWidth() / this.renderScale;
  }
  /**
   * Returns the canvas height scaled by the render scale.
   * @method getScaledHeight
   * @memberof Game
   * @returns {number}
   */
  getScaledHeight () {
    return MainComponent.getHeight() / this.renderScale;
  }
  /**
   * Calculates the angle from a given point of origins to a target point and 
   * returns it.
   * @method calculateAngle
   * @memberof Game
   * @static
   * @param {number} ox X position of origin
   * @param {number} oy Y position of origin
   * @param {number} tx X position of target
   * @param {number} ty Y position of target
   * @returns {number}
   */
  static calculateAngle (ox, oy, tx, ty) {
    var angle = 0;

    // Grab the range between to points
    var rangex = tx - ox;
    var rangey = ty - oy;
    var erange = Math.sqrt(Math.pow(Math.abs(rangex), 2) +
                           Math.pow(Math.abs(rangey), 2));

    // Grab the angle
    var dcos = Math.asin(Math.abs(rangex) / erange) / Math.PI * 2;
    var dsin = Math.acos(Math.abs(rangex) / erange) / Math.PI * 2;
    var dtan = Math.abs(Math.atan(rangex / rangey)) / Math.PI;

    if (rangex < 0) dcos = -dcos;
    if (rangey > 0) dsin = -dsin;

    // Calculate angle
    if (dsin >= 0 && dsin <= 1 && dcos >= 0 && dcos <= 1)
      angle = dtan;
    else if (dsin >= 0 && dsin <= 1 && dcos >= -1 && dcos <= 0)
      angle = 2 - dtan;
    else if (dsin >= -1 && dsin <= 0 && dcos >= -1 && dcos <= 0)
      angle = 1 + dtan;
    else
      angle = 1 - dtan;

    return angle;
  }
  /**
   * Takes a number and converts it to a separated string, then returns it.
   * @method scoreString
   * @memberof Game
   * @static
   * @param {string} string            Number string to be converted.
   * @param {string} [seperator = ","] String separator.
   * @returns {string}
   */
  static scoreString (string, separator = ",") {
    let s = "";

    for (var i = string.length - 1, l = 0; i > -1; i--, l++) {
      if (l % 3 == 0 && l > 0)
        s = separator + s;
      s = string.charAt(i) + s;
    }

    return s;
  }
}
