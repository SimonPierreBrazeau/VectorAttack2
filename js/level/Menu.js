/** @class */
class Menu {
  /**
   * @constructor
   * @memberof Menu
   */
  constructor () {
    this.model = new Model();
    this.entities = [];
    this.GUIentities = [];
    this.bullets = [];

    this.levelType = LevelID.MENU_LEVEL;
    this.ID = LevelID.MENU_LEVEL;

    // If there's a level here, next will be executed instead of
    // the current level.
    this.next = null;
    this.done = false;

    // Music to add
    this.music = null;
    this.musicSet = false;
  }
  /**
   * Updates variables of the menu. This includes entities and GUI elements.
   * @method tick
   * @memberof Menu
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    // Check if music was set
    if (this.music != null && !this.musicSet) {
      // Same song as currently playing?
      if (soundEngine.playingMusic.getAttribute("src") != this.music) {
        soundEngine.addMusic(this.music);
        this.musicSet = true;
      }
    }

    for (var i = 0; i < this.entities.length; i++)
      this.entities[i].tick();

    for (var i = 0; i < this.GUIentities.length; i++)
      this.GUIentities[i].tick(controls.leftClick, mousePos, renderScale, 
        soundEngine);
  }
}
