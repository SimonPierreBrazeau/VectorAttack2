var LevelID = {
  MAIN_MENU: -1,
  GAME_LEVEL: 0,
  WAVES: 1,
  DIAMOND_RUSH: 2,
  SUPER_LINES: 3,
  TRAVELING_MADNESS: 4,
  MENU_LEVEL: 20,
  PAUSE_MENU: 21,
  GAME_OVER_MENU: 22,
  SETTINGS_MENU: 23,
  EMPTY_LEVEL: 30,
  TEST_LEVEL_01: 31,
  TEST_LEVEL_02: 32,
  TEST_LEVEL_SELECT: 33
};

/** @class */
class Levels {
  /**
   * Returns a string containing the name of a given level ID.
   * @method getName
   * @memberof Levels
   * @static
   * @returns {string}
   */
  static getName (levelID) {
    switch (levelID) {
      case LevelID.MAIN_MENU: return "Main menu";
      case LevelID.WAVES: return "Waves";
      case LevelID.DIAMOND_RUSH: return "Diamond rush";
      case LevelID.SUPER_LINES: return "Super lines";
      case LevelID.TRAVELING_MADNESS: return "Traveling madness";
      case LevelID.PAUSE_MENU: return "Pause menu";
      case LevelID.GAME_OVER_MENU: return "Game over menu";
      case LevelID.SETTINGS_MENU: return "Settings menu";
      case LevelID.EMPTY_LEVEL: return "Empty level";
      case LevelID.TEST_LEVEL_01: return "Test level 01";
      case LevelID.TEST_LEVEL_02: return "Test level 02";
      case LevelID.TEST_LEVEL_SELECT: return "Test level select";
      default: return "Unknown level";
    }
  }
  /**
   * Returns the level object corresponding to the given level ID.
   * @method getLevel
   * @memberof Levels
   * @static
   * @returns {object}
   */
  static getLevel (levelID) {
    switch (levelID) {
      case LevelID.MAIN_MENU: return new MainMenu();
      case LevelID.WAVES: return new WavesLevel();
      case LevelID.DIAMOND_RUSH: return new DiamondsLevel();
      case LevelID.SUPER_LINES: return new SuperLinesLevel();
      case LevelID.TRAVELING_MADNESS: return new TravelingMadnessLevel();
      case LevelID.SETTINGS_MENU: return new SettingsMenu();
      case LevelID.EMPTY_LEVEL: return new EmptyLevel();
      case LevelID.TEST_LEVEL_01: return new TestLevel01();
      case LevelID.TEST_LEVEL_02: return new TestLevel02();
      case LevelID.TEST_LEVEL_SELECT: return new TestLevelSelect();
      default: console.error("Given level ID does not exist!");
    }
  }
}