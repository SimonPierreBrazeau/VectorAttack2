/** @class */
class Level {
  /**
   * @constructor
   * @memberof Level
   * @param {boolean} [checkLives = true] Level checks lives to end game.
   * @param {number} [size = 1000]        Size of the level.
   */
  constructor (checkLives = true, size = 1000) {
    this.width = size;
    this.height = size;
    this.levelType = LevelID.GAME_LEVEL;
    this.ID = LevelID.GAME_LEVEL;
    this.next = null;
    this.done = false;
    this.playerWeaponSet = false;

    this.player = new Player();
    this.lives = 2;
    this.score = 0;
    this.rawScore = 0;
    this.multiplier = 1;
    this.bombCount = 3;

    // Weapon changer
    this.changeWeaponByScore = true;
    this.lastScoreWeaponChange = 0;
    this.scoreWeaponChangeSpan = 1000;
    this.scoreWeaponChange = this.scoreWeaponChangeSpan;

    // Level settings
    this.weaponResetOnDeath = true;
    this.defaultPlayerWeapon = WeaponID.NARROW;
    this.multiplierResetOnDeath = true;

    // Multiplier affected by score instead of gems?
    this.multiplierBasedOnScore = false;
    this.multiplierScoreChangeSpan = 500;
    this.multiplierScoreChange = this.multiplierScoreChangeSpan;

    // Game ends with lives or with time?
    this.checkLives = checkLives;
    this.timerID = null;      // Holds the timer ID for setInterval
    this.gameTime = 120;      // Amount of time until end of game (seconds)
    this.prevGameTime = -1;         // For playing sound effect

    this.canShoot = true;     // Allow the player to shoot?

    // Bombs
    this.canDropBombs = true;
    this.gainBombs = true;
    this.gainBombScoreSpan = 3000;
    this.gainBombScore = this.gainBombScoreSpan;

    // Does the player gain lives by scoring points? How many points?
    this.gainLives = true;
    this.gainLivesScoreSpan = 4000;
    this.gainLivesScore = this.gainLivesScoreSpan;

    // Sequence variables
    this.deadKilled = false;      // Death sequence killed all entities
    this.exposeSeq = false;
    this.nothingSeq = false;
    this.deadTime = 0;
    this.deadSpan = 60;
    this.nothingSpan = 60;
    this.nothingTime = this.nothingSpan;

    this.spawnAllowed = true;

    this.model = new Model();

    this.killFound = false;

    this.entities = [];
    this.ghosts = [];
    this.GUIentities = [];
    this.bullets = [];
    this.bombs = [];

    this.collidableTypes = Entities.getPlayerCollidables();

    this.GUIentities.push(new Label("score", 10, 30, "", 20, 1));
    Label.find("score", this.GUIentities).model.text[0].color = "#0EF";
    this.GUIentities.push(new Label("multiplier", 10, 45, "", 12, 1));
    Label.find("multiplier", this.GUIentities).model.text[0].color = "#0EF";

    // Life counter
    this.GUIentities.push(new Icon("life1", 0, 0,
      Model.createPlayer(5, false)));
    this.GUIentities.push(new Icon("life2", 0, 0,
      Model.createPlayer(5, false)));
    this.GUIentities.push(new Icon("life3", 0, 0,
      Model.createPlayer(5, false)));
    this.GUIentities.push(new Icon("life4", 0, 0,
      Model.createPlayer(5, false)));
    this.GUIentities.push(new Icon("life5", 0, 0,
      Model.createPlayer(5, false)));
    this.GUIentities.push(new Label("lifeCounter", 0, 0, "x" + this.lives,
      20, 2));

    // Bomb counter
    this.GUIentities.push(new Icon("bomb1", 0, 0,
      Model.createBomb(5, false)));
    this.GUIentities.push(new Icon("bomb2", 0, 0,
      Model.createBomb(5, false)));
    this.GUIentities.push(new Icon("bomb3", 0, 0,
      Model.createBomb(5, false)));
    this.GUIentities.push(new Icon("bomb4", 0, 0,
      Model.createBomb(5, false)));
    this.GUIentities.push(new Icon("bomb5", 0, 0,
      Model.createBomb(5, false)));
    this.GUIentities.push(new Label("bombCounter", 0, 0, "x" + this.lives,
      20, 2, "#ffb13b"));

    if (!this.checkLives)
      this.GUIentities.push(new Button("gameTime", 0, 0, 140, 50, this.gameTime,
        20, false));

    // Music to add
    this.music = null;
    this.musicSet = false;

    this.soundEngine = null;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof Level
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    // Check if music was set
    if (this.music != null && !this.musicSet) {
      soundEngine.addMusic(this.music);
      this.musicSet = true;
    }

    this.soundEngine = soundEngine;

    // Check if the player weapon was set to the default one
    if (!this.playerWeaponSet) {
      this.player.weaponType = this.defaultPlayerWeapon;
      this.playerWeaponSet = true;
    }

    // Check if a timer has been generated for game timer
    if (!this.checkLives && this.timerID == null) {
      this.timerID = setInterval(Level.updateTimer, 1000, this);
    }

    // Reposition GUI
    Label.find("score", this.GUIentities).reposition(10 * renderScale,
      30 * renderScale);
    Label.find("multiplier", this.GUIentities).reposition(10 * renderScale,
      45 * renderScale);

    if (!this.checkLives || this.gameTime == 0) {
      this.tickGameTime(renderScale);

      if (this.gameTime <= 10)
        if (this.gameTime != this.prevGameTime) {
          soundEngine.addSound(SoundID.TIME_RUNNING_OUT);
          this.prevGameTime = this.gameTime
        }
    }

    // Check for menu keys
    if (controls.menu)
      this.next = new PauseMenu(this);

    // Check for weapon cycle
    if (this.changeWeaponByScore) {
      if (this.rawScore >= this.scoreWeaponChange) {
        this.scoreWeaponChange += this.scoreWeaponChangeSpan;
        let wep = Math.round(Math.random() * 2) + 1;
        this.player.weaponType = wep;
      }
    }

    // Check for multiplier growth based on score
    if (this.multiplierBasedOnScore)
      if (this.rawScore >= this.multiplierScoreChange) {
        this.multiplier++;
        this.multiplierScoreChange += this.multiplierScoreChangeSpan;
        this.entities.push(new ScorePop("x" + this.multiplier,
          this.player.x, this.player.y, "#FB0"));
      }

    // Check for gaining lives
    if (this.checkLives) {
      if (this.gainLives)
        if (this.rawScore >= this.gainLivesScore) {
          this.lives++;
          this.gainLivesScore += this.gainLivesScoreSpan;
          this.entities.push(new ScorePop("1 UP!",
            this.player.x, this.player.y - 35, "#e300ff"));
          soundEngine.addSound(SoundID.GAIN_BL);
        }

      this.tickLifeCounter(renderScale);
    }

    if (this.canDropBombs) {
      if (this.gainBombs) {
        if (this.rawScore >= this.gainBombScore) {
          this.bombCount++;
          this.gainBombScore += this.gainBombScoreSpan;
          this.entities.push(new ScorePop("Bomb Up!",
            this.player.x, this.player.y - 50, "#ffb13b"));
          soundEngine.addSound(SoundID.GAIN_BL);
        }
      }

      this.tickBombCounter(renderScale);
    }

    // Update the player
    this.player.tick(controls, mousePos, this, soundEngine);

    // Check for player ghosts
    while (this.player.spawner.length > 0) {
      this.ghosts.push(this.player.spawner[0]);
      this.player.spawner.splice(0, 1);
    }

    // Check if we can spawn stuff
    if (this.player.isDying || this.player.isSpawning)
      this.spawnAllowed = false;
    else
      this.spawnAllowed = true;

    // Update bombs;
    for (var i = 0; i < this.bombs.length; i++) {
      this.bombs[i].tick();

      if (!this.bombs[i].isAlive) {
        this.bombs.splice(i, 1);
        i--;
        continue;
      }
    }

    // Update ghosts
    for (var i = 0; i < this.ghosts.length; i++) {
      let g = this.ghosts[i];
      g.tick();

      if (!g.isAlive) {
        this.ghosts.splice(i, 1);
        i--;
      }
    }

    // Check if the player is dead
    if (this.player.isDying) {
      if (!this.deadKilled) {
        // Kill all the entities expect the killer
        for (var i = 0; i < this.entities.length; i++) {
          var en = this.entities[i];
          if (!en.isKiller) {
            if (this.collidableTypes.indexOf(en.entityType) > -1) {
              this.createDeadEntity(en.x, en.y, en, en.dirFacing);
              this.entities.splice(i, 1);
              i--;
            } else if (en.entityType != EntityID.DEAD) {
              this.entities.splice(i, 1);
              i--;
            }
          }
        }

        // Kill all the bullets
        for (var i = 0; i < this.bullets.length; i++) {
          var bu = this.bullets[i];
          this.createDeadEntity(bu.x, bu.y, bu, bu.dirFacing);
          this.bullets.splice(i, 1);
          i--;
        }

        // Reset level variables
        if (this.weaponResetOnDeath) {
          this.player.weaponType = this.defaultPlayerWeapon;
          if (this.player.weaponRevertable) {
            this.player.previousWeapon = null;
            this.player.weaponRevertTime = this.player.weaponRevertSpan;
            this.player.weaponRevertable = false;
          }
        }

        this.player.makeInvulnerable();

        if (this.multiplierResetOnDeath) this.multiplier = 1;

        // Start the timer
        this.deadTime = this.deadSpan + 1;

        this.deadKilled = true;
        this.exposeSeq = true;
        this.nothingSeq = false;
      }
      // Update dead entities
      for (var i = 0; i < this.entities.length; i++) {
        var en = this.entities[i];

        en.tick(this);
        if (!en.isAlive) {
          this.entities.splice(i--, 1);
          soundEngine.addSound(SoundID.ENEMY_DEATH);
        }
      }

      // Spawning Sequences
      if (this.exposeSeq) {
        // Update timer
        this.deadTime--;

        // Check the timer
        if (this.deadTime <= 0) {
          // Kill player and killer
          for (var i = 0; i < this.entities.length; i++) {
            var en = this.entities[i];
            if (en.isKiller) {
              this.createDeadEntity(en.x, en.y, en, en.dirFacing);
              this.entities.splice(i--, 1);
              break;
            }
          }

          // Respawn player
          this.createDeadEntity(this.player.x, this.player.y, this.player,
                                this.player.dirFacing);

          this.nothingTime = this.nothingSpan;
          this.player.model.visible = false;
          this.exposeSeq = false;
          this.nothingSeq = true;
          soundEngine.addSound(SoundID.PLAYER_DEATH);

        }
      } else if (this.nothingSeq) {
        this.nothingTime--;

        if (this.nothingTime <= 0) {
          if (this.checkLives) {
            this.lives--;
            if (this.lives < 0) {
              this.done = true;
              this.next = new GameOver(this.score, this.ID);
            }
          }

          this.player.isSpawning = true;
          this.player.isDying = false;
          this.player.model.visible = true;
          this.player.dirHeading = 0;

          // Reset the death sequence
          this.deadKilled = false;
          this.nothingSeq = false;
        }
      }

    } else {
      // ------ MAIN TICKING SEQUENCE ------ //
      // Update the bullets
      for (var i = 0; i < this.bullets.length; i++) {
        var bu = this.bullets[i];
        bu.tick(this);

        // Spawn entities
        while (bu.spawner.length > 0) {
          if (bu.spawner[0].entityType == EntityID.GHOST)
            this.ghosts.push(bu.spawner[0]);
          else
            this.entities.push(bu.spawner[0]);

          bu.spawner.splice(0, 1);
        }

        if (!bu.isAlive) {
          this.createDeadEntity(bu.x, bu.y, bu, bu.dirFacing);

          if (bu.hitWall)
            soundEngine.addSound(SoundID.WALL_HIT);

          this.bullets.splice(i, 1);
          i--;
        }
      }

      // Update the entities
      for (var j = 0; j < this.entities.length; j++) {
        var ue = this.entities[j];        // Entity being updated

        // Update the entity
        ue.tick(this);

        // Spawn every entity in the array
        if (ue.superType == EntityID.ENEMY) {
          while (ue.spawner.length > 0) {
            if (ue.spawner[0].entityType == EntityID.GHOST)
              this.ghosts.push(ue.spawner[0]);
            else
              this.entities.push(ue.spawner[0]);
            ue.spawner.splice(0, 1);
          }
        }

        // Check if it's dead
        if (!ue.isAlive) {
          if (ue.entityType == EntityID.SCORE_GEM) {
            if (ue.collected) {
              this.multiplier += ue.gemValue;
              this.createDeadEntity(ue.x, ue.y, ue, ue.dirFacing);
              this.entities.push(new ScorePop("x" + this.multiplier,
                                 this.player.x,
                                 this.player.y,
                                 "#FB0"));
              
              // Make sound effect
              soundEngine.addSound(SoundID.COLLECT_GEM);
            }
          } else if (ue.entityType == EntityID.GIANT) {
            // Spawn giant's entities
            this.spawnEntityCluster(ue.x, ue.y, 10, ue.width, ue.entitySpawn);
          }

          // Delete the entity
          this.entities.splice(j, 1);
          j--;
        } else if (ue.isDying) {
          if (this.collidableTypes.indexOf(ue.entityType) > -1) {
            // Create a dead entity
            this.createDeadEntity(ue.x, ue.y, ue, ue.dirFacing);
            if (!ue.gemDropped && ue.killedByPlayer) {
              // Calculate the added score
              var tScore = (ue.pointValue * this.multiplier);

              // Update the score
              this.score += tScore;
              this.rawScore += ue.pointValue;

              // Create the score pop
              this.entities.push(new ScorePop(tScore, ue.x, ue.y));

              // Create score gems
              if (!this.multiplierBasedOnScore) {
                var z = ue.gemValue;
                while (z > 0) {
                  var m = 0;

                  // Determine how many gems we can spawn
                  if (z > 5) m = z - 5;
                  else m = z;

                  // Determine how many we'll spawn
                  let a = Math.round(Math.random() * m);
                  if (a == 0) a = 1

                  // Spawn the gems
                  this.spawnGem(ue.x, ue.y, a);
                  z -= a;
                }

                ue.gemDropped = true;
              }
            }

            // Kill the entity
            ue.isAlive = false;

            // Play the sound effect
            soundEngine.addSound(SoundID.ENEMY_DEATH);
          }
        }
      }

      // Check collisions (bombs and entities)
      for (var i = 0; i < this.bombs.length; i++) {
        for (var j = 0; j < this.entities.length; j++) {
          let en = this.entities[j];
          if (en.isAlive && !en.isSpawning && !en.isDying && !en.isFading &&
            this.collidableTypes.indexOf(en.entityType) > -1)
          this.checkBombCollision(this.bombs[i], this.entities[j]);
        }
      }

      // Check collisions (player and enemy entities)
      for (var k = 0; k < this.entities.length; k++) {
        var en = this.entities[k];
        if (this.collidableTypes.indexOf(en.entityType) > -1 &&
            en.isAlive && !en.isSpawning && !en.isDying && !en.isFading &&
            !this.player.isSpawning && !this.player.isDying)
          this.checkCollisions(this.player, en);
      }

      // Check collisions (bullet and entities)
      for (var l = 0; l < this.bullets.length; l++) {
        for (var m = 0; m < this.entities.length; m++) {
          var bu = this.bullets[l];
          var en = this.entities[m];
          if (bu.isAlive && en.isAlive && !en.isSpawning && !en.isDying &&
              !en.isFading && this.collidableTypes.indexOf(en.entityType) > -1)
            this.checkCollisions(bu, en);
        }
      }

      // Update GUI entities
      for (var i = 0; i < this.GUIentities.length; i++) {
        var en = this.GUIentities[i];
        if (en.entityType == EntityID.BUTTON)
          en.tick(controls.leftClick, mousePos, renderScale, soundEngine);
        else if (en.identifier == "score")
          en.model.text[0].string = Game.scoreString(this.score.toString());
        else if (en.identifier == "multiplier")
          en.model.text[0].string = "x" + Game.scoreString(this.multiplier.toString());
      }
    }
  }
  /**
   * Updates the life counter (GUI element).
   * @method tickLifeCounter
   * @memberof Level
   * @param {number} renderScale Scale of rendering engine.
   */
  tickLifeCounter (renderScale) {
    var h = 30 * renderScale;
    var w = MainComponent.getWidth();
    var gap = 30 * renderScale;
    var lx = MainComponent.getWidth() - 45 * renderScale;

    // Reposition life icons and label
    for (var i = 1; i <= 5; i++) {
      let name = "life" + i;
      Icon.find(name, this.GUIentities).reposition(w - gap * i, h);
    }
    Label.find("lifeCounter", this.GUIentities).reposition(lx, h);
    Label.find("lifeCounter", this.GUIentities).model.text[0].string =
      this.lives + "x";

    if (this.lives <= 5) {
      Label.find("lifeCounter", this.GUIentities).model.visible = false;
      for (var i = 1; i <= 5; i++) {
        let name = "life" + i;
        Icon.find(name, this.GUIentities).model.visible = i - 1 < this.lives;
      }
    } else {
      Icon.find("life1", this.GUIentities).model.visible = true;
      Label.find("lifeCounter", this.GUIentities).model.visible = true;
      for (var i = 2; i <= 5; i++) {
        let name = "life" + i;
        Icon.find(name, this.GUIentities).model.visible = false;
      }
    }
  }
  /**
   * Updates the bomb counter (GUI element).
   * @method tickBombCounter
   * @memberof Level
   * @param {number} renderScale Scale of rendering engine.
   */
  tickBombCounter (renderScale) {
    var h = 60 * renderScale;
    var w = MainComponent.getWidth();
    var gap = 30 * renderScale;
    var lx = MainComponent.getWidth() - 45 * renderScale;

    if (!this.checkLives || this.lives == 0) h -= 25;

    // Reposition life icons and label
    for (var i = 1; i <= 5; i++) {
      let name = "bomb" + i;
      Icon.find(name, this.GUIentities).reposition(w - gap * i, h);
    }
    Label.find("bombCounter", this.GUIentities).reposition(lx, h);
    Label.find("bombCounter", this.GUIentities).model.text[0].string =
      this.bombCount + "x";

    if (this.bombCount <= 5) {
      Label.find("bombCounter", this.GUIentities).model.visible = false;
      for (var i = 1; i <= 5; i++) {
        let name = "bomb" + i;
        Icon.find(name, this.GUIentities).model.visible = i - 1 < this.bombCount;
      }
    } else {
      Icon.find("bomb1", this.GUIentities).model.visible = true;
      Label.find("bombCounter", this.GUIentities).model.visible = true;
      for (var i = 2; i <= 5; i++) {
        let name = "bomb" + i;
        Icon.find(name, this.GUIentities).model.visible = false;
      }
    }
  }
  /**
   * Updates the game timer (GUI element).
   * @method tickGameTime
   * @memberof Level
   * @param {number} renderScale Scale of rendering engine.
   */
  tickGameTime (renderScale) {
    let w = MainComponent.getWidth() / 2;
    let h = 20 * renderScale;

    Button.find("gameTime", this.GUIentities).reposition(w, h);

    // Format the string
    let s = Math.floor(this.gameTime / 60) + ":";
    let ss = this.gameTime % 60;

    if (ss < 10)
      ss = "0" + ss;

    s += ss;
    Button.find("gameTime", this.GUIentities).model.text[0].string = s;

    if (this.gameTime <= 10)
      Button.find("gameTime", this.GUIentities).model.text[0].color = "#ffb13b";
  }
  /**
   * Updates the given level's game timer.
   * @memberof Level
   * @method updateTimer
   * @static
   * @param {object} level Level instance to update the timer.
   */
  static updateTimer (level) {
    if (level.done)
      clearInterval(level.timerID);

    if (mainComp.running && level.next == null) {
      level.gameTime--;
      if (level.gameTime <= 0) {
        level.checkLives = true;
        level.lives = 0;
        level.player.isDying = true;

        clearInterval(level.timerID);
      }
    }
  }
  /**
   * Creates a bullet object and adds it to the bullets array.
   * @method createBullet
   * @memberof Level
   * @param {number} x                   X position.
   * @param {number} y                   Y position.
   * @param {number} direction           Direction of bullet.
   * @param {number} speed               Traveling speed.
   * @param {boolean} [ghosting = false] Bullet has ghosting effect.
   */
  createBullet (x, y, direction, speed, ghosting = false) {
    this.bullets.push(new Bullet(x, y, direction, speed, ghosting));
  }
  /**
   * Creates an entity at a random location on the level and appends it to the
   * entities array. If no direction is given, it will be random as well.
   * @method spawnEntityAtRandom
   * @memberof Level
   * @param {string} entityType     Type of entity to create.
   * @param {number} [dir = random] Direction of entity.
   */
  spawnEntityAtRandom (entityType, dir = Math.random() * 2) {
    let x = Math.random() * (this.width * 0.8) - (this.width / 2);
    let y = Math.random() * (this.height * 0.8) - (this.height / 2);

    this.spawnEntityAtPosition(entityType, x, y, dir);
  }
  /**
   * Creates multiple entities at a random location on the level. These 
   * entities are added to the entities array. if no direction is given,
   * it will be random as well.
   * @method spawnEntitiesAtRandom
   * @memberof Level
   * @param {number} amount       Amount of entities to create.
   * @param {string} entityType   Type of entity to create.
   * @param {number} [dir = null] Direction of entity.
   */
  spawnEntitiesAtRandom (amount, entityType, dir = null) {
    while (amount > 0) {
      if (dir != null)
        this.spawnEntityAtRandom(entityType, dir);
      else
        this.spawnEntityAtRandom(entityType);
      amount--;
    }
  }
  /**
   * Creates an entity at a specific position and adds it to the entities 
   * array. If the direction is not given, it will be random.
   * @method spawnEntityAtPosition
   * @memberof Level
   * @param {number} entityType     Type of entity to create.
   * @param {number} x              X position.
   * @param {number} y              Y position.
   * @param {number} [dir = random] Direction of entity.
   */
  spawnEntityAtPosition (entityType, x, y, dir = Math.random() * 2) {
    if (entityType == EntityID.TRAVELER)
      this.entities.push(new Traveler(x, y, dir));
    else if (entityType == EntityID.GOLDEN_TRAVELER)
      this.entities.push(new GoldenTraveler(x, y, dir));
    else {
      // Make sure that position is in level
      let safeDist = 30;
      if (x > this.width / 2 - safeDist) x = this.width / 2 - safeDist;
      else if (x < -this.width / 2 + safeDist) x = -this.width / 2 + safeDist;

      if (y > this.height / 2 - safeDist) y = this.height / 2 - safeDist;
      else if (y < -this.height / 2 + safeDist) y = -this.height / 2 + safeDist;

      if (entityType == EntityID.SPIRAL) 
        this.entities.push(new Spiral(x, y));
      else if (entityType == EntityID.DIAMOND) 
        this.entities.push(new Diamond(x, y));
      else if(entityType == EntityID.ARROW) 
        this.entities.push(new Arrow(x, y, dir));
      else if(entityType == EntityID.BLOCK) 
        this.entities.push(new Block(x, y));
      else if (entityType == EntityID.SCORE_GEM) 
        this.spawnGem(x, y);
      else if (entityType == EntityID.BOUNCER) 
        this.entities.push(new Bouncer(x, y, dir));
      else if (entityType == EntityID.SLIDER) 
        this.entities.push(new Slider(x, y));
      else if (entityType == EntityID.TRIANGLE) 
        this.entities.push(new Triangle(x, y));
      else if (entityType == EntityID.GIANT_SPIRAL)
        this.entities.push(new Giant(x, 
                                     y, 
                                     Model.createSpiral(75), 
                                     EntityID.SPIRAL, 
                                     dir));
      else if (entityType == EntityID.GIANT_DIAMOND)
        this.entities.push(new Giant(x, 
                                     y, 
                                     Model.createDiamond(75), 
                                     EntityID.DIAMOND, 
                                     dir));
      else if (entityType == EntityID.GIANT_SLIDER)
        this.entities.push(new Giant(x, 
                                     y, 
                                     Model.createSlider(75), 
                                     EntityID.SLIDER, 
                                     dir));
      else if (entityType == EntityID.GIANT_BOUNCER)
        this.entities.push(new Giant(x, 
                                     y, 
                                     Model.createBouncer(25), 
                                     EntityID.BOUNCER, 
                                     dir));
    }
    this.soundEngine.addSound(SoundID.ENEMY_SPAWN);
  }
  /**
   * Creates multiple entities at a specific location. These entities are 
   * added to the entities array. If the direction is not given, it will
   * be random.
   * @method spawnEntitiesAtPosition
   * @memberof Level
   * @param {number} amount       Amount of entities to create.
   * @param {string} entityType   Type of entity to create.
   * @param {number} x            X position.
   * @param {number} y            Y position.
   * @param {number} [dir = null] Direction of entity.
   */
  spawnEntitiesAtPosition (amount, entityType, x, y, dir = null) {
    while (amount > 0) {
      if (dir != null)
        this.spawnEntityAtPosition(entityType, x, y, dir);
      else
        this.spawnEntityAtPosition(entityType, x, y);
      amount--;
    }
  }
  /**
   * Creates an Arrow entity and adds it to the entities array. It's 
   * orientation depends on which quadrant it is created and will always
   * be facing the furthest edge of the level depending on if it's
   * horizontal or not.
   * @method spawnArrow
   * @memberof Level
   * @param {number} x           X position.
   * @param {number} y           Y position.
   * @param {boolean} horizontal Arrow going horizontal.
   */
  spawnArrow (x, y, horizontal) {
    var dir = 0;
    if (horizontal) {
      if (x > 0) dir = 1.5;
      else dir = 0.5;
    } else
      if (y < 0) dir = 1;

    this.spawnEntityAtPosition(EntityID.ARROW, x, y, dir);
  }
  /**
   * Creates an Arrow entity at a random location. This calls the spawnArrow
   * method to dictate the direction of the Arrow.
   * @method spawnRandomArrow
   * @memberof Level
   */
  spawnRandomArrow () {
    let x = Math.random() * (this.width * 0.7) - ((this.width * 0.7) / 2);
    let y = Math.random() * (this.height * 0.7) - ((this.height * 0.7) / 2);
    let r = Math.round(Math.random());
    let h = false;

    if (r == 0) h = true;

    this.spawnArrow(x, y, h);
  }
  /**
   * Creates a Block entity at a given position at a given size and adds it
   * to the entities array. If the size is not given, it will be randomized
   * the the Block constructor.
   * @method spawnBlock
   * @memberof Level
   * @param {number} x             X position of Block.
   * @param {number} y             Y position of Block.
   * @param {number} [size = null] Size of the Block.
   */
  spawnBlock (x, y, size = null) {
    if (size == null)
      this.spawnEntityAtPosition(EntityID.BLOCK, x, y);
    else
      this.entities.push(new Block(x, y, size));
  }
  /**
   * Creates a Block entity at a random position at a given size and adds it to
   * the entities array. If the size is not given, it will be randomized by the 
   * Block constructor.
   * @method spawnRandomBlock
   * @memberof Level
   * @param {number} [size = null] Size of the Block.
   */
  spawnRandomBlock (size = null) {
    let x = Math.random() * (this.width * 0.7) - ((this.width * 0.7) / 2);
    let y = Math.random() * (this.height * 0.7) - ((this.height * 0.7) / 2);

    // Make sure it stays inside the play field
    let s = size == null? 75: size / 2

    if (x > 0)
      x -= s;
    else if (x < 0)
      x += s;

    if (y > 0)
      y -= s;
    else if (y < 0)
      y += s;

    if (size == null)
      this.spawnBlock(x, y);
    else
      this.spawnBlock(x, y, size);
  }
  /**
   * Creates a ScoreGem entity at a given position that is worth a given 
   * amount of points and is added to the entities array.
   * @method spawnGem
   * @memberof Level
   * @param {number} x           X position.
   * @param {number} y           Y position.
   * @param {number} [value = 1] Value of the gem.
   */
  spawnGem (x, y, value = 1) {
    this.entities.push(new ScoreGem(x, y, value));
  }
  /**
   * Creates mutliple ScoreGem entities at a given position that are worth a
   * given number of points and adds them to the entities array.
   * @method spawnGems
   * @memberof Level
   * @param {number} amount      Amount of gems to create.
   * @param {number} x           X position.
   * @param {number} y           Y position.
   * @param {number} [value = 1] Value of the gems.
   */
  spawnGems (amount, x, y, value = 1) {
    while (amount > 0) {
      this.spawnGem(x, y, value);
      amount--;
    }
  }
  /**
   * Creates entities in a wall formation at a given position on the level. If 
   * randomDir is true, the entities will be facing different directions.
   * @method spawnEntityWall
   * @memberof Level
   * @param {number} x                    X position of center of wall.
   * @param {number} y                    Y position of center of wall.
   * @param {number} amount               Amount of entities in wall.
   * @param {number} space                Space between each entity.
   * @param {number} orientation          Orientation of the wall.
   * @param {number} entityType           Entity type to create.
   * @param {boolean} [randomDir = false] Spawning entities in different 
   *                                      directions.
   */
  spawnEntityWall (x, y, amount, space, orientation, entityType,
    randomDir = false) {
    for (var i = 0; i < amount; i++) {
      // Position of entity
      let g = i - ((amount - 1) / 2);
      let sx = x + Math.sin((orientation - 0.5) * Math.PI) * g * space;
      let sy = y - Math.cos((orientation - 0.5) * Math.PI) * g * space;

      if (!randomDir) {
        // Orientation of entity
        let d = orientation;
        this.spawnEntityAtPosition(entityType, sx, sy, d);
      } else
        this.spawnEntityAtPosition(entityType, sx, sy);
    }
  }
  /**
   * Creates a wall of entities at a given corner of the level. If randomDir is
   * true, the entities will be facing different directions. The entities are 
   * created facing accross the level. The corners are:
   *    1 = top-left;
   *    2 = bottom-right;
   *    3 = bottom-left;
   *    4 = top-left.
   * @method spawnEntityWallAtCorner
   * @memberof Level
   * @param {number} corner               Corner where wall is created.
   * @param {boolean} horizontal          Horizontal wall or vertical.
   * @param {number} amount               Amount of entities in wall.
   * @param {number} space                Space between each entity.
   * @param {number} entityType           Entity type to create.
   * @param {boolean} [randomDir = false] Spawning entities in different 
   *                                      directions.
   */
  spawnEntityWallAtCorner (corner, horizontal, amount, space, entityType,
    randomDir = false) {
    // Figure out what the coordinates are depending on the corner
    let x = 0;
    let y = 0;
    let m = (amount / 2) * space;
    let d = 0;

    if (horizontal) {
      if (corner == 1 || corner == 4) {
        d = 1;
        y = -(this.height / 2) + space;
      } else
        y = (this.height / 2) - space;

      if (corner == 1 || corner == 2)
        x = (this.width / 2) - m;
      else
        x = -(this.width / 2) + m;
    } else {
      if (corner == 1 || corner == 2) {
        d = 1.5;
        x = (this.width / 2) - space;
      } else {
        d = 0.5;
        x = -(this.width / 2) + space;
      }

      if (corner == 1 || corner == 4)
        y = -(this.height / 2) + m;
      else
        y = (this.height / 2) - m;
    }

    this.spawnEntityWall(x, y, amount, space, d, entityType, randomDir);
  }
  /**
   * Creates a circle of entities at a given position on the level and adds 
   * them to the entitites array. The entities created can be facing inside
   * the circle, outside or at a random direction.
   * @method spawnEntityRing
   * @memberof Level
   * @param {number} x                    X position of center of circle.
   * @param {number} y                    Y position of center of circle.
   * @param {number} amount               Amount of entities to create.
   * @param {number} radius               Radius of the circle.
   * @param {number} entityType           Type of entity to create.
   * @param {boolean} [randomDir = false] Entities go in random directions.
   * @param {boolean} [faceIn = false]    Entities facing inside the circle.
   */
  spawnEntityRing (x, y, amount, radius, entityType,
    randomDir = false, faceIn = true) {
    let rIncrement = 2 / amount;
    for (var i = 0; i < amount; i++) {
      let sx = x + Math.sin((rIncrement * i) * Math.PI) * radius;
      let sy = y - Math.cos((rIncrement * i) * Math.PI) * radius;

      if (!randomDir) {
        let d = (rIncrement * i);
        if (faceIn) d--;
        this.spawnEntityAtPosition(entityType, sx, sy, d);
      } else
        this.spawnEntityAtPosition(entityType, sx, sy);
    }
  }
  /**
   * Creates a given amount of entities in a cluster at a given position and 
   * adds them to the entities array. These entities are created at a random
   * position inside that cluster with a random direction.
   * @method spawnEntityCluster
   * @memberof Level
   * @param {number} x          X position of center of cluster.
   * @param {number} y          Y position of center of cluster.
   * @param {number} amount     Amount of entities to create.
   * @param {number} size       Size of the cluster.
   * @param {string} entityType Type of entity to create.
   */
  spawnEntityCluster (x, y, amount, size, entityType) {
    for (var i = 0; i < amount; i++) {
      let sx = (Math.random() * size) - (size / 2);
      let sy = (Math.random() * size) - (size / 2);

      let cx = x;
      let cy = y;

      // Make sure that the cluster is in the play field
      if (x + size / 2 > this.levelWidth / 2)
        cx -= (x + size / 2) - (this.levelWidth / 2);
      else if (x - size / 2 < -this.levelWidth / 2)
        cx += Math.abs((x - size / 2) + (-this.levelWidth / 2));

      if (y + size / 2 > this.levelHeight / 2)
        cy -= (y + size / 2) - (this.levelHeight / 2);
      else if (y - size / 2 < -this.levelHeight / 2)
        cy += Math.abs((y - size / 2) + (-this.levelHeight / 2));

      // Add random value
      cx += sx;
      cy += sy;

      this.spawnEntityAtPosition(entityType, cx, cy);
    }
  }
  /**
   * Creates Traveler entities outside the level at a given edge of it and
   * adds them to the entities array. GoldenTravelers can be created using this
   * method as well. The level edge IDs are:
   *    1 = top;
   *    2 = right;
   *    3 = bottom;
   *    4 = left.
   * @method spawnTravelers
   * @memberof Level
   * @param {number} amount            Amount of entities to create.
   * @param {boolean} [golden = false] Create GoldenTraveler.
   * @param {number} [edge = random]   Edge ID where entities are created.
   */
  spawnTravelers (amount, golden = false, edge = Math.ceil(Math.random() * 4)) {
    for (var i = 0; i < amount; i++) {
      let r = 0.2;
      let x = Math.random() * (this.width * 0.7) - ((this.width * 0.7) / 2);
      let y = Math.random() * (this.height * 0.7) - ((this.height * 0.7) / 2);
      let d = Math.random() * r;

      if (edge == 1) {
        y -= this.height;
        d += 1 - r / 2;
      } else if (edge == 2) {
        x += this.width;
        d += 1.5 - r / 2;
      } else if (edge == 3) {
        y += this.height;
        d -= r / 2;
      } else {
        x -= this.width;
        d += 0.5 - r / 2;
      }

      if (golden)
        this.spawnEntityAtPosition(EntityID.GOLDEN_TRAVELER, x, y, d);
      else
        this.spawnEntityAtPosition(EntityID.TRAVELER, x, y, d);
    }
  }
  /**
   * Creates Traveler entities outside the level at a given edge of it and
   * adds them to the entities array. GoldenTravelers can be created using this
   * method as well. The level edge IDs are:
   *    1 = top;
   *    2 = right;
   *    3 = bottom;
   *    4 = left.
   * @method spawnMixedTravelers
   * @memberof Level
   * @param {number} amount              Amount of entities to create.
   * @param {number} [goldenAmount = 0]  Create GoldenTraveler.
   * @param {number} [edge = random]     Edge ID where entities are created.
   */
  spawnMixedTravelers (amount, 
                       goldenAmount = 0, 
                       edge = Math.ceil(Math.random() * 4)) {
    this.spawnTravelers(amount, false, edge);
    this.spawnTravelers(goldenAmount, true, edge);
  }
  /**
   * Creates a BonusController entity at a given position and a given reward
   * and adds it to the entities array.
   * @method spawnBonusController
   * @memberof Level
   * @param {number} [bonusType = random] Bonus reward ID.
   * @param {number} [x = 0]              X position of controller.
   * @param {number} [y = 0]              Y position of controller.
   */
  spawnBonusController (bonusType = Weapons.getRandomBonusState(), x = 0,
    y = 0) {
    this.entities.push(new BonusController(x, y, bonusType, this));
    this.soundEngine.addSound(SoundID.BONUS_SPAWN);
  }
  /**
   * Creates a BonusPeg at a given position that is related to a given
   * BonusController and adds it to the entities array.
   * @method spawnBonusPeg
   * @memberof Level
   * @param {number} x               X position.
   * @param {number} y               Y position.
   * @param {object} bonusController BonusController object related to this.
   */
  spawnBonusPeg (x, y, bonusController) {
    this.entities.push(new BonusPeg(x, y, bonusController));
  }
  /**
   * Creates a Bomb entity at a given position and a given size and adds it
   * to the bombs array.
   * @method spawnBomb
   * @memberof Level
   * @param {number} x                 X position of center of explosion.
   * @param {number} y                 Y position of center of explosion.
   * @param {number} size              Size of explosion
   * @param {string} [color = #ffb13b] Color of explosion
   */
  spawnBomb (x, y, size, color = "#ffb13b") {
    this.bombs.push(new Bomb(x, y, size, color));
  }
  /**
   * Creates a PointSpawner entity at a given position and adds it to the 
   * entities array.
   * @method spawnPointSpawner
   * @memberof Level
   * @param {number} x          Y position.
   * @param {number} y          X position.
   * @param {number} amount     Amount of entities it will create.
   * @param {number} time       Amount of frames between entity creations.
   * @param {string} entityType Type of entities it will create.
   */
  spawnPointSpawner (x, y, amount, time, entityType) {
    this.entities.push(new PointSpawner(x, y, amount, time, entityType));
  }
  /**
   * Creates a RandomSpawner entity and adds it to the entities array.
   * @method spawnRandomSpawner
   * @memberof Level
   * @param {number} amount     Amount of entities it will create.
   * @param {number} time       Amount of frames between entity creations.
   * @param {string} entityType Type of entities it will create.
   */
  spawnRandomSpawner (amount, time, entityType) {
    this.entities.push(new RandomSpawner(amount, time, entityType));
  }
  /**
   * Checks if the two given entities are colliding using a box collision
   * model.
   * @method checkCollisions
   * @memberof Level
   * @param {object} entity1 First entity to compare. 
   * @param {object} entity2 Second entity to compare.
   */
  checkCollisions (entity1, entity2) {
    var x1 = entity1.x - entity1.width / 2;
    var xx1 = entity1.x + entity1.width / 2;
    var y1 = entity1.y - entity1.height / 2;
    var yy1 = entity1.y + entity1.height / 2;

    var x2 = entity2.x - entity2.width / 2;
    var xx2 = entity2.x + entity2.width / 2;
    var y2 = entity2.y - entity2.height / 2;
    var yy2 = entity2.y + entity2.height / 2;

    let pv = this.player.vulnerable;

    if (xx1 > x2 && x1 < xx2 && yy1 > y2 && y1 < yy2) {
        entity1.colliding(entity2.entityType, pv);
        entity2.colliding(entity1.entityType, pv);
    }
  }
  /**
   * Checks if a given bomb is colliding with a given entity using a circle to 
   * box collision model.
   * @method checkBombCollision
   * @memberof Level
   * @param {entity} bomb   Bomb to compare.
   * @param {entity} entity Entity to compare.
   */
  checkBombCollision (bomb, entity) {
    let ex = entity.x - entity.width / 2;
    let exx = entity.x + entity.width / 2;
    let ey = entity.y - entity.height / 2;
    let eyy = entity.y + entity.height / 2;

    let bx = bomb.x;
    let by = bomb.y;
    let bs = bomb.currentSize;

    // Calculate distance between each corner of entity to center of explosion
    // top-left  - 1
    // top-right - 2
    // bot-left  - 3
    // bot-right - 4
    let dist1 = Math.sqrt(Math.pow(Math.abs(ex - bx), 2) +
      Math.pow(Math.abs(ey - by), 2));
    let dist2 = Math.sqrt(Math.pow(Math.abs(exx - bx), 2) +
      Math.pow(Math.abs(ey - by), 2));
    let dist3 = Math.sqrt(Math.pow(Math.abs(ex - bx), 2) +
      Math.pow(Math.abs(eyy - by), 2));
    let dist4 = Math.sqrt(Math.pow(Math.abs(exx - bx), 2) +
      Math.pow(Math.abs(eyy - by), 2));

    if (dist1 <= bs || dist2 <= bs || dist3 <= bs || dist4 <= bs)
      entity.colliding(EntityID.BOMB);
  }
  /**
   * Creates DeadEdge entities by using a given object (entity) at a given 
   * position and adds them to the entities array.
   * @method createDeadEntity
   * @memberof Level
   * @param {number} x      X position of entity.
   * @param {number} y      Y position of entity.
   * @param {object} object Entity to create DeadEdges from.
   * @param {number} dir    Direction of entity.
   */
  createDeadEntity (x, y, object, dir) {
    var m = object.model;
    for (var i = 0; i < m.edges.length; i++) {
      var e = m.edges[i];
      var s = m.scale;
      var v1x = m.vertices[e[0]][0];
      var v1y = m.vertices[e[0]][1];
      var v2x = m.vertices[e[1]][0];
      var v2y = m.vertices[e[1]][1];

      // Calculate where the edge is in the game world
      var nx = object.x; // + ((v1x + v2x) / 2 * s);
      var ny = object.y; // + ((v1y + v2y) / 2 * s);
      var avgx = (v1x + v2x) / 2;
      var avgy = (v1y + v2y) / 2;

      // Calculate where the edge will be heading to
      var dirHeading = Game.calculateAngle(0, 0, avgx, avgy);

      // Calculate width and height
      let w = Math.abs(v2x) - Math.abs(v1x);
      let h = Math.abs(v2y) - Math.abs(v1y);

      // Create the edge object
      var edgeObj = new DeadEdge(nx, ny, dir, dirHeading, w, h);

      // Add the edge to the model
      edgeObj.model.addVertex(m.vertices[e[0]][0], m.vertices[e[0]][1]);
      edgeObj.model.addVertex(m.vertices[e[1]][0], m.vertices[e[1]][1]);
      edgeObj.model.addEdge(0, 1, m.ecolors[i], m.thickness[i]);
      edgeObj.model.scale = edgeObj.baseScale = m.scale;

      // Push the object to the entities
      this.entities.push(edgeObj);
    }
  }
  /**
   * Creates the model of the level with given colors.
   * @method createLevelModel 
   * @memberof Level
   * @param {string} border     Color of borders.
   * @param {string} mark       Color of lines inside the level.
   * @param {string} background Color of background.
   */
  createLevelModel (border, mark, background) {
    var w = this.width / 2;
    var h = this.height / 2;
    var m = this.model;
    // Create level border ----------------------------------------------- {{{
    // Add vertices
    m.addVertex(-w, -h);
    m.addVertex(w, -h);
    m.addVertex(w, h);
    m.addVertex(-w, h);

    // Add edges
    m.addEdge(0, 1, border, 7);
    m.addEdge(1, 2, border, 7);
    m.addEdge(2, 3, border, 7);
    m.addEdge(3, 0, border, 7);

    // Add background
    m.addPolygon([0, 1, 2, 3], background);
    // }}}
    // Add markers ------------------------------------------------------- {{{
    var ms = 50;
    var mb = 250;

    // Add horizontal marks
    for (var i = 1; i < this.width / ms; i++) {
      var cv = m.vertices.length;
      var cx = -w + (i * ms);
      m.addVertex(cx, -h);
      m.addVertex(cx, h);

      if (cx % mb == 0)         // Creae bigger marker
        m.addEdge(cv, cv + 1, mark, 3);
      else                              // Create smaller marker
        m.addEdge(cv, cv + 1, mark, 1);
    }

    // Add vertical marks
    for (var i = 1; i < this.height / ms; i++) {
      var cv = m.vertices.length;
      var cy = -h + (i * ms);
      m.addVertex(-w, cy);
      m.addVertex(w, cy);

      if (cy % mb == 0)         // Creae bigger marker
        m.addEdge(cv, cv + 1, mark, 3);
      else                              // Create smaller marker
        m.addEdge(cv, cv + 1, mark, 1);
    }
    // }}}
  }
}
