/**
 * @class
 * @extends Level
 */
class DiamondsLevel extends Level {
  /**
   * @constructor
   * @memberof DiamondsLevel
   */
  constructor () {
    super();
    this.createLevelModel("#0AF", "08D", "#000");
    this.ID = LevelID.DIAMOND_RUSH;

    // Level rules
    this.changeWeaponByScore = false;
    this.gainBombs = false;
    this.gainLives = false;
    this.bombCount = 1;
    this.lives = 0;
    this.multiplierBasedOnScore = true;
    
    // Level music
    this.music = SoundEngine.getMusic().GAME;

    // Spawning variables
    this.diamondCount = 4;
    this.spawnSpan = 180;
    this.spawnTime = this.spawnSpan;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof DiamondsLevel
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    if (this.spawnAllowed) {
      // Run the spawn timer
      this.spawnTime--;

      // Time to spawn diamonds
      if (this.spawnTime <= 0) {
        // Generate the size of the cluster
        let rad = (this.diamondCount / 3) * 20;
        
        // Generate random coordinates
        let x = Math.random() * (this.width * 0.7)- this.width / 2;
        let y = Math.random() * (this.height * 0.7) - this.height / 2;

        // Remove half the size on both coordinates
        if (x > 0) 
          x -= rad / 2;
        else
          x += rad / 2;
        
        if (y > 0)
          y -= rad / 2;
        else
          y += rad / 2;

        // Spawn the cluster
        this.spawnEntityCluster(x, y, this.diamondCount, rad, EntityID.DIAMOND);

        // Increase the number of diamonds to spawn
        this.diamondCount++;

        // Reset the timer
        this.spawnTime = this.spawnSpan;
      }
    }
  }
}