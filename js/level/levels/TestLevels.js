/**
 * @class
 * @extends Level
 */
class TestLevel01 extends Level {
  /**
   * @constructor
   * @memberof TestLevel01
   */
  constructor () {
    super();                              // Create the level
    this.createLevelModel("#FFF", "#AAA", "#000");
    this.ID = LevelID.TEST_LEVEL_01;

    // Settings
    this.multiplierResetOnDeath = false;

    // Spawning timer
    this.spawnTime = 0;
    this.spawnSpan = 90;

    // Set music
    this.music = SoundEngine.getMusic().GAME;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof TestLevel01
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    // Call the level update
    super.tick(controls, mousePos, renderScale, soundEngine);

    // Spawn stuff
    if (this.spawnAllowed) {
      // Update the spawn timer
      this.spawnTime++;

      // Has the timer expired?
      if (this.spawnTime >= this.spawnSpan) {
        // Reset the spawn timer
        this.spawnTime = 0;

        this.spawnEntityAtRandom(EntityID.SPIRAL, 0);
        this.spawnRandomBlock();
      }
    }
  }
}
/**
 * @class
 * @extends Level
 */
class TestLevel02 extends Level {
  /**
   * @constructor
   * @memberof TestLevel02
   */
  constructor () {
    super(false);
    this.createLevelModel("#FCF", "#DAD", "#000");
    this.ID = LevelID.TEST_LEVEL_02;

    // Settings
    this.multiplierBasedOnScore = true;
    this.defaultPlayerWeapon = WeaponID.STANDARD;
    this.player.weaponType = this.defaultPlayerWeapon;

    // Spawning Timer
    this.spawnTime = 0;
    this.spawnSpan = 90;

    // Set music
    this.music = SoundEngine.getMusic().GAME;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof TestLevel02
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    if (this.spawnAllowed) {
      this.spawnTime--;

      if (this.spawnTime <= 0) {
        this.spawnEntityAtRandom(EntityID.ARROW, 0);
        this.spawnEntityAtRandom(EntityID.DIAMOND, 0);
        this.spawnRandomArrow();

        this.spawnEntitiesAtRandom(5, EntityID.SPIRAL, 0);
        this.spawnTime = this.spawnSpan;
      }
    }
  }
}
/**
 * @class
 * @extends Level
 */
class EmptyLevel extends Level {
  /**
   * @constructor
   * @memberof EmptyLevel
   */
  constructor () {
    super();
    this.createLevelModel("#FFF", "#AAA", "#000");
    this.ID = LevelID.EMPTY_LEVEL;

    // Set music
    this.music = SoundEngine.getMusic().GAME;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof EmptyLevel
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);
  }
}
/**
 * @class
 * @extends Menu
 */
class TestLevelSelect extends Menu {
  /**
   * @constructor
   * @memberof TestLevelSelect
   */
  constructor () {
    super();
    this.ID = LevelID.TEST_LEVEL_SELECT;

    this.bbx = 0;
    this.cx = 0;
    this.bx1 = 0;
    this.bx2 = 0;
    this.b1y = 0;
    this.b2y = 0;
    this.b3y = 0;
    this.b4y = 0;
    this.bby = 0;
    this.l1y = 100;
    this.bg = 80;         // Gap between buttons

    this.bw = 300;
    this.bh = 54;
    this.bf = 32;

    this.GUIentities.push(new Button("testlvl1",
                                     this.bx2, 
                                     this.b1y, 
                                     this.bw, 
                                     this.bh, 
                                     Levels.getName(LevelID.TEST_LEVEL_01), 
                                     this.bf));

    this.GUIentities.push(new Button("testlvl2",
                                     this.bx2, 
                                     this.b2y, 
                                     this.bw, 
                                     this.bh, 
                                     Levels.getName(LevelID.TEST_LEVEL_02), 
                                     this.bf));

    this.GUIentities.push(new Button("emptylvl",
                                     this.bx2, 
                                     this.b3y, 
                                     this.bw, 
                                     this.bh, 
                                     Levels.getName(LevelID.EMPTY_LEVEL), 
                                     this.bf));

    this.GUIentities.push(new Button("waveslvl",
                                    this.bx1, 
                                    this.b1y, 
                                    this.bw, 
                                    this.bh, 
                                    Levels.getName(LevelID.WAVES), 
                                    this.bf));
    
    this.GUIentities.push(new Button("diamondslvl",
                                     this.bx1, 
                                     this.b2y, 
                                     this.bw, 
                                     this.bh, 
                                     Levels.getName(LevelID.DIAMOND_RUSH), 
                                     this.bf));
    
    this.GUIentities.push(new Button("lineslvl",
                                     this.bx1, 
                                     this.b3y, 
                                     this.bw, 
                                     this.bh, 
                                     Levels.getName(LevelID.SUPER_LINES), 
                                     this.bf));

    this.GUIentities.push(new Button("travellvl",
                                     this.bx1, 
                                     this.b4y, 
                                     this.bw, 
                                     this.bh, 
                                     Levels.getName(LevelID.TRAVELING_MADNESS), 
                                     this.bf));

    this.GUIentities.push(new Button("back",
                                     this.bbx, 
                                     this.bby, 
                                     100, 
                                     40, 
                                     "Back", 
                                     25));


    this.GUIentities.push(new Label("title", 
                                    this.cx, 
                                    this.l1y,
                                    "Select a Level", 
                                    80));

    this.music = SoundEngine.getMusic().MAIN_MENU;
  }
  /**
   * Updates variables of the menu. This includes entities and GUI elements.
   * @method tick
   * @memberof TestLevelSelect
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);
    this.cx = MainComponent.getWidth() / 2;
    this.bx1 = MainComponent.getWidth() / 3;
    this.bx2 = MainComponent.getWidth() / 3 * 2;
    this.b1y = MainComponent.getHeight() / 2 - this.bg * renderScale;
    this.b2y = MainComponent.getHeight() / 2;
    this.b3y = MainComponent.getHeight() / 2 + this.bg * renderScale;
    this.b4y = MainComponent.getHeight() / 2 + (this.bg * 2) * renderScale;
    this.bby = MainComponent.getHeight() - 40 * renderScale;
    this.bbx = 75 * renderScale;
    this.l1y = 100 * renderScale;

    // Reposition the buttons
    Button.find("testlvl1", this.GUIentities).reposition(this.bx2, this.b1y);
    Button.find("testlvl2", this.GUIentities).reposition(this.bx2, this.b2y);
    Button.find("waveslvl", this.GUIentities).reposition(this.bx1, this.b1y);
    Button.find("diamondslvl", this.GUIentities).reposition(this.bx1, this.b2y);
    Button.find("lineslvl", this.GUIentities).reposition(this.bx1, this.b3y);
    Button.find("travellvl", this.GUIentities).reposition(this.bx1, this.b4y);
    Button.find("emptylvl", this.GUIentities).reposition(this.bx2, this.b3y);
    Button.find("back", this.GUIentities).reposition(this.bbx, this.bby);
    Label.find("title", this.GUIentities).reposition(this.cx, this.l1y)

    // Check for button presses
    if (Button.find("testlvl1", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.TEST_LEVEL_01);
      this.musicSet = false;
    }

    if (Button.find("testlvl2", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.TEST_LEVEL_02);
      this.musicSet = false;
    }

    if (Button.find("waveslvl", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.WAVES);
      this.musicSet = false;
    }

    if (Button.find("diamondslvl", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.DIAMOND_RUSH);
      this.musicSet = false;
    }
    
    if (Button.find("lineslvl", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.SUPER_LINES);
      this.musicSet = false;
    }

    if (Button.find("travellvl", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.TRAVELING_MADNESS);
      this.musicSet = false;
    }

    if (Button.find("emptylvl", this.GUIentities).isPressed) {
      this.next = Levels.getLevel(LevelID.EMPTY_LEVEL);
      this.musicSet = false;
    }

    if (Button.find("back", this.GUIentities).isPressed) {
      this.done = true;
      this.musicSet = false;
    }
  }
}
