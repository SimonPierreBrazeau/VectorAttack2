/**
 * @class
 * @extends Level
 */
class SuperLinesLevel extends Level {
  /**
   * @constructor
   * @memberof SuperLineLevel
   */
  constructor () {
    super();
    this.createLevelModel("#FFF", "#AAA", "#000");
    this.ID = LevelID.SUPER_LINES;

    this.changeWeaponByScore = false;
    this.bombCount = 0;
    this.gainLives = false;
    this.defaultPlayerWeapon = WeaponID.STANDARD;

    // Spawning counters
    this._currentLineID = 0;
    
    this._enemySequences = [
      EntityID.SLIDER,
      EntityID.SPIRAL,
      EntityID.ARROW,
      EntityID.DIAMOND,
      EntityID.BOUNCER
    ];

    this._currentLineCount = 0;
    this._MAX_LINE_COUNT = 10;

    this._currentSequence = 0;
    this._MAX_SEQUENCE = 2;

    this._spawnSpan = 100;
    this._spawnTime = this._spawnSpan;
    this._spawnBreakSpan = 150;
    this._spawnBreakTriSpan = 500;
    this._spawnBreakTime = this._spawnBreakSpan;
    this._isSequenceDone = false;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof SuperLineLevel
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    if (this.spawnAllowed) {
      if (!this._isSequenceDone) {
        this._spawnTime--;

        if (this._spawnTime <= 0) {
          // Spawn a line of enemies
          if (this._currentSequence == 0)
            this.spawnWave();
          else {
            this.spawnCross();
            if (this._currentSequence == 2)
              this.spawnTravelers(10);
          }

          this._spawnTime = this._spawnSpan;
          this._currentLineCount++;
          if (this._currentLineCount >= this._MAX_LINE_COUNT) {
            this._currentLineCount = 0;
            this._currentLineID++;

            if (this._currentLineID >= this._enemySequences.length) {
              this._currentLineID = 0;
              this._currentSequence++;
              if (this._currentSequence == 2)
                this.spawnBonusController(WeaponID.INVULNERABLE);

              // Spawn triangles
              this.spawnPointSpawner(-this.width * 0.8, -this.height * 0.8, 30,
                30 / this._spawnBreakTriSpan, EntityID.TRIANGLE);
              this.spawnPointSpawner(this.width * 0.8, -this.height * 0.8, 30,
                30 / this._spawnBreakTriSpan, EntityID.TRIANGLE);
              this.spawnPointSpawner(this.width * 0.8, this.height * 0.8, 30,
                30 / this._spawnBreakTriSpan, EntityID.TRIANGLE);
              this.spawnPointSpawner(-this.width * 0.8, this.height * 0.8, 30,
                30 / this._spawnBreakTriSpan, EntityID.TRIANGLE);

              this._spawnBreakTime = this._spawnBreakTriSpan;

              if (this._currentSequence >= this._MAX_SEQUENCE)
                this._currentSequence = this._MAX_SEQUENCE;
            }
            this._isSequenceDone = true;
          }
        }
      } else {        // Between sequences
        this._spawnBreakTime--;
        if (this._spawnBreakTime <= 0) {
          this._spawnBreakTime = this._spawnBreakSpan;
          this._isSequenceDone = false;
        }
      }
    }
  }
  /**
   * Spawns a wave of the current wave's enemy ID.
   * @method spawnWave
   * @memberof SuperLineLevel
   */
  spawnWave () {
    let d = Math.round(Math.random());
    let x = 0;
    let y = 0;
    let o = 0;
    let a = 0;
    let s = 50;

    if (d) {      // Horizontal
      y = Math.random() * (this.height * 0.8) - (this.height * 0.8 / 2);
      x = 0;
      a = this.width / s - 1;

      if (y >= 0)
        o = 0;
      else
        o = 1;
    } else {        // Vertical
      x = Math.random() * (this.width * 0.8) - (this.width * 0.8 / 2);
      y = 0;
      a = this.height / s - 1;

      if (x >= 0)
        o = 1.5;
      else
        o = 0.5;
    }

    if (this._enemySequences[this._currentLineID] == EntityID.ARROW)
      this.spawnEntityWall(x, y, a, s, o, 
        this._enemySequences[this._currentLineID]);
    else
      this.spawnEntityWall(x, y, a, s, o, 
        this._enemySequences[this._currentLineID], true);

  }
  /**
   * Spawns a cross wave of the current wave's enemy ID.
   * @method spawnCross
   * @memberof SuperLineLevel
   */
  spawnCross () {
    let x = Math.random() * (this.width * 0.8) - (this.width * 0.8 / 2);
    let y = Math.random() * (this.height * 0.8) - (this.height * 0.8 / 2);
    let oh = 0;
    let ov = 0;
    let s = 50;
    let ah = this.width / s - 1;
    let av = this.height / s - 1;
    let r = true;

    if (x >= 0)
      oh = 1.5;
    else
      oh = 0.5;
    
    if (y >= 0)
      ov = 0;
    else
      ov = 1;

    if (this._enemySequences[this._currentLineID] == EntityID.ARROW)
      r = false;

    this.spawnEntityWall(x, 0, ah, s, oh, 
      this._enemySequences[this._currentLineID], r);
    this.spawnEntityWall(0, y, av, s, ov, 
      this._enemySequences[this._currentLineID], r);
  }
}