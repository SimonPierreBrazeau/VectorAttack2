/**
 * @class
 * @extends Menu
 */
class SettingsMenu extends Menu {
  /**
   * @constructor
   * @memberof SettingsMenu
   */
  constructor () {
    super();
    this.ID = LevelID.SETTINGS_MENU;

    // Predefined positions
    this.cx = 0;
    this.cy = 0;
    this.msby = 0;
    this.mmby = 0;
    this.fwby = 0;
    this.by = 0;
    this.ty = 0;
    this.bg = 80;

    // Elements
    this.GUIentities.push(new Label("title", this.cx, this.ty, "Settings", 80));
    this.GUIentities.push(new Button("fillWindow", this.cx, this.msby, 400, 60,
      "Fill window", 40));
    this.GUIentities.push(new Button("back", this.cx, this.by, 100, 40,
      "Back", 25));

    // Music
    this.music = SoundEngine.getMusic().MAIN_MENU;

    // To set the button colors with their setting
    this.colorSet = false;
  }
  /**
   * Updates variables of the menu. This includes entities and GUI elements.
   * @method tick
   * @memberof SettingsMenu
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    // Recalculate positions
    this.cx = MainComponent.getWidth() / 2;
    this.cy = MainComponent.getHeight() / 2;
    this.ty = 100 * renderScale;
    this.by = MainComponent.getHeight() - 40 * renderScale;
    this.msby = this.cy - this.bg * renderScale;
    this.mmby = this.cy;
    this.fwby = this.cy + this.bg * renderScale;

    // Get buttons
    let fwButton = Button.find("fillWindow", this.GUIentities);
    let bButton = Button.find("back", this.GUIentities);

    // Set button colors
    if (!this.colorSet) {
      fwButton.toggleColor(mainComp.resizable);
    }

    // Reposition everything
    Label.find("title", this.GUIentities).reposition(this.cx, this.ty);
    bButton.reposition(this.cx, this.by);
    fwButton.reposition(this.cx, this.msby);

    // Check button presses
    if (bButton.isPressed) {
      this.done = true;
      this.musicSet = false;
    }

    if (fwButton.isPressed) {
      if (mainComp.resizable)
        mainComp.resizable = false;
      else
        mainComp.resizable = true;

      mainComp.resizeCanvas();

      fwButton.toggleColor(mainComp.resizable);
    }
  }
}
