/**
 * @class
 * @extends Menu
 */
class MainMenu extends Menu {
  /**
   * @constructor
   * @memberof MainMenu
   */
  constructor () {
    super();
    this.ID = LevelID.MAIN_MENU;

    // predifined positions
    this.cx = 0;
    this.cy = 0;
    this.ty = 0;
    this.slx = 0;
    this.sly = 0;
    this.smx = 0;

    this.ls = 40;

    this.GUIentities.push(new Label("title", this.cx, this.ty,
      "Vector Attack 2", 80));

    this.GUIentities.push(new Button("selectLevel", this.slx, this.sly,
      400, 60, "Select a Level", 40));

    //this.GUIentities.push(new Button("settings", this.smx, this.sly,
    //  400, 60, "Settings", 40));

    this.GUIentities.push(new Icon("logogem1", this.cx, this.cy,
      Model.createLogoGem(this.ls)));

    this.GUIentities.push(new Icon("logogem2", this.cx, this.cy,
      Model.createLogoGem(this.ls)));

    this.GUIentities.push(new Icon("logomain", this.cx, this.cy,
      Model.createLogoBase(this.ls)));

    this.GUIentities.push(new Label("version", 0, 0,
      MainComponent.getVersion(), 12, 2));

    // Add music
    this.music = SoundEngine.getMusic().MAIN_MENU;
  }
  /**
   * Updates variables of the menu. This includes entities and GUI elements.
   * @method tick
   * @memberof MainMenu
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    // Resize coordinates
    this.cx = MainComponent.getWidth() / 2;
    this.cy = MainComponent.getHeight() / 2;
    this.ty = 100 * renderScale;
    this.sly = MainComponent.getHeight() - 100 * renderScale;
    this.slx = this.cx - 210 * renderScale;
    this.smx = this.cx + 210 * renderScale;
    let vx = MainComponent.getWidth() - 4 * renderScale;
    let vy = MainComponent.getHeight() - 8 * renderScale;

    // Logo position
    let lx = this.cx;
    let gg = this.ls;
    let g1x = lx - gg * renderScale;
    let g2x = lx + gg * renderScale;
    let ly = this.cy;

    // Reposition GUI elements
    Label.find("title", this.GUIentities).reposition(this.cx, this.ty);
    Button.find("selectLevel", this.GUIentities).reposition(this.cx, this.sly);
    //Button.find("settings", this.GUIentities).reposition(this.smx, this.sly);
    Icon.find("logomain", this.GUIentities).reposition(lx, ly);
    Icon.find("logogem1", this.GUIentities).reposition(g1x, ly);
    Icon.find("logogem2", this.GUIentities).reposition(g2x, ly);
    Label.find("version", this.GUIentities).reposition(vx, vy);

    // Check for button presses
    if (Button.find("selectLevel", this.GUIentities).isPressed) {
      this.next = new TestLevelSelect();
      this.musicSet = false;
    }

    //if (Button.find("settings", this.GUIentities).isPressed) {
    //  this.next = new SettingsMenu();
    //  this.musicSet = false;
    //}

  }
}
