/**
 * @class
 * @extends Menu
 */
class GameOver extends Menu {
  /**
   * @constructor
   * @memberof GameOver
   * @param {number} score  Finishing score of previous level.
   * @param {object} origin Level instance that created this.
   */
  constructor (score, origin) {
    super();
    this.ID = LevelID.GAME_OVER_MENU;
    this.soundPlayed = false;

    this.origin = origin;             // Level ID that triggered the game over

    // GUI position
    this.lx = MainComponent.getWidth() / 2;
    this.bx1 = MainComponent.getWidth() / 2 - 100;
    this.bx2 = MainComponent.getWidth() / 2 + 100;
    this.lyt = MainComponent.getHeight() / 2 - 100;
    this.lys = MainComponent.getHeight() / 2 + 50;
    this.by = MainComponent.getHeight() / 2 + 200;

    // Game Over label
    this.GUIentities.push(new Label("title", this.lx, this.lyt,
      "Game Over", 100));

    // Score label
    this.GUIentities.push(new Label("score", this.lx, this.lys,
      "Score: " + Game.scoreString(score.toString()), 80));

    // Back to main menu button
    this.GUIentities.push(new Button("exit", this.bx1,
      this.by, 250, 75, "Main Menu", 30));

    // Restart Button
    this.GUIentities.push(new Button("restart", this.bx2,
      this.by, 250, 75, "Restart", 30));
  }
  /**
   * Updates variables of the menu. This includes entities and GUI elements.
   * @method tick
   * @memberof GameOver
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    if (!this.soundPlayed) {
      soundEngine.addSound(SoundID.GAME_OVER);
      this.soundPlayed = true;
    }

    // Recalculate positiona
    this.lx = MainComponent.getWidth() / 2;
    this.bx1 = MainComponent.getWidth() / 2 - 200 * renderScale;
    this.bx2 = MainComponent.getWidth() / 2 + 200 * renderScale;
    this.lyt = MainComponent.getHeight() / 2 - 100 * renderScale;
    this.lys = MainComponent.getHeight() / 2 + 50 * renderScale;
    this.by = MainComponent.getHeight() / 2 + 200 * renderScale;

    // Reposition GUI
    Label.find("title", this.GUIentities).reposition(this.lx, this.lyt);
    Label.find("score", this.GUIentities).reposition(this.lx, this.lys);
    Button.find("exit", this.GUIentities).reposition(this.bx1, this.by);
    Button.find("restart", this.GUIentities).reposition(this.bx2, this.by);

    // Check button presses
    if (Button.find("exit", this.GUIentities).isPressed) {
      this.done = true;
    }

    if (Button.find("restart", this.GUIentities).isPressed) {
      this.done = true;
      this.next = Levels.getLevel(this.origin);
    }
  }
}
