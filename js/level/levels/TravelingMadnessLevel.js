/**
 * @class
 * @extends Level
 */
class TravelingMadnessLevel extends Level {
  /**
   * @constructor
   * @memberof TravelingMadnessLevel
   */
  constructor () {
    super(false);
    this.createLevelModel("#FFF", "#AAA", "#000");

    this.defaultPlayerWeapon = WeaponID.STANDARD;
    this.changeWeaponByScore = false;
    this.bombCount = 0;
    this.gainBombs = false;
    this.gameTime = 60 * 3;

    this._travelerCount = 6;
    this._travelerGrowSpan = 2;
    this._travelerGrowTime = this._travelerGrowSpan;

    this._goldenCount = 0;
    this._goldenSpan = 4;
    this._goldenTime = this._goldenSpan;

    this._waveSpan = 300;
    this._waveTime = this._waveSpan;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof SuperLineLevel
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    if (this.spawnAllowed) {
      this._waveTime--;
      if (this._waveTime <= 0) {
        this._goldenTime--;
        this._travelerGrowTime--;

        if (this._travelerGrowTime <= 0 ) {
          this._travelerCount++;
          this._travelerGrowTime = this._travelerGrowSpan;
        }
        
        if (this._goldenTime <= 0) {
          this._goldenCount++;
          this._goldenTime = this._goldenSpan;
          this.spawnMixedTravelers(this._travelerCount, this._goldenCount);
        } else 
          this.spawnTravelers(this._travelerCount);

        this._waveTime = this._waveSpan;
      }
    }
  }
}