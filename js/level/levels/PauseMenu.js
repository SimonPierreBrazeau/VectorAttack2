/**
 * @class
 * @extends Menu
 */
class PauseMenu extends Menu {
  /**
   * @constructor
   * @memberof PauseMenu
   * @param {object} level Level instance which created this.
   */
  constructor (level) {
    super();
    this.levelType = LevelID.PAUSE_MENU;
    this.currentID = level.ID;
    this.ID = LevelID.PAUSE_MENU;

    // Everything from the current level
    this.player = level.player;
    this.model = level.model;
    this.entities = level.entities;
    this.GUIentities = level.GUIentities;
    this.bullets = level.bullets;
    this.ghosts = level.ghosts;
    this.bombs = level.bombs;

    this.pausedLevel = level;
    this.soundPlayed = false;

    // Create GUI stuff for menu
    let tsw = 300;
    let tsh = 100;
    let bsw = 400;
    let bsh = 100;

    let cx = MainComponent.getWidth() / 2;
    let cy = MainComponent.getHeight() / 2;
    let bg = 80;
    let ty = 40;
    let by = MainComponent.getHeight() - 40;

    this.GUIentities.push(new Button("pmTop", cx, ty, tsw, tsh, "", 1, false));
    this.GUIentities.push(new Button("pmBot", cx, by, bsw, bsh, "", 1, false));
    this.GUIentities.push(new Label("pmTitle", cx, ty, "Game Paused", 40));
    this.GUIentities.push(new Button("pmBack", cx - bg, by, 140, 40,
      "Main Menu", 25));
    this.GUIentities.push(new Button("pmRestart", cx + bg, by, 140, 40,
      "Restart", 25));

    // Sounds
    this.soundsPaused = false;
  }
  /**
   * Updates variables of the menu. This includes entities and GUI elements.
   * @method tick
   * @memberof PauseMenu
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    if (!this.soundPlayed) {
      soundEngine.addSound(SoundID.PAUSE_MENU_OPEN);
      this.soundPlayed = true;
    }
    
    // Pause sounds
    if (!this.soundsPaused)
      soundEngine.pauseSounds();

    // Don't call super.tick because it will update stuff from the previous
    // level.
    let cx = MainComponent.getWidth() / 2;
    let cy = MainComponent.getHeight() / 2;
    let bg = 80 * renderScale;
    let ty = 40 * renderScale;
    let by = MainComponent.getHeight() - 40 * renderScale;

    // Reposition stuff
    Label.find("pmTitle", this.GUIentities).reposition(cx, ty);
    Button.find("pmBack", this.GUIentities).reposition(cx - bg, by);
    Button.find("pmRestart", this.GUIentities).reposition(cx + bg, by);
    Button.find("pmTop", this.GUIentities).reposition(cx, ty);
    Button.find("pmBot", this.GUIentities).reposition(cx, by);

    // Tick important stuff
    Button.find("pmBack", this.GUIentities).tick(controls.leftClick, mousePos,
      renderScale, soundEngine);
    Button.find("pmRestart", this.GUIentities).tick(controls.leftClick, mousePos,
      renderScale, soundEngine);

    // Check for presses
    if (Button.find("pmBack", this.GUIentities).isPressed) {
      this.done = true;
      this.pausedLevel.done = true;
    }

    if (Button.find("pmRestart", this.GUIentities).isPressed) {
      this.done = true;
      this.pausedLevel.done = true;
      this.next = Levels.getLevel(this.currentID);
    }

    // Check for menu key
    if (controls.menu) {
      this.done = true;
      soundEngine.resumeSounds();

      // Delete GUI elements that were created here
      for (var i = 0; i < this.GUIentities.length; i++) {
        let en = this.GUIentities[i];
        if (en.identifier == "pmTitle" || en.identifier == "pmBack" ||
          en.identifier == "pmRestart" || en.identifier == "pmTop" ||
          en.identifier == "pmBot") {
          this.GUIentities.splice(i, 1);
          i--;
        }
      }
    }
  }
}
