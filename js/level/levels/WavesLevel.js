/**
 * @class
 * @extends Level
 */
class WavesLevel extends Level {
  /**
   * @constructor
   * @memberof WavesLevel
   */
  constructor () {
    super();
    this.createLevelModel("#F80", "#B35900", "#000");
    this.ID = LevelID.WAVES;
    this.changeWeaponByScore = false;

    // Spawning Timer
    this.spawnSpan = 180;
    this.spawnTime = this.spawnSpan;

    // Wave spawning timer
    this._spawnWaveSpan = 200;
    this._spawnWaveTime = this._spawnWaveSpan;

    // Spawning spirals
    this._spawnSpiralSpan = 180;
    this._spawnSpiralTime = this._spawnSpiralSpan;
    this._spawnSpiralAmount = 1;
    this._spawnSpiralAmountMax = 5;
    this._spawnSpiralAmountGrows = true;
    this._spawnSpiralGrowSpan = 4;
    this._spawnSpiralGrowTime = this._spawnSpiralGrowSpan;
    this._spawnSpiralDelaySpan = 600;
    this._spawnSpiralDelayTime = this._spawnSpiralDelaySpan;
    this._spawnSpirals = false;

    // Spawning sliders
    this._spawnSliderSpan = 180;
    this._spawnSliderTime = this._spawnSliderSpan;
    this._spawnSliderAmount = 1;
    this._spawnSliderAmountMax = 5;
    this._spawnSliderAmountGrows = true;
    this._spawnSliderGrowSpan = 4;
    this._spawnSliderGrowTime = this._spawnSliderGrowSpan;
    this._spawnSliders = false;

    // Spawning bouncers
    this._spawnBouncerSpan = 180;
    this._spawnBouncerTime = this._spawnBouncerSpan;
    this._spawnBouncerAmount = 1;
    this._spawnBouncerAmountMax = 5;
    this._spawnBouncerAmountGrows = true;
    this._spawnBouncerGrowSpan = 4;
    this._spawnBouncerGrowTime = this._spawnBouncerGrowSpan;
    this._spawnBouncers = false;

    // Spawning diamonds
    this._spawnDiamondSpan = 180;
    this._spawnDiamondTime = this._spawnDiamondSpan;
    this._spawnDiamondAmount = 1;
    this._spawnDiamondAmountMax = 5;
    this._spawnDiamondAmountGrows = true;
    this._spawnDiamondGrowSpan = 4;
    this._spawnDiamondGrowTime = this._spawnDiamondGrowSpan;
    this._spawnDiamonds = false;

    // Going past max
    this._maxReached = false;

    // Spawning bonus controllers
    this.spawnBonusSpan = 3600;
    this.spawnBonusTime = this.spawnBonusSpan;

    // Set music
    this.music = SoundEngine.getMusic().GAME;

    // Arrow waves
    this._arrowSpawnSize = 50;  // size of arrow + 5 on each side
    this._wallSizeMax = this.width / this._arrowSpawnSize;
    this._wallSize = 6;  // Start with 6 arrows
    this._wallSizeSpan = 4;  // Grow every 2 spawn cycles
    this._wallSizeTime = this._wallSizeSpan;
  }
  /**
   * Updates variables of the level. This includes entities, player and GUI
   * elements.
   * @method tick
   * @memberof WavesLevel
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {number} renderScale     Scale of rendering engine.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, renderScale, soundEngine) {
    super.tick(controls, mousePos, renderScale, soundEngine);

    if (this.spawnAllowed) {
      this._tickSpawnWave();
      this._tickSpawnSpirals();
      this._tickSpawnSliders();
      this._tickSpawnBouncers();
      this._tickSpawnDiamonds();

      this.spawnBonusTime--;

      if (!this.spawnBonusTime) {
        this.spawnBonusController(Weapons.getRandomBonusState());
        this.spawnBonusTime = this.spawnBonusSpan;
      }
    }
  }
  /**
   * Updates variables for spawning waves of Arrows.
   * @method _tickSpawnWave
   * @memberof WavesLevel
   */
  _tickSpawnWave () {
    this._spawnWaveTime--;

    // Spawn waves
    if (this._spawnWaveTime <= 0) {
      // Generate a number for corner and one for horizontal
      let c = Math.round(Math.random() * 3) + 1;
      let h = Math.round(Math.random());

      this.spawnEntityWallAtCorner(c, 
                                   h, 
                                   this._wallSize, 
                                   this._arrowSpawnSize,
                                   EntityID.ARROW);

      // Tick the counter to increase the number of arrows to spawn
      if (this._wallSize < this._wallSizeMax) {
        this._wallSizeTime--;
        if (this._wallSizeTime <= 0) {
          this._wallSize++;
          this._wallSizeTime = this._wallSizeSpan;
        }
      }

      this._spawnWaveTime = this._spawnWaveSpan;
    }
  }
  /**
   * Updates variables for spawning Spirals.
   * @method _tickSpawnSpirals
   * @memberof WavesLevel
   */
  _tickSpawnSpirals () {
    if (!this._spawnSpirals)
      this._spawnSpiralDelayTime--;
    else
      this._spawnSpiralTime--;
    
    // Check if spirals can spawn
    if (this._spawnSpiralDelayTime <= 0)
      this._spawnSpirals = true;

    // Spawn Spirals
    if (this._spawnSpirals && this._spawnSpiralTime <= 0) {
      // Reset the timer
      this._spawnSpiralTime = this._spawnSpiralSpan;

      // Spawn the spirals
      this.spawnEntitiesAtRandom(this._spawnSpiralAmount, EntityID.SPIRAL);
      
      // Change the amount of spirals spawning
      this._spawnSpiralGrowTime--;
      if (this._spawnSpiralGrowTime <= 0 && this._spawnSpiralAmountGrows) {
        this._spawnSpiralGrowTime = this._spawnSpiralGrowSpan;
        this._spawnSpiralAmount++;
        
        if (this._spawnSpiralAmount == 
          (this._spawnSpiralAmountMax || this._maxReached)) {
          if (!this.spawnSliders) {
            this._spawnSliders = true;
            this._spawnSpiralAmount = 1;
          } else
            this._spawnSpiralAmountGrows = false;
        }
      }
    }
  }
  /**
   * Updates variables for spawning Sliders.
   * @method _tickSpawnSliders
   * @memberof WavesLevel
   */
  _tickSpawnSliders () {
    if (this._spawnSliders)
      this._spawnSliderTime--;

    // Spawn Sliders
    if (this._spawnSliders && this._spawnSliderTime <= 0) {
      // Reset the timer
      this._spawnSliderTime = this._spawnSliderSpan;

      // Spawn the sliders
      this.spawnEntitiesAtRandom(this._spawnSliderAmount, EntityID.SLIDER);
      
      // Change the amount of sliders spawning
      this._spawnSliderGrowTime--;
      if (this._spawnSliderGrowTime <= 0 && 
        (this._spawnSliderAmountGrows || this._maxReached)) {
        this._spawnSliderGrowTime = this._spawnSliderGrowSpan;
        this._spawnSliderAmount++;
        
        if (this._spawnSliderAmount == this._spawnSliderAmountMax) {
          this._spawnSliderAmountGrows = false;
          this._spawnBouncers = true;
        }
      }
    }
  }
  /**
   * Updates variables for spawning Bouncers.
   * @method _tickSpawnBouncers
   * @memberof WavesLevel
   */
  _tickSpawnBouncers () {
    if (this._spawnBouncers)
      this._spawnBouncerTime--;

    // Spawn Bouncers
    if (this._spawnBouncers && this._spawnBouncerTime <= 0) {
      // Reset the timer
      this._spawnBouncerTime = this._spawnBouncerSpan;

      // Spawn the Bouncers
      this.spawnEntitiesAtRandom(this._spawnBouncerAmount, EntityID.BOUNCER);
      
      // Change the amount of Bouncers spawning
      this._spawnBouncerGrowTime--;
      if (this._spawnBouncerGrowTime <= 0 && 
        (this._spawnBouncerAmountGrows || this._maxReached)) {
        this._spawnBouncerGrowTime = this._spawnBouncerGrowSpan;
        this._spawnBouncerAmount++;
        
        if (this._spawnBouncerAmount == this._spawnBouncerAmountMax) {
          this._spawnBouncerAmountGrows = false;
          this._spawnDiamonds = true;
        }
      }
    }
  }
  /**
   * Updates variables for spawning Diamonds.
   * @method _tickSpawnDiamonds
   * @memberof WavesLevel
   */
  _tickSpawnDiamonds () {
    if (this._spawnDiamonds)
      this._spawnDiamondTime--;

    // Spawn Bouncers
    if (this._spawnDiamonds && this._spawnDiamondTime <= 0) {
      // Reset the timer
      this._spawnDiamondTime = this._spawnDiamondSpan;

      // Spawn the Bouncers
      this.spawnEntitiesAtRandom(this._spawnDiamondAmount, EntityID.DIAMOND);
      
      // Change the amount of Bouncers spawning
      this._spawnDiamondGrowTime--;
      if (this._spawnDiamondGrowTime <= 0 && 
        (this._spawnDiamondAmountGrows || this._maxReached)) {
        this._spawnDiamondGrowTime = this._spawnDiamondGrowSpan;
        this._spawnDiamondAmount++;
        
        if (this._spawnDiamondAmount == this._spawnDiamondAmountMax) {
          this._spawnDiamondAmountGrows = false;
          if (!this._maxReached) {
            this._maxReached = true;
            this.spawnPointSpawner(0, 0, 100, 5, EntityID.TRIANGLE);
            this.spawnPointSpawner(0, 0, 100, 5, EntityID.TRIANGLE);
            this.spawnPointSpawner(0, 0, 100, 5, EntityID.TRIANGLE);
            this.spawnPointSpawner(0, 0, 100, 5, EntityID.TRIANGLE);
          }
        }
      }
    }
  }
}