/**
 * @class
 * @extends Enemy
 */
class Giant extends Enemy {
  /**
   * @constructor
   * @memberof Giant
   * @param {number} x                     X position.
   * @param {number} y                     Y position.
   * @param {object} model                 Model of the object.
   * @param {number} entitySpawn           Type of entities that will spawn 
   *                                       when this dies.
   * @param {number} [dirHeading = random] Direction where object is heading.
   *                                       Default value is random between 0 
   *                                       and 2.
   */
  constructor (x, y, model, entitySpawn, dirHeading = Math.random() * 2) {
    super(x, y, 150, 150, 3, EntityID.GIANT, model, 10, 0);
    this.dirHeading = dirHeading;
    this.health = 15;

    this.entitySpawn = entitySpawn;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Giant
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    if (!this.isSpawning && !this.isDying && !this.isKiller) {
      // Move the object
      this.move(level.width, level.height, true);
    }
  }
}
