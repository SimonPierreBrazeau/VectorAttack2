/**
 * @class
 * @extends Enemy
 */
class Arrow extends Enemy {
  /**
   * @constructor
   * @memberof Arrow
   * @param {number} x         X position.
   * @param {number} y         Y position.
   * @param {number} [dir = 0] Direction where it is heading.
   */
  constructor (x, y, dir = 0) {
    super(x, y, 40, 40, 5, EntityID.ARROW, Model.createArrow(), 5, 2);
    this.dirHeading = dir;
    this.dirFacing = dir;
    this.oldDirHeading = dir;
    this.flip = false;
    this.isFlipping = false;
    this.flippedX = 0;
    this.flippedY = 0;
    this.rotFac = this.speedAccel / (this.topSpeed * 2);
    // Spawn the flipper
    this.spawnEntity(new Flipper(x, y, this));
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Arrow
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    // Update entity for normal play
    if (!this.isSpawning && !this.isDying && !this.isKiller) {
      if (this.flip && !this.isFlipping) {
        this.isFlipping = true;
        this.flippedX = this.x;
        this.flippedY = this.y;
        this.speed = -this.speed;
        this.oldDirHeading = this.dirHeading;
        this.dirHeading++;
      }

      if (this.isFlipping)  {

        this.dirFacing += this.rotFac;

        if (this.dirFacing >= this.dirHeading) {
          this.isFlipping = false;
          this.dirFacing = this.dirHeading;
          if (this.dirHeading > 2) this.dirHeading -= 2;
          if (this.dirFacing > 2) this.dirFacing -= 2;
        }
      }

      // Move object
      this.move(level.width, level.height);
    }
  }
}
