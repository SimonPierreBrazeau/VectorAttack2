/**
 * @class
 * @extends Enemy
 */
class Block extends Enemy {
  /**
   * @constructor
   * @memberof Block
   * @param {number} x               X position.
   * @param {number} y               Y position.
   * @param {number} [size = random] Size of the object. Default value is 
   *                                 randomly generated between 20 and 100.
   */
  constructor (x, y, size = Math.round(Math.random() * 50) + 50) {
    super(x, y, size, size, 0, EntityID.BLOCK, Model.createBlock(size / 2), 0, 0);
    this.vulnerable = false;

    this.isMoving = false;
    this.moveDelaySpan = 60;
    this.moveDelayTime = this.moveDelaySpan;

    this.moveSpan = 60;
    this.moveTime = this.moveSpan;

    this.topSpeed = size / (this.moveSpan / 2);
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Block
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    if (!this.isKiller && !this.isSpawning && !this.isDying) {
      if (!this.isMoving) {
        // The timers
        this.moveDelayTime--;

        // Moving the thing
        if (this.moveDelayTime <= 0) {
          this.isMoving = true;
          this.moveDelayTime = this.moveDelaySpan;
          this.isGhosting = true;

          // Figure out a new direction to move to
          let dirValid = false;
          while (!dirValid) {
            // Generate a new direction
            this.dirHeading = (Math.round(Math.random() * 3) + 1) / 2;
            if (this.dirHeading == 2) this.dirHeading = 0;

            // Make sure that we are not going towards a wall
            if (this.dirHeading == 0 &&
              this.y - this.height * 1.5 <= -level.height / 2 ||
              this.dirHeading == 0.5 &&
              this.x + this.width * 1.5 >= level.width / 2 ||
              this.dirHeading == 1 &&
              this.y + this.height * 1.5 >= level.height / 2 ||
              this.dirHeading == 1.5 &&
              this.x - this.width * 1.5 <= -level.width / 2)
              continue;
            else
              dirValid = true;
          }
        }
      } else {
        // The block is moving
        this.moveTime--;

        // Change the speed
        if (this.moveTime > this.moveSpan / 2)
          this.speed = (this.topSpeed / (this.moveSpan / 2)) *
            (this.moveSpan - this.moveTime);
        else
          this.speed = (this.topSpeed / (this.moveSpan / 2)) * this.moveTime;

        // Move the damn thing!
        this.move(level.width, level.height, false);

        // Check the timer
        if (this.moveTime <= 0) {
          this.isMoving = false;
          this.moveTime = this.moveSpan;
          this.isGhosting = false;
        }
      }
    }
  }
}
