/**
 * @class
 * @extends Enemy
 */
class Spiral extends Enemy {
  /**
   * @constructor
   * @memberof Spiral
   * @param {number} x                     X position.
   * @param {number} y                     Y position.
   * @param {number} [dirHeading = random] Direction of the object. Default 
   *                                       value is between 0 and 2.
   */
  constructor (x, y, dirHeading = (Math.random() * 2)) {
    super(x, y, 30, 30, 1.2, EntityID.SPIRAL, Model.createSpiral(), 5, 1);

    this.dirHeading = dirHeading;
    this.rotFactor = 0.06;          // Rotation speed of the model
    this.dirChangeSpan = 30;
    this.dirChangeTime = 0;
    this.dirChangeMaxAngle = 0.5;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Spiral
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    // Is the entity spawned and not dying?
    if (!this.isSpawning && !this.isDying && !this.isKiller) {

      // Rotating the model
      this.dirFacing += this.rotFactor;

      // Update the dirChange timer
      this.dirChangeTime++;
      // Timer reached limit?
      if (this.dirChangeTime >= this.dirChangeSpan) {
        this.dirChangeTime = 0;

        // Change the angle
        var positive = Math.round(Math.random());
        var angle = Math.random() * this.dirChangeMaxAngle;

        if (positive == 1)
          this.dirHeading += angle;
        else
          this.dirHeading -= angle;
      }

      // Move the object
      this.move(level.width, level.height);
    }
  }
}
