/**
 * @class
 * @extends Enemy
 */
class Bouncer extends Enemy {
  /**
   * @constructor
   * @memberof Bouncer
   * @param {number} x                     X position.
   * @param {number} y                     Y position.
   * @param {number} [dirHeading = random] Direction of the object. Default 
   *                                       value is between 0 and 2.
   */
  constructor (x, y, dirHeading = (Math.random() * 2)) {
    super(x, y, 48, 48, 7, EntityID.BOUNCER, Model.createBouncer(), 5, 2);
    this.dirHeading = dirHeading;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Bouncer
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    if (!this.isSpawning && !this.isDying && !this.isKiller) {
      // Move the object
      this.move(level.width, level.height, true);
    }
  }
}
