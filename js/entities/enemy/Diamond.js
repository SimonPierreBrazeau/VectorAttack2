/**
 * @class
 * @extends Enemy
 */
class Diamond extends Enemy {
  /**
   * @constructor
   * @memberof Diamond
   * @param {number} x X position.
   * @param {number} y Y position.
   */
  constructor (x, y) {
    super(x, y, 40, 40, 0, EntityID.DIAMOND, Model.createDiamond(), 10, 2);

    this.baseSpeed = 3;               // Initial top speed of object
    this.lifeTime = 0;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Diamond
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    this.lifeTime++;

    if (!this.isSpawning && !this.isDying && !this.isKiller) {
      // Change top speed based on life time
      this.topSpeed = this.baseSpeed * (this.lifeTime / 1000) + this.baseSpeed;

      // Make object face player
      this.dirHeading = Game.calculateAngle(this.x, this.y,
                                            level.player.x, level.player.y);

      // Move the object
      this.move(level.width, level.height);
    }
  }
}
