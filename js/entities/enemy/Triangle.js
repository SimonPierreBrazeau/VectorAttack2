/**
 * @class
 * @extends Enemy
 */
class Triangle extends Enemy {
  /**
   * @constructor
   * @memberof Bouncer
   * @param {number} x X position.
   * @param {number} y Y position.
   */
  constructor (x, y) {
    super(x, y, 20, 20, 3, EntityID.SPIRAL, Model.createTriangle(), 5, 1);

    // Change direction timer
    this.changeDirSpan = 30;
    this.changeDirTime = 0;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Triangle
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    if (!this.isSpawning && !this.isDying && !this.isKiller) {
      this.changeDirTime--;

      if (this.changeDirTime <= 0) {
        // Change direction
        let d = Math.random() * 0.2;
        this.dirHeading = Game.calculateAngle(this.x, this.y,
          level.player.x, level.player.y) + (d / 2) - d;
        this.changeDirTime = this.changeDirSpan;
      }

      // Move object
      this.move(level.width, level.height);
    }
  }
}
