/**
 * @class
 * @extends Enemy
 */
class Slider extends Enemy {
  /**
   * @constructor
   * @memberof Slider
   * @param {number} x X position.
   * @param {number} y Y position.
   */
  constructor (x, y) {
    super(x, y, 30, 30, 0, EntityID.SLIDER, Model.createSlider(), 5, 1);

    this.isMoving = false;
    this.moveDelaySpan = 60;
    this.moveDelayTime = this.moveDelaySpan;

    this.moveSpan = 60;
    this.moveTime = this.moveSpan;

    this.topSpeed = 30 / (this.moveSpan / 2);
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Slider
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);
    if (!this.isKiller && !this.isSpawning && !this.isDying) {
      if (!this.isMoving) {
        // The timers
        this.moveDelayTime--;

        // Moving the thing
        if (this.moveDelayTime <= 0) {
          this.isMoving = true;

          // Figure out a new direction to move to
          this._generateDirection(level.width, level.height);
        }
      } else {
        // The block is moving
        this.moveTime--;

        // Change the speed
        if (this.moveTime > this.moveSpan / 2)
          this.speed = (this.topSpeed / (this.moveSpan / 2)) *
            (this.moveSpan - this.moveTime);
        else
          this.speed = (this.topSpeed / (this.moveSpan / 2)) * this.moveTime;

        // Move the damn thing!
        this.move(level.width, level.height, false);

        // Check the timer
        if (this.moveTime <= 0) {
          this.moveTime = this.moveSpan;

          // Figure out a new direction to move to
          this._generateDirection(level.width, level.height);
        }
      }
    }
  }
  /**
   * Generates a new direction where the Slider would go. If the object is next
   * to a wall, the direction generated will not be towards the wall.
   * @param {number} levelWidth  Width of the currebt Level object.
   * @param {number} levelHeight Height of the current Level object.
   */
  _generateDirection (levelWidth, levelHeight) {
    let dirValid = false;
    while (!dirValid) {
      // Generate a new direction
      this.dirHeading = (Math.round(Math.random() * 3) + 1) / 2;
      if (this.dirHeading == 2) this.dirHeading = 0;

      // Make sure that we are not going towards a wall
      if (this.dirHeading == 0 &&
        this.y - this.height * 1.5 <= -levelHeight / 2 ||
        this.dirHeading == 0.5 &&
        this.x + this.width * 1.5 >= levelWidth / 2 ||
        this.dirHeading == 1 &&
        this.y + this.height * 1.5 >= levelHeight / 2 ||
        this.dirHeading == 1.5 &&
        this.x - this.width * 1.5 <= -levelWidth / 2)
        continue;
      else
        dirValid = true;
    }
  }
}
