/** @class */
class Flipper {
  /**
   * @constructor
   * @memberof Flipper
   * @param {number} x      X position.
   * @param {number} y      Y position.
   * @param {object} master BonusController instance that controls this.
   */
  constructor (x, y, master) {
    this.x = x;
    this.y = y;
    this.width = 40;
    this.height = 40;
    this.isAlive = true;
    this.spawner = [];
    this.entityType = EntityID.FLIPPER;

    this.model = Model.createScoreGem(15);
    this.model.visible = false;

    this.master = master;
    this.flip = false;

    // Distance Stuff
    this.DISTANCE_DEFAULT = 10;
    this.DISTANCE_MIN = 5;
    this.distance = 0;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Flipper
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    this.isAlive = this.master.isAlive;

    if (this.isAlive) {
      // Calculate position relative to master's speed
      this.distance = this.DISTANCE_DEFAULT * this.master.speed;

      //if (this.distance < this.DISTANCE_MIN) this.distance = this.DISTANCE_MIN;

      this.x = this.master.x +
               Math.sin(this.master.dirHeading * Math.PI) * this.distance;

      this.y = this.master.y -
                Math.cos(this.master.dirHeading * Math.PI) * this.distance;

      // Trigger a flip if outside the world
      if (this.x + this.width / 2 > level.width / 2 ||
          this.x - this.width / 2 < -level.width / 2 ||
          this.y + this.height / 2> level.height / 2 ||
          this.y - this.height / 2 < -level.height / 2)
        this.master.flip = true;
      else
        this.master.flip = false;
    }
  }
  /**
   * Updates variables depending on what this object is colliding with.
   * @method colliding
   * @memberof Flipper
   * @param {string} entityType Type of entity is is colliding with.
   */
  colliding (entityType) {
    if (Entities.getObstacles().indexOf(entityType) > -1)
      if (!this.flip) this.flip = true;
  }
}
