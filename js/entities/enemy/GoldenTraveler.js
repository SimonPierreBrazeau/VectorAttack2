/**
 * @class
 * @extends Enemy
 */
class GoldenTraveler extends Enemy {
  /**
   * @constructor
   * @memberof GoldenTraveler
   * @param {number} x                     X position.
   * @param {number} y                     Y position.
   * @param {number} [dirHeading = random] Direction where object is heading.
   *                                       Default value is random between 0 
   *                                       and 2.
   */
  constructor (x, y, dirHeading = (Math.random() * 2)) {
    super(x, y, 48, 48, 14, EntityID.GOLDEN_TRAVELER, 
      Model.createGoldenTraveler(), 50, 10);
    this.dirHeading = dirHeading;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof GoldenTraveler
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    if (!this.isSpawning && !this.isDying && !this.isKiller) {
      // Accelerate
      this.speed += this.speedAccel;
      if (this.speed > this.topSpeed) this.speed = this.topSpeed;

      // Move it
      this.nx += Math.sin(this.dirHeading * Math.PI) * this.speed;
      this.ny -= Math.cos(this.dirHeading * Math.PI) * this.speed;

      // Apply movement to object
      this.x = this.nx;
      this.y = this.ny;

      // Check if it's far enough to fade away
      if (this.dirHeading >= 0.25 && this.dirHeading < 0.75 &&
        this.x > level.width * 0.75 ||
        this.dirHeading >= 0.75 && this.dirHeading < 1.25 &&
        this.y > level.height * 0.75 ||
        this.dirHeading >= 1.25 && this.dirHeading < 1.75 &&
        this.x < -level.width * 0.75 ||
        (this.dirHeading >= 1.75 || this.dirHeading < 0.25) &&
        this.y < -level.height * 0.75)
        this.isFading = true;
    }
  }
}
