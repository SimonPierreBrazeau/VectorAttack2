/** @class */
class Model {
  /**
   * @constructor
   * @memberof Model
   */
  constructor () {
    this.vertices = [];
    this.cvertices = [];
    this.center = [];
    this.edges = [];
    this.ecolors = [];
    this.thickness = [];
    this.polygons = [];
    this.pcolors = [];
    this.scale = 1;
    this.text = [];
    this.visible = true;
  }
  /**
   * Adds an array of coordinates for a vertex in the vertices array.
   * @method addVertex
   * @memberof Model
   * @param {number} x X position.
   * @param {number} y Y position.
   */
  addVertex (x, y) {
    this.vertices.push([x, y]);
    this.cvertices.push([0, 0]);
  }
  /**
   * Adds an array of cordinates for an edges in the edges array and stores its 
   * color and thickness.
   * @method addEdge
   * @memberof Model
   * @param {number} p1        First vertex of the edge.
   * @param {number} p2        Second vertex of the edge.
   * @param {string} color     Color of the edge.
   * @param {number} thickness Thickness of the edge.
   */
  addEdge (p1, p2, color, thickness) {
    this.edges.push([p1, p2]);
    this.ecolors.push(color);
    this.thickness.push(thickness);
  }
  /**
   * Adds an array of vertices in the polygons array and stores its color.
   * @method addPolygon
   * @memberof Model
   * @param {Array<number>} vertices 
   * @param {string} color 
   */
  addPolygon (vertices, color) {
    this.polygons.push(vertices);
    this.pcolors.push(color);
  }
  /**
   * Creates a Text instance and adds it to the text array.
   * @method addText
   * @memberof Model
   * @param {string} string      Text to be displayed.
   * @param {string} identifier  Unique name of the text.
   * @param {string} color       Color of the text.
   * @param {number} [size = 15] Font size.
   * @param {number} [align = 0] Controls how the text is aligned. The
   *                             alignments are:
   *                              - 0: centered;
   *                              - 1: left;
   *                              - 2: right.
   * @param {number} [x = 0]     X position.
   * @param {number} [y = 0]     Y position.
   */
  addText (string, identifier, color, size = 15, align = 0, x = 0, y = 0) {
    this.text.push(new Text(string, identifier, color, size, align, x, y));
  }
  // --- STATIC METHODS ------------------------------------------------------
  /**
   * Returns a Bullet model.
   * @method createBullet
   * @memberof Model
   * @static
   * @param {number} [scale = 3]       Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createBullet (scale = 3, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, -1);
    model.addVertex(1, 0);
    model.addVertex(0, 6);
    model.addVertex(-1, 0);

    // Edges
    model.addEdge(0, 1, "#FC0", 2);
    model.addEdge(1, 2, "#FC0", 2);
    model.addEdge(2, 3, "#FC0", 2);
    model.addEdge(3, 0, "#FC0", 2);

    // Polygons
    model.addPolygon([0, 1, 2, 3], "#B38F00");

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Spiral model.
   * @method createSpiral
   * @memberof Model
   * @static
   * @param {number} [scale = 15]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createSpiral (scale = 15, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, -1);
    model.addVertex(1, -1);
    model.addVertex(1, 0);
    model.addVertex(1, 1);
    model.addVertex(0, 1);
    model.addVertex(-1, 1);
    model.addVertex(-1, 0);
    model.addVertex(-1, -1);
    model.addVertex(0, 0);

    // Edges
    model.addEdge(0, 1, "#C117C9", 2);
    model.addEdge(1, 8, "#C117C9", 2);
    model.addEdge(8, 0, "#C117C9", 2);
    model.addEdge(2, 3, "#C117C9", 2);
    model.addEdge(3, 8, "#C117C9", 2);
    model.addEdge(8, 2, "#C117C9", 2);
    model.addEdge(4, 5, "#C117C9", 2);
    model.addEdge(5, 8, "#C117C9", 2);
    model.addEdge(8, 4, "#C117C9", 2);
    model.addEdge(6, 7, "#C117C9", 2);
    model.addEdge(7, 8, "#C117C9", 2);
    model.addEdge(8, 6, "#C117C9", 2);

    // Polygons
    model.addPolygon([0, 1, 8], "#6D0D72");
    model.addPolygon([2, 3, 8], "#6D0D72");
    model.addPolygon([4, 5, 8], "#6D0D72");
    model.addPolygon([6, 7, 8], "#6D0D72");

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Diamond model.
   * @method createDiamond
   * @memberof Model
   * @static
   * @param {number} [scale = 20]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createDiamond (scale = 20, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, -1);
    model.addVertex(1, 0);
    model.addVertex(0, 1);
    model.addVertex(-1, 0);

    // Edges
    model.addEdge(0, 1, "#0CF", 2);
    model.addEdge(1, 2, "#0CF", 2);
    model.addEdge(2, 3, "#0CF", 2);
    model.addEdge(3, 0, "#0CF", 2);

    // Polygons
    model.addPolygon([0, 1, 2, 3], "#00A3CC");

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Arrow model.
   * @method createArrow
   * @memberof Model
   * @static
   * @param {number} [scale = 20]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createArrow(scale = 20, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, -1);
    model.addVertex(-1, 1);
    model.addVertex(0, 0.65);
    model.addVertex(1, 1);

    // Edges
    model.addEdge(0, 1, "#F93", 2);
    model.addEdge(1, 2, "#F93", 2);
    model.addEdge(2, 3, "#F93", 2);
    model.addEdge(3, 0, "#F93", 2);
    model.addEdge(0, 2, "#F93", 2);

    // Polygons
    model.addPolygon([0, 1, 2, 3], "#C60");

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Player model.
   * @method createPlayer
   * @memberof Model
   * @static
   * @param {number} [scale = 10]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createPlayer (scale = 10, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, -2);
    model.addVertex(2, 1);
    model.addVertex(0.5, 2);
    model.addVertex(1.25, 1);
    model.addVertex(0, -0.5);
    model.addVertex(-1.25, 1);
    model.addVertex(-0.5, 2);
    model.addVertex(-2, 1);
    model.addVertex(0, 0.5);
    model.addVertex(0.25, 0.75);
    model.addVertex(0, 1.5);
    model.addVertex(-0.25, 0.75);

    // Edges
    model.addEdge(0, 1, "#FFF", 2);
    model.addEdge(1, 2, "#FFF", 2);
    model.addEdge(2, 3, "#FFF", 2);
    model.addEdge(3, 4, "#FFF", 2);
    model.addEdge(4, 5, "#FFF", 2);
    model.addEdge(5, 6, "#FFF", 2);
    model.addEdge(6, 7, "#FFF", 2);
    model.addEdge(7, 0, "#FFF", 2);
    model.addEdge(8, 9, "#F50", 1);
    model.addEdge(9, 10, "#F50", 1);
    model.addEdge(10, 11, "#F50", 1);
    model.addEdge(11, 8, "#F50", 1);

    // Polygons
    model.addPolygon([0, 1, 2, 3, 4, 5, 6, 7], "#BBB");
    model.addPolygon([8, 9, 10, 11], "#930");

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a ScoreGem model.
   * @method createScoreGem
   * @memberof Model
   * @static
   * @param {number} [scale = 5]       Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createScoreGem (scale = 5, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, 1);
    model.addVertex(0.5, 0);
    model.addVertex(0, -1);
    model.addVertex(-0.5, 0);

    // Edges
    model.addEdge(0, 1, "#0F0", 1);
    model.addEdge(1, 2, "#0F0", 1);
    model.addEdge(2, 3, "#0F0", 1);
    model.addEdge(3, 0, "#0F0", 1);

    // Polygons
    model.addPolygon([0, 1, 2, 3], "#090");

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a CombatCursor model.
   * @method createCombatCursor
   * @memberof Model
   * @static
   * @param {number} [scale = 10]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createCombatCursor (scale = 10, visible = true) {
    var model = new Model();

    // Vertices
    model.addVertex(0, -1);
    model.addVertex(0, 1);
    model.addVertex(-1, 0);
    model.addVertex(1, 0);

    // Edges
    model.addEdge(0, 1, "#FFF", 2);
    model.addEdge(2, 3, "#FFF", 2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Button model.
   * @method createButtonModel
   * @memberof Model
   * @static
   * @param {number} width                    Width of the button.
   * @param {number} height                   Height of the button.
   * @param {string} [border = "#4bdfff"]     Color of the edges of the button.
   * @param {string} [background = "#3f92a4"] Color of the back of the button.
   * @param {number} [scale = 1]              Scale of the model.
   * @param {boolean} [visible = true]        Controls whether the model is
   *                                          visible or not.
   * @returns {object}
   */
  static createButtonModel (width, height, border = "#4bdfff",
    background = "#3f92a4", scale = 1, visible = true) {
    let model = new Model();

    // Variables
    let w = width / 2;
    let h = height / 2;
    let c1 = border;
    let c2 = background;

    // Vertices
    model.addVertex(-w, -h);
    model.addVertex(w, -h);
    model.addVertex(w, h);
    model.addVertex(-w, h);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a BonusPeg model.
   * @method createBonusPeg
   * @memberof Model
   * @static
   * @param {number} [scale = 10]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createBonusPeg(scale = 10, visible = true) {
    let model = new Model();

    let c1 = "#fc86e2";
    let c2 = "#ecb6e0";

    // Vertices
    model.addVertex(0.5, -1);
    model.addVertex(1, -0.5);
    model.addVertex(1, 0.5);
    model.addVertex(0.5, 1);
    model.addVertex(-0.5, 1);
    model.addVertex(-1, 0.5);
    model.addVertex(-1, -0.5);
    model.addVertex(-0.5, -1);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 4, c1, 2);
    model.addEdge(4, 5, c1, 2);
    model.addEdge(5, 6, c1, 2);
    model.addEdge(6, 7, c1, 2);
    model.addEdge(7, 0, c1, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3, 4, 5, 6, 7], c2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Text model.
   * @method createTextModel
   * @memberof Model
   * @static
   * @param {string} string      Text to be displayed.
   * @param {string} identifier  Unique name of the text.
   * @param {string} color       Color of the text.
   * @param {number} [size = 15] Font size.
   * @param {number} [align = 0] Controls how the text is aligned. The
   *                             alignments are:
   *                              - 0: centered;
   *                              - 1: left;
   *                              - 2: right.
   * @param {number} [x = 0]     X position.
   * @param {number} [y = 0]     Y position.
   * @returns {object}
   */
  static createTextModel (string, identifier, color, size = 15, align = 0,
                     x = 0, y = 0) {
    let model = new Model();

    model.addText(string, identifier, color, size, align, x, y);

    return model;
  }
  /**
   * Returns a LogoBase model.
   * @method createLogoBase
   * @memberof Model
   * @static
   * @param {number} [scale = 10]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createLogoBase (scale = 10, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#0CF";
    let c2 = "#C117C9";
    let c3 = "#00A3CC";
    let c4 = "#6D0D72";

    // Vertices
    model.addVertex(-3, -3);
    model.addVertex(-1, 3);
    model.addVertex(1, -3);
    model.addVertex(3, 3);
    model.addVertex(-1, 1);
    model.addVertex(1, -1);

    // Edges
    model.addEdge(2, 3, c2, 2);
    model.addEdge(3, 5, c2, 2);
    model.addEdge(5, 1, c2, 2);

    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 4, c1, 2);
    model.addEdge(4, 0, c1, 2);

    // Polygon
    model.addPolygon([2, 3, 5, 1], c4);
    model.addPolygon([0, 1, 2, 4], c3);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a LogoGem model.
   * @method createLogoGem
   * @memberof Model
   * @static
   * @param {number} [scale = 10]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createLogoGem (scale = 10, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#0F0";
    let c2 = "#090";

    // Vertices
    model.addVertex(0, -4);
    model.addVertex(1, 0);
    model.addVertex(0, 4);
    model.addVertex(-1, 0);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2)

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Bomb model.
   * @method createBomb
   * @memberof Model
   * @static
   * @param {number} [scale = 5]       Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createBomb(scale = 5, visible = true) {
    let model = new Model();

    let c1 = "#ffb13b";
    let c2 = "#fecd84";

    let e1 = 1;
    let e2 = 2;

    // Vertices
    model.addVertex(0.5, -1);
    model.addVertex(1, -0.5);
    model.addVertex(1, 0.5);
    model.addVertex(0.5, 1);
    model.addVertex(-0.5, 1);
    model.addVertex(-1, 0.5);
    model.addVertex(-1, -0.5);
    model.addVertex(-0.5, -1);

    model.addVertex(1, -2);
    model.addVertex(2, -1);
    model.addVertex(2, 1);
    model.addVertex(1, 2);
    model.addVertex(-1, 2);
    model.addVertex(-2, 1);
    model.addVertex(-2, -1);
    model.addVertex(-1, -2);

    // Edges
    model.addEdge(0, 1, c1, e1);
    model.addEdge(1, 2, c1, e1);
    model.addEdge(2, 3, c1, e1);
    model.addEdge(3, 4, c1, e1);
    model.addEdge(4, 5, c1, e1);
    model.addEdge(5, 6, c1, e1);
    model.addEdge(6, 7, c1, e1);
    model.addEdge(7, 0, c1, e1);

    model.addEdge(8, 9, c1, e2);
    model.addEdge(9, 10, c1, e2);
    model.addEdge(10, 11, c1, e2);
    model.addEdge(11, 12, c1, e2);
    model.addEdge(12, 13, c1, e2);
    model.addEdge(13, 14, c1, e2);
    model.addEdge(14, 15, c1, e2);
    model.addEdge(15, 8, c1, e2);

    // Polygon
    model.addPolygon([0, 1, 2, 3, 4, 5, 6, 7], c2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Block model.
   * @method createBlock
   * @memberof Model
   * @static
   * @param {number} [scale = 20]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createBlock (scale = 20, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#da1f35";
    let c2 = "#991827";

    // Verticies
    model.addVertex(-1, -1);
    model.addVertex(-1, 1);
    model.addVertex(1, 1);
    model.addVertex(1, -1);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Bouncer model.
   * @method createBouncer
   * @memberof Model
   * @static
   * @param {number} [scale = 8]       Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createBouncer (scale = 8, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#de1cb2";
    let c2 = "#b41790";
    let c3 = "#ffffff";
    let c4 = "#b3b3b3";

    // Vertices
    model.addVertex(0, -1);
    model.addVertex(1, 0);
    model.addVertex(0, 1);
    model.addVertex(-1, 0);

    model.addVertex(0, -2);
    model.addVertex(1.5, -1.5);
    model.addVertex(2, 0);
    model.addVertex(1.5, 1.5);
    model.addVertex(0, 2);
    model.addVertex(-1.5, 1.5);
    model.addVertex(-2, 0);
    model.addVertex(-1.5, -1.5);

    model.addVertex(0, -2.75);
    model.addVertex(2, -2);
    model.addVertex(2.75, 0);
    model.addVertex(2, 2);
    model.addVertex(0, 2.75);
    model.addVertex(-2, 2);
    model.addVertex(-2.75, 0);
    model.addVertex(-2, -2);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);

    model.addEdge(4, 5, c3, 2);
    model.addEdge(5, 6, c3, 2);
    model.addEdge(6, 7, c3, 2);
    model.addEdge(7, 8, c3, 2);
    model.addEdge(8, 9, c3, 2);
    model.addEdge(9, 10, c3, 2);
    model.addEdge(10, 11, c3, 2);
    model.addEdge(11, 4, c3, 2);

    model.addEdge(12, 13, c3, 2);
    model.addEdge(13, 14, c3, 2);
    model.addEdge(14, 15, c3, 2);
    model.addEdge(15, 16, c3, 2);
    model.addEdge(16, 17, c3, 2);
    model.addEdge(17, 18, c3, 2);
    model.addEdge(18, 19, c3, 2);
    model.addEdge(19, 12, c3, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2);

    model.addPolygon([4, 5, 13, 12], c4);
    model.addPolygon([5, 6, 14, 13], c4);
    model.addPolygon([6, 7, 15, 14], c4);
    model.addPolygon([7, 8, 16, 15], c4);
    model.addPolygon([8, 9, 17, 16], c4);
    model.addPolygon([9, 10, 18, 17], c4);
    model.addPolygon([10, 11, 19, 18], c4);
    model.addPolygon([11, 4, 12, 19], c4);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Traveler model.
   * @method createTraveler
   * @memberof Model
   * @static
   * @param {number} [scale = 8]       Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createTraveler (scale = 8, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#F93";
    let c2 = "#C60";
    let c3 = "#ffffff";
    let c4 = "#b3b3b3";

    // Vertices
    model.addVertex(-1, -1);
    model.addVertex(1, -1);
    model.addVertex(1, 1);
    model.addVertex(-1, 1);

    model.addVertex(0, -2);
    model.addVertex(1.5, -1.5);
    model.addVertex(2, 0);
    model.addVertex(1.5, 1.5);
    model.addVertex(0, 2);
    model.addVertex(-1.5, 1.5);
    model.addVertex(-2, 0);
    model.addVertex(-1.5, -1.5);

    model.addVertex(0, -2.75);
    model.addVertex(2, -2);
    model.addVertex(2.75, 0);
    model.addVertex(2, 2);
    model.addVertex(0, 2.75);
    model.addVertex(-2, 2);
    model.addVertex(-2.75, 0);
    model.addVertex(-2, -2);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);

    model.addEdge(4, 5, c3, 2);
    model.addEdge(5, 6, c3, 2);
    model.addEdge(6, 7, c3, 2);
    model.addEdge(7, 8, c3, 2);
    model.addEdge(8, 9, c3, 2);
    model.addEdge(9, 10, c3, 2);
    model.addEdge(10, 11, c3, 2);
    model.addEdge(11, 4, c3, 2);

    model.addEdge(12, 13, c3, 2);
    model.addEdge(13, 14, c3, 2);
    model.addEdge(14, 15, c3, 2);
    model.addEdge(15, 16, c3, 2);
    model.addEdge(16, 17, c3, 2);
    model.addEdge(17, 18, c3, 2);
    model.addEdge(18, 19, c3, 2);
    model.addEdge(19, 12, c3, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2);

    model.addPolygon([4, 5, 13, 12], c4);
    model.addPolygon([5, 6, 14, 13], c4);
    model.addPolygon([6, 7, 15, 14], c4);
    model.addPolygon([7, 8, 16, 15], c4);
    model.addPolygon([8, 9, 17, 16], c4);
    model.addPolygon([9, 10, 18, 17], c4);
    model.addPolygon([10, 11, 19, 18], c4);
    model.addPolygon([11, 4, 12, 19], c4);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Golden Traveler model.
   * @method createGoldenTraveler
   * @memberof Model
   * @static
   * @param {number} [scale = 8]       Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createGoldenTraveler (scale = 8, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#F93";
    let c2 = "#C60";
    let c3 = "#ffe641";
    let c4 = "#d2bc2b";

    // Vertices
    model.addVertex(-1, -1);
    model.addVertex(1, -1);
    model.addVertex(1, 1);
    model.addVertex(-1, 1);

    model.addVertex(0, -2);
    model.addVertex(1.5, -1.5);
    model.addVertex(2, 0);
    model.addVertex(1.5, 1.5);
    model.addVertex(0, 2);
    model.addVertex(-1.5, 1.5);
    model.addVertex(-2, 0);
    model.addVertex(-1.5, -1.5);

    model.addVertex(0, -2.75);
    model.addVertex(2, -2);
    model.addVertex(2.75, 0);
    model.addVertex(2, 2);
    model.addVertex(0, 2.75);
    model.addVertex(-2, 2);
    model.addVertex(-2.75, 0);
    model.addVertex(-2, -2);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);

    model.addEdge(4, 5, c3, 2);
    model.addEdge(5, 6, c3, 2);
    model.addEdge(6, 7, c3, 2);
    model.addEdge(7, 8, c3, 2);
    model.addEdge(8, 9, c3, 2);
    model.addEdge(9, 10, c3, 2);
    model.addEdge(10, 11, c3, 2);
    model.addEdge(11, 4, c3, 2);

    model.addEdge(12, 13, c3, 2);
    model.addEdge(13, 14, c3, 2);
    model.addEdge(14, 15, c3, 2);
    model.addEdge(15, 16, c3, 2);
    model.addEdge(16, 17, c3, 2);
    model.addEdge(17, 18, c3, 2);
    model.addEdge(18, 19, c3, 2);
    model.addEdge(19, 12, c3, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2);

    model.addPolygon([4, 5, 13, 12], c4);
    model.addPolygon([5, 6, 14, 13], c4);
    model.addPolygon([6, 7, 15, 14], c4);
    model.addPolygon([7, 8, 16, 15], c4);
    model.addPolygon([8, 9, 17, 16], c4);
    model.addPolygon([9, 10, 18, 17], c4);
    model.addPolygon([10, 11, 19, 18], c4);
    model.addPolygon([11, 4, 12, 19], c4);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Slider model.
   * @method createSlider
   * @memberof Model
   * @static
   * @param {number} [scale = 15]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createSlider (scale = 15, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#da1fc7";
    let c2 = "#ad199e";

    // Verticies
    model.addVertex(-1, -1);
    model.addVertex(-1, 1);
    model.addVertex(1, 1);
    model.addVertex(1, -1);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 3, c1, 2);
    model.addEdge(3, 0, c1, 2);
    model.addEdge(0, 2, c1, 2);

    // Polygon
    model.addPolygon([0, 1, 2, 3], c2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  /**
   * Returns a Triangle model.
   * @method createTriangle
   * @memberof Model
   * @static
   * @param {number} [scale = 10]      Scale of the model.
   * @param {boolean} [visible = true] Controls whether the model is visible
   *                                   or not.
   * @returns {object}
   */
  static createTriangle (scale = 10, visible = true) {
    let model = new Model();

    // Colors
    let c1 = "#F93";
    let c2 = "#C60";

    // Verticies
    model.addVertex(0, -0.866);
    model.addVertex(1, 0.866);
    model.addVertex(-1, 0.866);

    // Edges
    model.addEdge(0, 1, c1, 2);
    model.addEdge(1, 2, c1, 2);
    model.addEdge(2, 0, c1, 2);

    // Polygon
    model.addPolygon([0, 1, 2], c2);

    // Scale
    model.scale = scale;

    // Visible
    model.visible = visible;

    return model;
  }
  // }}}
}
