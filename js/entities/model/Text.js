/** @class */
class Text {
  /**
   * @constructor
   * @memberof Text
   * @param {string} string      Text to be displayed.
   * @param {string} identifier  Unique name of the text.
   * @param {string} color       Color of the text.
   * @param {number} [size = 15] Font size.
   * @param {number} [align = 0] Controls how the text is aligned. The
   *                             alignments are:
   *                              - 0: centered;
   *                              - 1: left;
   *                              - 2: right.
   * @param {number} [x = 0]     X position.
   * @param {number} [y = 0]     Y position.
   */
  constructor (string, identifier, color, size = 15, align = 0, x = 0, y = 0) {
    this.string = string;
    this.xoff = x;
    this.yoff = y;
    this.size = size;
    this.color = color;
    this.align = align;
    this.identifier = identifier;
  }
}
