/** @class */
class ScoreGem {
  /**
   * @constructor
   * @memberof ScoreGem
   * @param {number} x            X position.
   * @param {number} y            Y position.
   * @param {number} [amount = 1] Value of the gem.
   */
  constructor (x, y, amount = 1) {
    this.x = x;
    this.y = y;
    this.nx = x;
    this.ny = y;
    this.width = 5 * amount;
    this.height = 5 * amount;
    this.dirFacing = 0;
    this.entityType = EntityID.SCORE_GEM;
    this.gemValue = amount;
    this.isAlive = true;
    this.collected = false;
    this.isFlashing = false;

    this.lifeSpan = 4 * 60;         // 4 second life span
    this.flashBegin = 1.5 * 60;     // Start flashing model after 1.5 seconds
    this.flashInterval = 0.1 * 60;  // Flash every 0.1 second
    this.lifeTime = this.lifeSpan;

    var rt = Math.round(Math.random());
    this.rotFactor = rt == 1? 0.005 : -0.005;
    this.dirHeading = Math.random() * 2;
    this.speed = Math.random() * 5 + 0.5;
    this.decelFactor = 0.95;
    this.magnetDistance = 150;
    this.magnetSpeed = 5;
    this.magnetAcc = 0.25;
    this.isMagnetized = false;
    this.setLocation = false;

    this.model = Model.createScoreGem(3.5 + (1.5 * amount));
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof ScoreGem
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    // Update the lifeTime
    this.lifeTime--;

    // Make sure it's still inside the level
    if (!this.setLocation) {
      if (this.x < -level.width / 2)
        this.x = -level.width / 2;
      else if (this.x > level.width / 2)
        this.x = level.width / 2;

      if (this.y < -level.height / 2)
        this.y = -level.height / 2;
      else if (this.y > level.height / 2)
        this.y = level.height / 2;

      this.nx = this.x;
      this.ny = this.y;

      this.setLocation = true;
    }

    // Is the object flashing?
    if (this.lifeTime <= this.flashBegin && !this.isFlashing)
      this.isFlashing = true;

    // Make the object flash
    if (this.isFlashing && this.lifeTime % this.flashInterval == 0) {
      if (this.model.visible)
        this.model.visible = false;
      else
        this.model.visible = true;
    }

    // Is the object alive?
    if (this.lifeTime <= 0)
      this.isAlive = false;
    else {
      // Rotate the model
      this.dirFacing += this.rotFactor;

      // Grab the player's coordinates
      var px = level.player.x;
      var py = level.player.y;

      // Calculate the distance between the player and the gem
      var dx = Math.abs(px - this.x);
      var dy = Math.abs(py - this.y);
      var distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

      // Is gem close enough to player?
      if (distance < this.magnetDistance) {
        this.isMagnetized = true;

        // Make gem go to player
        var destCos = Math.asin(dx / distance) / Math.PI * 2;
        var destSin = Math.acos(dx / distance) / Math.PI * 2;
        var destTan = Math.atan(dx / dy) / Math.PI;

        if (px < this.x) destCos = -destCos;
        if (py > this.y) destSin = -destSin;

        // Calculate the angle
        // Top right
        if (destSin >= 0 && destSin <= 1 && destCos >= 0 && destCos <= 1)
          this.dirHeading = destTan; // 1 - t
        // Top left
        else if (destSin >= 0 && destSin <= 1 && destCos >= -1 && destCos <= 0)
          this.dirHeading = 2 - destTan;
        // Bottom left
        else if (destSin >= -1 && destSin <= 0 && destCos >= -1 && destCos <= 0)
          this.dirHeading = 1 + destTan;
        // Bottom Right
        else
          this.dirHeading = 1 - destTan;

        this.speed += this.magnetAcc;
      } else {
        this.isMagnetized = false;
      }
      // Move the object
      this.nx += Math.sin(this.dirHeading * Math.PI) * this.speed;
      this.ny -= Math.cos(this.dirHeading * Math.PI) * this.speed;

      // Check if the object is still in the level
      if (this.nx + this.width / 2 > level.width / 2
      || this.nx - this.width / 2 < -level.width / 2)
        this.nx = this.x;

      if (this.ny + this.height / 2 > level.height / 2
      || this.ny - this.height / 2 < -level.height / 2)
        this.ny = this.y;

      // Apply movement to object
      this.x = this.nx;
      this.y = this.ny;

      // Slow the object down
      if (!this.isMagnetized)
        this.speed *= this.decelFactor;
    }
  }
  /**
   * Updates variables depending on what this object is colliding with.
   * @method colliding
   * @memberof ScoreGem
   * @param {number} entityType Type of entity is is colliding with.
   */
  colliding (entityType) {
    switch (entityType) {
      case EntityID.PLAYER:
        this.isAlive = false;
        this.collected = true;
        break;
      default:

    }
  }
}
