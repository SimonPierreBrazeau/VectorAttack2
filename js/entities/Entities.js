var EntityID = {
  DEAD: -1,
  SPIRAL: 0,
  DIAMOND: 1,
  ARROW: 2,
  SCORE_GEM: 3,
  BONUS_PEG: 4,
  BONUS_CONTROLLER: 5,
  BLOCK: 6,
  BOUNCER: 7,
  TRAVELER: 8,
  GOLDEN_TRAVELER: 9,
  SLIDER: 10,
  TRIANGLE: 11,
  GIANT: 12,
  GIANT_SPIRAL: 13,
  GIANT_DIAMOND: 14,
  GIANT_SLIDER: 15,
  GIANT_BOUNCER: 16,
  FLIPPER: 17,
  SCORE_POP: 18,
  SPAWNER: 50,
  POINT_SPAWNER: 51,
  RANDOM_SPAWNER: 52,
  LABEL: 80,
  ICON: 81,
  BUTTON: 82,
  BOMB: 98,
  BULLET: 99,
  GHOST: 100,
  ENEMY: 101,
  GENERIC: 102,
  PLAYER: 129
};
/** @class */
class Entities {
  /**
   * Returns a string containing the name of a given entity ID.
   * @method toString
   * @memberof Entities
   * @static
   * @returns {string}
   */
  static toString (entityID) {
    switch (entityID) {
      case EntityID.DEAD: return "Dead edge";
      case EntityID.SPIRAL: return "Spiral";
      case EntityID.DIAMOND: return "Diamond";
      case EntityID.ARROW: return "Arrow";
      case EntityID.SCORE_GEM: return "Score gem";
      case EntityID.BONUS_PEG: return "Bonus peg";
      case EntityID.BONUS_CONTROLLER: return "Bonus controller";
      case EntityID.BLOCK: return "Block";
      case EntityID.BOUNCER: return "Bouncer";
      case EntityID.TRAVELER: return "Traveler";
      case EntityID.GOLDEN_TRAVELER: return "Golden traveler";
      case EntityID.SLIDER: return "Slider";
      case EntityID.TRIANGLE: return "Triangle";
      case EntityID.GIANT: return "Giant";
      case EntityID.GIANT_SPIRAL: return "Giant spiral";
      case EntityID.GIANT_DIAMOND: return "Giant diamond";
      case EntityID.GIANT_SLIDER: return "Giant slider";
      case EntityID.GIANT_BOUNCER: return "Giant bouncer";
      case EntityID.FLIPPER: return "Flipper";
      case EntityID.SCORE_POP: return "Score pop";
      case EntityID.SPAWNER: return "Spawner";
      case EntityID.POINT_SPAWNER: return "Point spawner";
      case EntityID.RANDOM_SPAWNER: return "Random spawner";
      case EntityID.LABEL: return "Label";
      case EntityID.ICON: return "Icon";
      case EntityID.BUTTON: return "Button";
      case EntityID.BOMB: return "Bomb";
      case EntityID.BULLET: return "Bullet";
      case EntityID.GHOST: return "Ghost";
      case EntityID.ENEMY: return "Enemy";
      case EntityID.GENERIC: return "Generic";
      case EntityID.PLAYER: return "Player";
      default: return "Unknown";
    }
  }
  /**
   * Returns an array of entity IDs that the player can collide with.
   * @method getPlayerCollidables
   * @memberof Entities
   * @static
   * @returns {Array<number>}
   */
  static getPlayerCollidables () {
    return [
      EntityID.SPIRAL,
      EntityID.DIAMOND,
      EntityID.ARROW,
      EntityID.SCORE_GEM,
      EntityID.BONUS_PEG,
      EntityID.BLOCK,
      EntityID.BOUNCER,
      EntityID.TRAVELER,
      EntityID.GOLDEN_TRAVELER,
      EntityID.SLIDER,
      EntityID.TRIANGLE,
      EntityID.GIANT
    ];
  }
  /**
   * Returns an array of entity IDs that the player can be killed by.
   * @method getPlayerKillers
   * @memberof Entities
   * @static
   * @returns {Array<number>}
   */
  static getPlayerKillers () {
    return [
      EntityID.SPIRAL,
      EntityID.DIAMOND,
      EntityID.ARROW,
      EntityID.BONUS_PEG,
      EntityID.BLOCK,
      EntityID.BOUNCER,
      EntityID.TRAVELER,
      EntityID.GOLDEN_TRAVELER,
      EntityID.SLIDER,
      EntityID.TRIANGLE,
      EntityID.GIANT
    ];
  }
  /**
   * Returns an array of entity IDs that the player can destroy.
   * @method getPlayerDestroyables
   * @memberof Entities
   * @static
   * @returns {Array<number>}
   */
  static getPlayerDestroyables () {
    return [
      EntityID.SPIRAL,
      EntityID.DIAMOND,
      EntityID.ARROW,
      EntityID.BONUS_PEG,
      EntityID.BLOCK,
      EntityID.BOUNCER,
      EntityID.TRAVELER,
      EntityID.GOLDEN_TRAVELER,
      EntityID.SLIDER,
      EntityID.TRIANGLE,
      EntityID.GIANT
    ];
  }
  /**
   * Returns an array of entity IDs that entities can collide with.
   * @method getObstacles
   * @memberof Entities
   * @static
   * @returns {Array<number>}
   */
  static getObstacles () {
    return [
      EntityID.BLOCK
    ];
  }
}