/** @class */
class Generic {
  /**
   * @constructor
   * @memberof Generic
   * @param {number} x                   X position.
   * @param {number} y                   Y position.
   * @param {object} [model = new Model] Model instance of the entity.
   */
  constructor (x, y, model = new Model()) {
    this.x = x;
    this.y = y;
    this.superType = EntityID.GENERIC;
    this.entityType = EntityID.GENERIC;
    this.identifier = "";

    this.dirFacing = 0;

    this.isAlive = true;

    this.model = model;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Generic
   */
  tick () { }
}
