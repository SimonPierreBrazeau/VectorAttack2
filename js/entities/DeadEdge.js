/** @class */
class DeadEdge {
  /**
   * @constructor
   * @memberof DeadEdge
   * @param {number} x        x position.
   * @param {number} y        Y position.
   * @param {number} rotation Angle at which the edge is.
   * @param {number} dir      Direction of the edge.
   * @param {number} width    Width of the edge.
   * @param {number} height   Height of the edge.
   */
  constructor (x, y, rotation, dir, width, height) {
    this.x = x;
    this.y = y;
    this.nx = x;
    this.ny = y;
    this.dirFacing = rotation;
    this.dirHeading = dir;
    this.entityType = EntityID.DEAD;
    this.width = width;
    this.height = height;

    this.speed = 5;
    this.decelFactor = 0.85;
    this.rotFact = 0;

    this.model = new Model;

    this.baseScale = 0;

    this.isAlive = true;
    this.lifeSpan = 30;
    this.lifeTime = this.lifeSpan;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof DeadEdge
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    // Update life timer
    this.lifeTime--;
    if (this.lifeTime <= 0) this.isAlive = false;

    if (this.isAlive) {
      // Move the object
      this.nx += Math.sin(this.dirHeading * Math.PI) * this.speed;
      this.ny -= Math.cos(this.dirHeading * Math.PI) * this.speed;

      // Check if the object is still in the level
      if (this.nx + this.width / 2 > level.width / 2
      || this.nx - this.width / 2 < -level.width / 2)
        this.nx = this.x;

      if (this.ny + this.height / 2 > level.height / 2
      || this.ny - this.height / 2 < -level.height / 2)
        this.ny = this.y;

      if (this.nx >= level.width / 2) 
        this.nx = level.width / 2;
      else if (this.nx <= -level.width / 2) 
        this.nx = -level.width / 2;

      if (this.ny >= level.height / 2) 
        this.ny = level.height / 2;
      else if (this.ny <= -level.height / 2) 
        this.ny = -level.height / 2;

      // Apply movement to object
      this.x = this.nx;
      this.y = this.ny;

      // Slow the edge down
      this.speed = this.speed * this.decelFactor;

      // Scale the edge down based on the life time
      this.model.scale = this.baseScale * (this.lifeTime / this.lifeSpan);
    }
  }
}
