/** @class */
class Ghost extends Generic {
  /**
   * @constructor
   * @memberof Ghost
   * @param {number} x                X position.
   * @param {number} y                Y position.
   * @param {number} dirFacing        Direction of the entity.
   * @param {number} lifeSpan         How many frames the entity lasts.
   * @param {object} model            Model of the entity.
   * @param {number} width            Width of the entity.
   * @param {number} height           Height of the entity.
   * @param {boolean} [fading = true] Controls whether it fades or not.
   */
  constructor (x, y, dirFacing, lifeSpan, model, width, height, fading = true) {
    super(x, y, model);
    this.entityType = EntityID.GHOST;
    this.dirFacing = dirFacing;
    this.width = width * 2;
    this.height = height * 2;

    this.lifeSpan = lifeSpan;
    this.lifeTime = this.lifeSpan;

    this.isFading = false;
    this.fadeSpan = 20;
    this.fadeTime = this.fadeSpan;
    this.willFade = fading;

    this.model.renderPolygons = false;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Ghost
   */
  tick () {
    if (!this.isFading) {
      this.lifeTime--;

      if (this.lifeTime <= 0)
        if (this.willFade)
          this.isFading = true;
        else
          this.isAlive = false;
    } else {
      this.fadeTime--;
      if (this.fadeTime <= 0) this.isAlive = false;
    }
  }
}
