/** 
 * @class
 * @extends Generic
 */
class Bomb extends Generic {
  /**
   * @constructor
   * @memberof Bomb
   * @param {number} x                   X position of the center of the bomb.
   * @param {number} y                   Y position of the center of the bomb.
   * @param {number} size                Maximum size of the bomb.
   * @param {string} [color = "#db6647"] Color of the bomb.
   */
  constructor (x, y, size, color = "#db6647") {
    super(x, y, null);
    this.entityType = EntityID.BOMB;
    this.color = color;

    this.lifeSpan = 20;
    this.lifeTime = this.lifeSpan;

    this.targetSize = size;
    this.currentSize = 0;
    this.growthRate = size / this.lifeSpan;
  }
  /**
   * Updates the bomb's variables.
   * @method tick
   * @memberof Bomb
   */
  tick () {
    this.currentSize += this.growthRate;
    this.lifeTime--;

    if (this.lifeTime <= 0)
      this.isAlive = false;
  }
}
