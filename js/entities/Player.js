/** @class */
class Player {
  /**
   * @constructor
   * @memberof Player
   */
  constructor () {
    // Positionning
    this.x = 0;                 // Global x position
    this.y = 0;                 // Global y position
    this.nx = this.x;
    this.ny = this.y;
    this.width = 30;                    // Width of object
    this.height = 30;                   // Height of object
    this.entityType = EntityID.PLAYER;  // Type of entity
    this.isAlive = false;               // Is the entity alive?
    this.isDying = false;               // Has entity been touched by enemy?
    this.isSpawning = true;             // Is the entity spawning?

    this.spawnSpan = 60;
    this.spawnTime = this.spawnSpan;

    // On-screen position
    this.onScreenx = 0;         // On screen x position
    this.onScreeny = 0;         // On screen y position

    // Moving around
    this.speed = 0;             // Movement speed of player
    this.acc = 0.75;            // Movement acceleration
    this.maxSpeed = 6;          // Movement top speed
    this.isMoving = false;      // Is the player moving?

    // Directions
    this.dirFacing = 0;         // Direction player is facing
    this.dirHeading = 0;        // Direction player is going
    this.targetDir = 0;         // Direction where user wants to face
    this.oldTargetDir = 0;
    this.newTargetDir = false;  // Is targetDir different?
    this.rotFac = 0.05;         // Rotation factor, or speed

    // Shooting
    this.dirShooting = 0;       // Direction player is shooting

    // --- Weapons ---
    // Fire Span: fire every X frames
    this.weaponFireSpan = 10;
    this.weaponFastFireSpan = 7;
    this.weaponSlowFireSpan = 13;
    this.weaponSnipingFireSpan = 20;

    this.weaponFireTime = 0;    // Fire rate timer

    this.weaponType = WeaponID.NARROW;  // Stores the current weapon type
    this.previousWeapon = null;             // Stores the previous weapon used
    this.weaponRevertSpan = 600;
    this.weaponRevertTime = this.weaponRevertSpan;
    this.weaponRevertable = false;

    // Weapon bullet speeds
    this.bulletSlowSpeed = 11;
    this.bulletSlowSlowSpeed = 10.25;
    this.bulletRegSlowSpeed = 13.5;
    this.bulletRegSpeed = 15;
    this.bulletFastSpeed = 18;
    this.bulletFastSlowSpeed = 17;
    this.bulletSnipingSpeed = 20;

    // Bombs
    this.bombDropTime = 0;
    this.bombDropSpan = 60;

    // Invulnerability
    this.vulnerable = false;
    this.invulnerableSpawnSpan = 120;
    this.invulnerableBonusSpan = 600;
    this.invulnerableTime = this.invulnerableSpawnSpan;
    this._GOD = false;

    this.ghostingSpan = 6;
    this.ghostingTime = this.ghostingSpan;

    this.invulnerableFlashSpan = 20;
    this.invulnerableFlashTime = this.invulnerableFlashSpan;
    this.invulnerableFinishSpan = this.invulnerableFlashSpan * 3.9;
    this.invulnerableFinishTime = this.invulnerableFinishSpan;

    this.spawner = [];

    // Rendering
    this.model = Model.createPlayer();
  }
  /** 
   * Updates the player's variables based on the user's input.
   * @method tick
   * @memberof Player
   * @param {object} controls        Controls instance.
   * @param {Array<number>} mousePos Position of the mouse cursor.
   * @param {object} level           Instance of the current Level object.
   * @param {object} soundEngine     SoundEngine instance.
   */
  tick (controls, mousePos, level, soundEngine) {
    if (!this.isDying && !this.isSpawning) {
      // Weapon reverting timer
      if (this.weaponRevertable && this.previousWeapon != null) {
        this.weaponRevertTime--;

        if (this.weaponRevertTime <= 0) {
          this.weaponType = this.previousWeapon;
          this.previousWeapon = null;
          this.weaponRevertTime = this.weaponRevertSpan;
          this.weaponRevertable = false;
        }
      }

      // Invlunerability ghosting
      if (!this.vulnerable) {
        // Is the first timer running?
        if (this.invulnerableTime > 0) {
          // Update timer as long as it's not God
          if (!this._GOD)
            this.invulnerableTime--;
          
          this.ghostingTime--;

          if (this.ghostingTime <= 0) {
            this.ghostingTime = this.ghostingSpan;
            this.spawner.push(new Ghost(this.x, this.y, this.dirFacing, 5,
              this.model, this.width, this.height));
          }
        } else if (this.invulnerableFinishTime > 0) {
          this.invulnerableFinishTime--;
          this.invulnerableFlashTime--;

          if (this.invulnerableFlashTime <= 0) {
            this.invulnerableFlashTime = this.invulnerableFlashSpan;
            this.spawner.push(new Ghost(this.x, this.y, this.dirFacing, 5,
              this.model, this.width, this.height));
            soundEngine.addSound(SoundID.INVINCIBLE);
          }
        } else
          this.vulnerable = true;
      }

      // --- Check for movement --- //
      if (controls.up || controls.down || controls.left || controls.right)
        this.isMoving = true;
      else
        this.isMoving = false;

      // --- Adjust the player's orientation --- //
      // Set the target direction based on the input
      if (controls.up && controls.left) this.targetDir = 1.75;
      else if (controls.up && controls.right) this.targetDir = 0.25;
      else if (controls.left && controls.down) this.targetDir = 1.25;
      else if (controls.right && controls.down) this.targetDir = 0.75;
      else if (controls.up) this.targetDir = 0;
      else if (controls.right) this.targetDir = 0.5;
      else if (controls.down) this.targetDir = 1;
      else if (controls.left) this.targetDir = 1.5;

      // Making the rotation smooth
      if (this.isMoving && this.dirFacing != this.targetDir) {
        // Did the player change where he wanted to go?
        if (this.targetDir != this.oldTargetDir) {
          this.newTargetDir = true;
          this.oldTargetDir = this.targetDir;
          this.dirAdjustment = false;
        }

        // This is for if there's a difference greater that 1 in the rotation
        // i.e. from 0.25 to 1.75 so that the player doesn't go full circle
        if (this.newTargetDir) {
          if (this.dirFacing - this.targetDir > 1) this.dirFacing -= 2;
          if (this.dirFacing - this.targetDir < -1) this.dirFacing += 2;
          this.newTargetDir = false;
        }

        // Adjust the player's direction smoothly
        if (this.dirFacing > this.targetDir)  {
          if (this.dirFacing - this.targetDir < this.rotFac)
            this.dirFacing = this.targetDir;
          else
            this.dirFacing -= this.rotFac;

        } else if (this.dirFacing < this.targetDir) {
          if (this.dirFacing - this.targetDir > -this.rotFac)
            this.dirFacing = this.targetDir;
          else
            this.dirFacing += this.rotFac;
        }
      }

      // --- Adjust the player's speed --- //
      if (this.isMoving) {    // Accelerate
        if (this.speed != this.maxSpeed) {
          this.speed += this.acc;
          if (this.speed > this.maxSpeed) this.speed = this.maxSpeed;
        }
      } else {                // Decelerate
        if (this.speed > 0) {
          this.speed -= this.acc;
          if (this.speed < 0) this.speed = 0;
        }
      }

      // Adjust dir heading
      this.dirHeading = this.targetDir;

      // --- Move the player around --- //
      this.nx += Math.sin(this.dirHeading * Math.PI) * this.speed;
      this.ny += -Math.cos(this.dirHeading * Math.PI) * this.speed;

      // --- Make sure the player is still in the level --- //
      if (this.nx + this.width / 2 > level.width / 2
      || this.nx - this.width / 2 < -level.width / 2)
        this.nx = this.x;

      if (this.ny + this.height / 2 > level.height / 2
      || this.ny - this.height / 2 < -level.height / 2)
        this.ny = this.y;

      // Apply movement to object
      this.x = this.nx;
      this.y = this.ny;

      // -------------------- //
      // ----< Shooting >---- //
      // -------------------- //
      if (controls.shoot && level.canShoot) {
        // Check the timer
        if (this.weaponFireTime <= 0) {
          // Grabing range of mouse from player
          var rangeX = mousePos.x - this.onScreenx;
          var rangeY = mousePos.y - this.onScreeny;
          var mpr = Math.sqrt(Math.pow(Math.abs(rangeX), 2) +
                              Math.pow(Math.abs(rangeY), 2));

          // Grabing angle variables from range
          var shootCos = Math.asin(Math.abs(rangeX) / mpr) / Math.PI * 2;
          var shootSin = Math.acos(Math.abs(rangeX) / mpr) / Math.PI * 2;
          var shootTan = Math.abs(Math.atan(rangeX / rangeY)) / Math.PI;

          if (mousePos.x < this.onScreenx) shootCos = -shootCos;
          if (mousePos.y > this.onScreeny) shootSin = -shootSin;

          // Calculating shooting angle
          if (shootSin >= 0 && shootSin <= 1 && shootCos >= 0 && shootCos <= 1)
            this.dirShooting = shootTan;
          else if (shootSin >= 0 && shootSin <= 1 &&
                   shootCos >= -1 && shootCos <= 0)
            this.dirShooting = 2 - shootTan;
          else if (shootSin >= -1 && shootSin <= 0 &&
                   shootCos >= -1 && shootCos <= 0)
            this.dirShooting = 1 + shootTan;
          else
            this.dirShooting = 1 - shootTan;

          // What weapon is player shooting?
          if (this.weaponType == WeaponID.STANDARD) {
            level.createBullet(this.x, this.y, this.dirShooting,
              this.bulletRegSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.015,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.015,
              this.bulletRegSlowSpeed);
            this.weaponFireTime = this.weaponFireSpan;    // Reset timer
            soundEngine.addSound(SoundID.SHOOTING_SN);
          } else if (this.weaponType == WeaponID.REVERSE) {
            level.createBullet(this.x, this.y, this.dirShooting,
              this.bulletRegSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.015,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.015,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 1,
              this.bulletRegSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 1.015,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 1.015,
              this.bulletRegSlowSpeed);
            this.weaponFireTime = this.weaponFireSpan;    // Reset timer
            soundEngine.addSound(SoundID.SHOOTING_FR);
          } else if (this.weaponType == WeaponID.FAST) {
            level.createBullet(this.x, this.y, this.dirShooting,
              this.bulletFastSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.01,
              this.bulletFastSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.01,
              this.bulletFastSlowSpeed);
            this.weaponFireTime = this.weaponFastFireSpan; // Reset timer
            soundEngine.addSound(SoundID.SHOOTING_FR);
          } else if (this.weaponType == WeaponID.WIDE) {
            level.createBullet(this.x, this.y, this.dirShooting,
              this.bulletSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.018,
              this.bulletSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.018,
              this.bulletSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.036,
              this.bulletSlowSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.036,
              this.bulletSlowSlowSpeed);
            this.weaponFireTime = this.weaponSlowFireSpan;  // Reset timer
            soundEngine.addSound(SoundID.SHOOTING_W);
          } else if (this.weaponType == WeaponID.NARROW) {
            // Calculate new origins for bullets
            let dist = 7;
            let p1x = this.x + Math.sin((this.dirShooting + 0.5) * Math.PI)
              * dist;
            let p1y = this.y - Math.cos((this.dirShooting + 0.5) * Math.PI)
              * dist;
            let p2x = this.x + Math.sin((this.dirShooting - 0.5) * Math.PI)
              * dist;
            let p2y = this.y - Math.cos((this.dirShooting - 0.5) * Math.PI)
              * dist;

            // Create bullets
            level.createBullet(p1x, p1y, this.dirShooting, this.bulletRegSpeed);
            level.createBullet(p2x, p2y, this.dirShooting, this.bulletRegSpeed);

            this.weaponFireTime = this.weaponFireSpan;  // Reset timer
            soundEngine.addSound(SoundID.SHOOTING_SN);
          } else if (this.weaponType == WeaponID.SUPER) {
            level.createBullet(this.x, this.y, this.dirShooting,
              this.bulletRegSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.015,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.015,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting - 0.225,
              this.bulletRegSlowSpeed);
            level.createBullet(this.x, this.y, this.dirShooting + 0.225,
              this.bulletRegSlowSpeed);
            this.weaponFireTime = this.weaponFireSpan;    // Reset timer
            soundEngine.addSound(SoundID.SHOOTING_S);
          } else if (this.weaponType == WeaponID.SNIPER) {
            level.createBullet(this.x, this.y, this.dirShooting, 
              this.bulletSnipingSpeed, true);
            this.weaponFireTime = this.weaponSnipingFireSpan;
            soundEngine.addSound(SoundID.SHOOTING_SNIPER);
          }

          // Add sound
          // soundEngine.addSound(SoundID.SHOOTING_SN);
        }
      }

      // Dropping Bombs
      if (controls.special && level.canDropBombs) {
        if (this.bombDropTime <= 0 && level.bombCount > 0) {
          level.spawnBomb(this.x, this.y, 600);
          this.bombDropTime = this.bombDropSpan;
          level.bombCount--;
          soundEngine.addSound(SoundID.USE_BOMB);
        }
      }

      // Update the timer
      this.weaponFireTime--;
      this.bombDropTime--;

      // Make sure the timer doesn't go down forever
      if (this.weaponFireTime < 0) this.weaponFireTime = 0;
    } else if (this.isDying) {
      this.speed = 0;
      this.weaponFireTime = this.weaponFireSpan;
      this.spawnTime = this.spawnSpan;
    } else {
      this.spawnTime--;
      this.dirFacing = 0;

      if (this.spawnTime <= 0) {
        this.isSpawning = false;
        this.isAlive = true;
      }
    }
  }
  /**
   * Changes the player's on-screen position.
   * @param {number} x X position.
   * @param {number} y Y position.
   */
  changeOnScreenPosition (x, y) {
    this.onScreenx = x;
    this.onScreeny = y;
  }
  /**
   * Updates variables depending on what this object is colliding with.
   * @method colliding
   * @memberof Player
   * @param {number} entityType Type of entity is is colliding with.
   */
  colliding (entityType) {
    if (Entities.getPlayerKillers().indexOf(entityType) > -1)
      if (this.vulnerable) {
          console.log("Player dead...");
          this.isDying = true;
        }
  }
  /**
   * Makes the player invulnerable for a short amount of time.
   * @method makeInvulnerable
   * @memberof Player
   * @param {boolean} [bonus = false] Controls whether the invulnerability is 
   *                                  from spawning or getting a bonus.
   */
  makeInvulnerable (bonus = false) {
    this.vulnerable = false;

    this.invulnerableFinishTime = this.invulnerableFinishSpan;
    this.invulnerableFlashTime = this.invulnerableFlashSpan;

    if (bonus)
      this.invulnerableTime = this.invulnerableBonusSpan;
    else
      this.invulnerableTime = this.invulnerableSpawnSpan;
  }
  /**
   * Makes the player invulnerable indefinitly.
   * @method _makeGOD
   * @memberof Player
   */
  _makeGOD () {
    this.vulnerable = false;

    this.invulnerableFinishTime = this.invulnerableFinishSpan;
    this.invulnerableFlashTime = this.invulnerableFlashSpan;
    this.invulnerableTime = 1;    // So that it doesn't stop right away.

    this._GOD = true;
  }
  /**
   * Makes the current weapon the previous and make it revertable.
   * @method makeWeaponRevertable
   * @memberof Player
   */
  makeWeaponRevertable () {
    this.weaponRevertTime = this.weaponRevertSpan;
    this.weaponRevertable = true;
  }
  /**
   * Rotates an entity to face its target.
   * @method rotate
   * @memberof Player
   * @static
   * @param {object} entity Entity instance to rotate.
   */
  static rotate (entity) {
    if (entity.dirFacing != entity.targetDir) {
      // Did the player change where he wanted to go?
      if (entity.targetDir != entity.oldTargetDir) {
        entity.newTargetDir = true;
        entity.oldTargetDir = entity.targetDir;
        entity.dirAdjustment = false;
      }

      // This is for if there's a difference greater that 1 in the rotation
      // i.e. from 0.25 to 1.75 so that the player doesn't go full circle
      if (entity.newTargetDir) {
        if (entity.dirFacing - entity.targetDir > 1) entity.dirFacing -= 2;
        if (entity.dirFacing - entity.targetDir < -1) entity.dirFacing += 2;
        entity.newTargetDir = false;
      }

      // Adjust the player's direction smoothly
      if (entity.dirFacing > entity.targetDir)  {
        if (entity.dirFacing - entity.targetDir < entity.rotFac)
          entity.dirFacing = entity.targetDir;
        else
          entity.dirFacing -= entity.rotFac;

      } else if (entity.dirFacing < entity.targetDir) {
        if (entity.dirFacing - entity.targetDir > -entity.rotFac)
          entity.dirFacing = entity.targetDir;
        else
          entity.dirFacing += entity.rotFac;
      }
    }
  }
}
