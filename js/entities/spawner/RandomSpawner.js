/**
 * @class
 * @extends Spawner
 */
class RandomSpawner extends Spawner {
  /**
   * @constructor
   * @memberof RandomSpawner
   * @param {number} amount     Amount of entities to spawn.
   * @param {number} time       Amount of frames between each spawn 
   *                            cycles.
   * @param {number} entityType Type of entities to spawn.
   */
  constructor (amount, time, entityType) {
    super(amount, time, entityType);
    this.entityType = EntityID.RANDOM_SPAWNER;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof RandomSpawner
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);
  }
}
