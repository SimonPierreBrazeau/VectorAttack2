/** @class */
class Spawner {
  /**
   * @constructor
   * @memberof Spawner
   * @param {number} amount           Amount of entities to spawn.
   * @param {number} time             Amount of frames between each spawn 
   *                                  cycles.
   * @param {number} entityType       Type of entities to spawn.
   * @param {number} [x = 0]          X position.
   * @param {number} [y = 0]          Y position.
   * @param {boolean} [random = true] Controls whether it should spawn its 
   *                                  entities at random locations or not.
   */
  constructor(amount, time, entityType, x = 0, y = 0, random = true) {
    this.x = x;
    this.y = y;
    this.entityType = null;
    this.superType = EntityID.SPAWNER;

    this.amount = amount;
    this.spawnSpan = time;
    this.spawnTime = this.spawnSpan;
    this.spawnType = entityType;

    this.randomLocation = random;
    this.isAlive = true;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Spawner
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    this.spawnTime--;

    if (this.spawnTime <= 0) {
      this.amount--;

      if (this.randomLocation)
        level.spawnEntityAtRandom(this.spawnType);
      else
        level.spawnEntityAtPosition(this.spawnType, this.x, this.y);

      if (this.amount > 0) {
        this.spawnTime = this.spawnSpan;
      } else
        this.isAlive = false;
    }
  }
}
