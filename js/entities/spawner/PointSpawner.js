/**
 * @class
 * @extends Spawner
 */
class PointSpawner extends Spawner {
  /**
   * @constructor
   * @memberof PointSpawner
   * @param {number} x          X position.
   * @param {number} y          Y position.
   * @param {number} amount     Amount of entities to spawn.
   * @param {number} time       Amount of frames between each spawn 
   *                            cycles.
   * @param {number} entityType Type of entities to spawn.
   */
  constructor (x, y, amount, time, entityType) {
    super(amount, time, entityType, x, y, false);
    this.entityType = EntityID.POINT_SPAWNER;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof PointSpawner
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);
  }
}
