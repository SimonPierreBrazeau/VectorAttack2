var WeaponID = {
  STANDARD: 0,
  REVERSE: 1,
  FAST: 2,
  WIDE: 3,
  NARROW: 4,
  SUPER: 5,
  SNIPER: 6,
  INVULNERABLE: 9
};

/** @class */
class Weapons {
  /**
   * Returns a random standard weapon ID.
   * @method getRandomStandardWeapon
   * @memberof Weapons
   * @static
   * @returns {number}
   */
  static getRandomStandardWeapon () {
    let types = [
      WeaponID.FAST,
      WeaponID.WIDE
    ];

    return types[Math.round(Math.random() * (types.length - 1))];
  }
  /**
   * Returns a random special weapon ID.
   * @method getRandomSpecialWeapon
   * @memberof Weapons
   * @static
   * @returns {number}
   */
  static getRandomSecialWeapon () {
    let types = [
      WeaponID.REVERSE,
      WeaponID.SUPER
    ];

    return types[Math.round(Math.random() * (types.length - 1))];
  }
  /**
   * Returns a random bonus state ID.
   * @method getRandomBonusState
   * @memberof Weapons
   * @static
   * @returns {number}
   */
  static getRandomBonusState () {
    let types = [
      WeaponID.REVERSE,
      WeaponID.SUPER,
      WeaponID.INVULNERABLE
    ];

    return types[Math.round(Math.random() * (types.length - 1))];
  }
}