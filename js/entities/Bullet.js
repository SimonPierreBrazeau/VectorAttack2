/**
 * @class
 * @extends Enemy
 */
class Bullet extends Enemy {
  /**
   * @constructor
   * @memberof Bullet
   * @param {number} x                       X position.
   * @param {number} y                       Y position.
   * @param {number} dir                     Direction of the bullet.
   * @param {number} speed                   Maximum traveling speed of bullet.
   * @param {boolean} [invulnerable = false] Controls whether the bullet is 
   *                                         invulnerable or not.
   */
  constructor (x, y, dir, speed, invulnerable = false) {
    super(x, y, 6, 6, speed, EntityID.BULLET, Model.createBullet(), 0, 0)
    this.dirFacing = dir;
    this.speed = speed;

    this.hitWall = false;   // Controls if we need to play wall hit sound

    // Is the bullet alive?
    this.isSpawning = false;

    // Ghosting
    this.isInvulnerable = invulnerable;
    this.isGhosting = invulnerable;
    this.ghostingSpan = 2;
    this.ghostingTime = this.ghostingSpan;
    this.ghostDuration = 1;

    this.model = Model.createBullet();

    this.modelCurrentCoord = new Array(this.model.vertices.length);
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Bullet
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    // --- Move the bullet around --- //
    this.x += Math.sin(this.dirFacing * Math.PI) * this.speed;
    this.y -= Math.cos(this.dirFacing * Math.PI) * this.speed;

    // Check if the bullet is still in the playfield
    if (this.x < -(level.width / 2)) this.isAlive = false;
    else if (this.x > level.width / 2) this.isAlive = false;
    else if (this.y < -(level.height / 2)) this.isAlive = false;
    else if (this.y > level.height / 2) this.isAlive = false;

    if (this.isAlive == false)
      this.hitWall = true;
  }
  /**
   * Updates variables depending on what this object is colliding with.
   * @method colliding
   * @memberof Bullet
   * @param {number} entityType Type of entity is is colliding with.
   */
  colliding (entityType) {
    if (Entities.getPlayerDestroyables().indexOf(entityType) > -1)
      if (!this.isInvulnerable) this.isAlive = false;
    else if (entityType == EntityID.BLOCK) {
      this.isAlive = false;
      this.hitWall = true;
    }
  }
}
