/** @class */
class ScorePop {
  /**
   * @constructor
   * @memberof ScorePop
   * @param {string} amount           Text to be displayed.
   * @param {number} x                X position.
   * @param {number} y                Y position.
   * @param {string} [color = "#0EF"] Color of the text.
   */
  constructor (amount, x, y, color = "#0EF") {
    this.x = x;
    this.y = y;
    this.entityType = EntityID.SCORE_POP;
    this.width = amount.toString().length * 5;
    this.height = 10;

    this.model = Model.createTextModel(amount, "score", color);

    this.isAlive = true;
    this.lifeTime = 0;
    this.lifeSpan = 30;
    this.visible = true;
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof ScorePop
   */
  tick () {
    this.lifeTime++;
    if (this.lifeTime >= this.lifeSpan)
      this.isAlive = false;
  }
}
