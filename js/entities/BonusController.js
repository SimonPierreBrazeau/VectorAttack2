/**
 * @class
 * @extends Enemy
 */
class BonusController extends Enemy {
  /**
   * @constructor
   * @memberof BonusController
   * @param {number} x                X position.
   * @param {number} y                Y position.
   * @param {number} bonusType        ID of the bonus given for destroying 
   *                                  every bonus peg controlled by this 
   *                                  controller.
   * @param {object} level            Instance of the current Level object.
   * @param {number} [lifeTime = 660] How many frames will it be alive.
   */
  constructor (x, y, bonusType, level, lifeTime = 660) {
    super(x, y, 0, 0, 0, EntityID.BONUS_CONTROLLER, new Model(), 0, 0);
    this.bonusType = bonusType;
    this.pegCount = 0;              // How many pegs are left
    this.pegPlacement = null;       // How pegs are placed on the map

    this.lifeTime = lifeTime;

    if (bonusType == WeaponID.REVERSE)
      this.pegPlacement = BonusController.pegReverse();
    else if (bonusType == WeaponID.FAST)
      this.pegPlacement = BonusController.pegFast();
    else if (bonusType == WeaponID.WIDE)
      this.pegPlacement = BonusController.pegWide();
    else if (bonusType == WeaponID.SUPER)
      this.pegPlacement = BonusController.pegSuper();
    else
      this.pegPlacement = BonusController.pegInvulnerable();

    // Spawn Pegs
    var pS = 30;
    var structW = this.pegPlacement[0].length;
    var structH = this.pegPlacement.length;

    for (var i = 0; i < this.pegPlacement.length; i++) {
      for (var j = 0; j < this.pegPlacement[i].length; j++) {
        if (this.pegPlacement[i][j]) {
          var px = x + (j - Math.floor(structW / 2)) * pS;
          var py = y + (i - Math.floor(structH / 2)) * pS;

          if (structW % 2 == 0) px += pS / 2;
          if (structH % 2 == 0) py += pS / 2;

          level.spawnBonusPeg(px, py, this);
          this.pegCount++;
        }
      }
    }

  }
  /**
   * Updates variables of the controller.
   * @method tick
   * @memberof BonusController
   * @param {object} level Instance of current Level object.
   */
  tick (level) {
    // Handle life time
    this.lifeTime--;
    if (this.lifeTime == 0) this.isAlive = false;

    // Check if every peg has been destroyed
    if (this.pegCount == 0) {
      if (this.bonusType == WeaponID.INVULNERABLE)
        level.player.makeInvulnerable(true);
      else {
        // Make sure that the current weapon is not a bonus weapon
        if (level.player.weaponType == level.defaultPlayerWeapon)
          level.player.previousWeapon = level.player.weaponType;

        level.player.weaponType = this.bonusType;
        level.player.makeWeaponRevertable();
      }

      this.isAlive = false;
    }
  }
  /**
   * Returns the placement of the bonus pegs for the REVERSE bonus.
   * @method pegReverse
   * @memberof BonusController
   * @static
   * @returns {Array<Array<number>>}
   */
  static pegReverse () {
    var placement = [
      [0, 1, 0],
      [1, 1, 1],
      [1, 1, 1],
      [1, 0, 1],
      [1, 1, 1],
      [1, 1, 1],
      [0, 1, 0]
    ];

    return placement;
  }
  /**
   * Returns the placement of the bonus pegs for the FAST bonus.
   * @method pegFast
   * @memberof BonusController
   * @static
   * @returns {Array<Array<number>>}
   */
  static pegFast () {
    var placement = [
      [0, 1, 0],
      [1, 1, 1],
      [1, 1, 1],
      [1, 0, 1],
      [0, 1, 0],
      [0, 1, 0],
      [0, 1, 0]
    ];

    return placement;
  }
  /**
   * Returns the placement of the bonus pegs for the WIDE bonus.
   * @method pegWide
   * @memberof BonusController
   * @static
   * @returns {Array<Array<number>>}
   */
  static pegWide () {
    var placement = [
      [0, 0, 1, 0, 0],
      [0, 1, 1, 1, 0],
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 0, 0, 0, 1]
    ];

    return placement;
  }
  /**
   * Returns the placement of the bonus pegs for the SUPER bonus.
   * @method pegSuper
   * @memberof BonusController
   * @static
   * @returns {Array<Array<number>>}
   */
  static pegSuper () {
    var placement = [
      [0, 0, 0, 1, 0, 0, 0],
      [0, 0, 1, 1, 1, 0, 0],
      [1, 0, 1, 1, 1, 0, 1],
      [0, 1, 1, 0, 1, 1, 0],
      [0, 0, 1, 1, 1, 0, 0]
    ];

    return placement;
  }
  /**
   * Returns the placement of the bonus pegs for the INVULNERABLE bonus.
   * @method pegInvulnerable
   * @memberof BonusController
   * @static
   * @returns {Array<Array<number>>}
   */
  static pegInvulnerable () {
    let placement = [
      [0, 1, 1, 1, 1, 0],
      [1, 0, 0, 0, 0, 1],
      [1, 0, 1, 1, 0, 1],
      [1, 0, 1, 1, 0, 1],
      [1, 0, 0, 0, 0, 1],
      [0, 1, 1, 1, 1, 0]
    ];

    return placement;
  }
}
