/**
 * @class
 * @extends Enemy
 */
class BonusPeg extends Enemy {
  /**
   * @constructor
   * @memberof BonusPeg
   * @param {number} x               X position.
   * @param {number} y               Y position.
   * @param {object} bonusController Instance of BonusController controlling 
   *                                 this instance.
   */
  constructor (x, y, bonusController) {
    super(x, y, 20, 20, 0, EntityID.BONUS_PEG, Model.createBonusPeg(), 5, 1);
    this.bonusController = bonusController;
  }
  /**
   * Updates variables of the peg.
   * @method tick
   * @memberof BonusPeg
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    super.tick(level);

    if (!this.isFading && !this.isKiller && !this.bonusController.isAlive)
      this.isFading = true;
  }
  /**
   * Updates variables depending on what the peg is colliding with.
   * @method colliding
   * @memberof BonusPeg
   * @param {number} entityType        Type of entity it is colliding with.
   * @param {boolean} playerVulnerable Whether the player is vulnerable or not.
   */
  colliding (entityType, playerVulnerable) {
    super.colliding(entityType, playerVulnerable);
    if (entityType == EntityID.BULLET || entityType == EntityID.BOMB ||
      entityType == EntityID.PLAYER && !playerVulnerable)
      this.bonusController.pegCount--;

    if (this.isKiller)
      console.log(this);
  }
}
