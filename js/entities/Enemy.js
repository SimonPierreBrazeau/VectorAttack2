/** @class */
class Enemy {
  /**
   * @constructor
   * @memberof Enemy
   * @param {number} x          X position.
   * @param {number} y          Y position.
   * @param {number} width      Width of collision bound.
   * @param {number} height     Height of collision bound.
   * @param {number} speed      Maximum traveling speed.
   * @param {number} entityType Type of the entity.
   * @param {object} model      Model instance of the entity.
   * @param {number} points     Points given by killing this entity.
   * @param {number} gems       Amount of gems given by killing this entity.
   */
  constructor (x, y, width, height, speed, entityType, model, points, gems) {
    this.x = x;                       // Global x position
    this.y = y;                       // Global y position
    this.nx = x;                      // Attempted x position
    this.ny = y;                      // Attempted y position
    this.width = width;               // Width of object
    this.height = height;             // Height of object
    this.entityType = entityType;     // Type of object
    this.model = model;               // Model of object
    this.lifeTime = 0;                // Amount of frames object has been alive
    this.vulnerable = true;           // Can be killed by bullets
    this.superType = EntityID.ENEMY;  // Super type ID
    this.health = 1;                  // How many bullets until death

    // Movement
    this.speed = 0;                   // Current speed of object
    this.topSpeed = speed;            // Maximum speed of object
    this.speedAccel = 0.4;            // Speed acceleration of object

    // Bounce
    this.bounceH = false;
    this.bounceHSpan = 20;
    this.bounceHTime = this.bounceHSpan;

    this.bounceV = false;
    this.bounceVSpan = 20;
    this.bounceVTime = this.bounceVSpan;

    // Directions
    this.dirFacing = 0;               // Facing agle of model
    this.dirHeading = 0;              // Direction object is heading
    this.rotFac = 0.05;               // Rotation speed of object
    this.targetDir = 0;
    this.oldTargetDir = 0;
    this.newTargetDir = false;
    this.dirAdjustment = false;

    // State Variables
    this.isAlive = true;              // Is object alive?
    this.isSpawning = true;           // Is object spawning?
    this.isDying = false;             // Is object dying?
    this.isFading = false;            // Is object fading?
    this.isKiller = false;            // Did object kill player?
    this.killedByPlayer = false;      // Was object killed by player?
    this.killedByBomb = false;        // Was object killed by bomb?

    // Spawning timer
    this.spawnSpan = 60;              // How long is spawning?
    this.spawnTime = this.spawnSpan;  // Where is object in spawning?

    // Death Timer
    this.deathTime = 0;               // Where is object in death?
    this.deathSpan = 60;              // How long is death?

    // Fade Timer
    this.fadeSpan = 30;
    this.fadeTime = this.fadeSpan;

    // Ghosting
    this.isGhosting = false;
    this.ghostingSpan = 6;
    this.ghostingTime = this.ghostingSpan;
    this.ghostDuration = 20;

    // Scoring Stuff
    this.pointValue = points;         // Points given to player on death
    this.gemValue = gems;             // Gems dropped on death
    this.gemDropped = false;          // Did it drop gems?

    // Entity Spawning
    this.spawner = [];                // This array contains objects that are
                                      // to be pushed in the game's entity
                                      // array
  }
  /**
   * Updates variables of the object.
   * @method tick
   * @memberof Enemy
   * @param {object} level Instance of the current Level object.
   */
  tick (level) {
    this.tickSpawnDeath();

    // Ghosting tick
    if (!this.isSpawning && !this.isDying && !this.isFading &&
      this.isGhosting) {
      this.ghostingTime--;
      if (this.ghostingTime <= 0) {
        this.ghostingTime = this.ghostingSpan;
        let m = this.model;
        this.spawnEntity(new Ghost(this.x, this.y, this.dirFacing,
          this.ghostDuration, m, this.width, this.height));
      }
    }
  }
  /**
   * Moves the entity and makes sure that it's still in the level.
   * @method move
   * @memberof Enemy
   * @param {number} levelWidth  Width of the level.
   * @param {number} levelHeight Height of the level.
   * @param {boolean} bounce     Controls whether the entity should bounce off
   *                             the level's walls or not.
   * @param {boolean} accelerate Controls whether the entity should accelerate
   *                             to its top speed or not.
   */
  move (levelWidth, levelHeight, bounce = false, accelerate = true) {
    // Accelerate object
    if (accelerate) {
      this.speed += this.speedAccel;
      if (this.speed > this.topSpeed) this.speed = this.topSpeed;
    }

    // Move the object
    this.nx += Math.sin(this.dirHeading * Math.PI) * this.speed;
    this.ny -= Math.cos(this.dirHeading * Math.PI) * this.speed;

    if (bounce) {
      // Bounce vertically
      if ((this.y - this.height / 2 <= -levelHeight / 2 ||
        this.y + this.height / 2 >= levelHeight / 2) && this.bounceV) {
        // Check if going left or right
        if (this.dirHeading >= 0 && this.dirHeading < 1)
          this.dirHeading = 1 - this.dirHeading;
        else
          this.dirHeading = 2 - (this.dirHeading - 1);

        this.bounceV = false;
      }

      // Bounce horizontally
      if ((this.x - this.width / 2 < -levelWidth / 2 ||
        this.x + this.width / 2 >= levelWidth / 2) && this.bounceH) {
        // Check if going up or down
        if (this.dirHeading >= 0.5 && this.dirHeading < 1.5)
          this.dirHeading = 1.5 - (this.dirHeading - 0.5);
        else
          this.dirHeading = 0.5 - (this.dirHeading - 1.5);

        this.bounceH = false;
      }

      // Make sure that the direction is within 0 and 2
      if (this.dirHeading > 2) this.dirHeading -= 2;
      if (this.dirHeading < 0) this.dirHeading += 2;

      // Update the timers
      if (!this.bounceH) {
        this.bounceHTime--;
        if (this.bounceHTime <= 0) {
          this.bounceHTime = this.bounceHSpan;
          this.bounceH = true;
        }
      }

      if (!this.bounceV) {
        this.bounceVTime--;
        if (this.bounceVTime <= 0) {
          this.bounceVTime = this.bounceVSpan;
          this.bounceV = true;
        }
      }
    } else {
      // Check if the object is still in the level
      if (this.nx + this.width / 2 > levelWidth / 2
      || this.nx - this.width / 2 < -levelWidth / 2)
        this.nx = this.x;

      if (this.ny + this.height / 2 > levelHeight / 2
      || this.ny - this.height / 2 < -levelHeight / 2)
        this.ny = this.y;
    }

    // Apply movement to object
    this.x = this.nx;
    this.y = this.ny;
  }
  /**
   * Updates variables depending on what this object is colliding with.
   * @method colliding
   * @memberof Enemy
   * @param {number} entityType        Type of entity is is colliding with.
   * @param {boolean} playerVulnerable Whether the player is vulnerable or not.
   */
  colliding (entityType, playerVulnerable) {
    switch (entityType) {
      case EntityID.PLAYER:
        if (playerVulnerable) {
          this.isKiller = true;
          break;
        }
      case EntityID.BULLET:
        if (this.vulnerable) {
          this.health--;
          if (this.health > 0) break;
        }
      case EntityID.BOMB:
        if (this.vulnerable) {
          this.isDying = true;
          this.deathTime = this.deathSpan;
          this.killedByPlayer = true;
        }
        break;
      default:
    }
  }
  /**
   * Updates the death, spawning or fading timers based on the state of the 
   * entity (isDying, isSpawning or isFading).
   * @method tickSpawnDeath
   * @memberof Enemy
   */
  tickSpawnDeath () {
    if (this.isDying) {
      this.deathTime--;
      if (this.deathTime <= 0) this.isAlive = false;
    } else if (this.isSpawning) {
      this.spawnTime--;
      if (this.spawnTime <= 0) this.isSpawning = false;
    } else if (this.isFading) {
      this.fadeTime--;
      if (this.fadeTime <= 0) this.isAlive = false;
    }
  }
  /**
   * Adds entities to the spawner array so that it can be added to the level 
   * during its next tick cycle.
   * @method spawnEntity
   * @memberof Enemy
   * @param {object} entity       Instance of entity to add to the array.
   * @param {number} [amount = 1] Amount of entities to add to the array.
   */
  spawnEntity (entity, amount = 1) {
    for (var i = 0; i < amount; i++) this.spawner.push(entity);
  }
}
