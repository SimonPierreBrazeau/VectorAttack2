/** @class */
class FPScounter {
  /**
   * @constructor
   * @memberof FPScounter
   */
  constructor () {
    this.startTime = 0;
    this.endTime = 0;
    this.frameTimes = 0;
    this.frames = 0;
    this.threshold = 250;
    this.lastLog = 0;
  }
  /**
   * Starts recording the frame rendered.
   * @method startCounter
   * @memberof FPScounter
   */
  startCounter () {
    var time = new Date();
    this.startTime = time.getTime();
  }
  /**
   * Stops recording the frame rendered and calculates the frame rate. If 
   * printLog is true, it prints the results to the browser console and returns
   * it.
   * @method stopAndPost
   * @memberof FPScounter
   * @param {boolean} printLog Controls whether the results are returned or not.
   * @returns {number}
   */
  stopAndPost (printLog) {
    if (this.startTime === 0) {
      this.startCounter();
      return -1;
    } else {
      var time = new Date();
      this.endTime = time.getTime();
      this.frameTimes = this.endTime - this.startTime;
      ++this.frames;
  
      if (this.frameTimes >= this.threshold) {
        this.lastLog = this.frames / (this.threshold / 1000);
        if (printLog) 
          this.logFPS();
        this.reset();
        return this.lastLog;
      }
      return -1;
    }
  }
  /**
   * Resets the counter's variables.
   * @method reset
   * @memberof FPScounter
   */
  reset () {
    this.frames = 0;
    this.frameTimes = 0;
    this.startCounter();
  }
  /**
   * Returns the last calculated frame rate.
   * @method getFPS
   * @memberof FPScounter
   * @returns {number}
   */
  getFPS () {
    return this.lastLog;
  }
  /**
   * Prints the last calculated frame rate to the browser console.
   * @method logFPS
   * @memberof FPScounter
   */
  logFPS () {
    console.log(this.lasLog + " fps");
  }
}
