var SoundID = {
  SHOOTING_SN: 0,
  SHOOTING_W: 1,
  SHOOTING_FR: 2,
  SHOOTING_S: 3,
  INVINCIBLE: 4,
  ENEMY_SPAWN: 5,
  BONUS_SPAWN: 6,
  ENEMY_DEATH: 7,
  TIME_RUNNING_OUT: 8,
  GAIN_BL: 9,
  GAME_OVER: 10,
  MENU_SELECT: 11,
  PAUSE_MENU_OPEN: 12,
  WALL_HIT: 13,
  SHOOTING_SNIPER: 14,
  PLAYER_DEATH: 15,
  USE_BOMB: 16,
  COLLECT_GEM: 17
};

class Sounds {
  static getSource (soundID) {
    switch (soundID) {
      case SoundID.SHOOTING_SN:      return "";
      case SoundID.SHOOTING_W:       return "";
      case SoundID.SHOOTING_FR:      return "";
      case SoundID.SHOOTING_S:       return "";
      case SoundID.INVINCIBLE:       return "";
      case SoundID.ENEMY_SPAWN:      return "";
      case SoundID.BONUS_SPAWN:      return "";
      case SoundID.ENEMY_DEATH:      return "";
      case SoundID.TIME_RUNNING_OUT: return "";
      case SoundID.GAIN_BL:          return "";
      case SoundID.GAME_OVER:        return "";
      case SoundID.MENU_SELECT:      return "";
      case SoundID.PAUSE_MENU_OPEN:  return "";
      case SoundID.WALL_HIT:         return "";
      case SoundID.SHOOTING_SNIPER:  return "";
      case SoundID.PLAYER_DEATH:     return "";
      case SoundID.USE_BOMB:         return "";
      case SoundID.COLLECT_GEM:      return "";
    }
  }
}
